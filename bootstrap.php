<?php

/**
 * Установка локали
 */
setlocale(LC_ALL, 'ru_RU.UTF-8');

/**
 * Установка часового пояса
 */
date_default_timezone_set('UTC');

/**
 * Расширенная инициализация фреймворка
 */
fenric()->advancedInit();

/**
 * Фреймворк установлен из composer
 */
fenric()->registerPath('.', function() : string
{
	return __DIR__;
});

/**
 * Регистрация обработчика неперехваченных исключений
 */
fenric()->registerUncaughtExceptionHandler(function($exception)
{
	fenric('response')->setStatus(503);
});

/**
 * Регистрация в контейнере фреймворка приложения
 */
fenric()->registerDisposableSharedService('app', function()
{
	return new \Fenric\App();
});

/**
 * Регистрация в контейнере фреймворка одиночной службы для работы с учетной записью пользователя
 */
fenric()->registerDisposableSharedService('user', function()
{
	return new \Fenric\User();
});

/**
 * Регистрация в контейнере фреймворка именованной службы для вывода меню
 */
fenric()->registerResolvableSharedService('menu', function(string $resolver, array $options = null)
{
	$query = \Propel\Models\MenuQuery::create();

	$model = $query->findOneByCode($resolver);

	if ($model instanceof \Propel\Models\Menu)
	{
		return $model->getTree(
			(array) $options
		);
	}

	return $options['default'] ?? null;
});

/**
 * Регистрация в контейнере фреймворка именованной службы для вывода баннера
 */
fenric()->registerResolvableSharedService('banner', function(string $resolver, array $options = null)
{
	$query = \Propel\Models\BannerGroupQuery::create();

	$model = $query->findOneByCode($resolver);

	if ($model instanceof \Propel\Models\BannerGroup)
	{
		return $model->getBanner(
			(array) $options
		);
	}

	return $options['default'] ?? null;
});

/**
 * Регистрация в контейнере фреймворка именованной службы для вывода сниппета
 */
fenric()->registerResolvableSharedService('snippet', function(string $resolver, array $options = null)
{
	$query = \Propel\Models\SnippetQuery::create();

	$model = $query->findOneByCode($resolver);

	if ($model instanceof \Propel\Models\Snippet)
	{
		return snippetable($model->getValue());
	}

	return $options['default'] ?? null;
});

/**
 * Регистрация в контейнере фреймворка именованной службы для вывода параметра
 */
fenric()->registerResolvableSharedService('parameter', function(string $resolver, array $options = null)
{
	$query = \Propel\Models\ParameterQuery::create();

	$model = $query->findOneByCode($resolver);

	if ($model instanceof \Propel\Models\Parameter)
	{
		return snippetable($model->getValue());
	}

	return $options['default'] ?? null;
});

/**
 * Регистрация в контейнере фреймворка именованной службы для вывода опроса
 */
fenric()->registerResolvableSharedService('poll', function(string $resolver = 'random', array $options = null)
{
	$query = \Propel\Models\PollQuery::create();

	if (0 === strcmp($resolver, 'random'))
	{
		$resolver = \Propel\Models\PollQuery::getRandomCode();
	}

	$model = $query->findOneByCode($resolver);

	if ($model instanceof \Propel\Models\Poll)
	{
		return $model->getForm();
	}

	return $options['default'] ?? null;
});

/**
 * Регистрация в контейнере фреймворка именованной службы для работы с почтовым отправителем
 */
fenric()->registerResolvableSharedService('mailer', function(string $resolver = 'default')
{
	$mail = new PHPMailer();

	if (fenric('config::mailing')->get($resolver) instanceof Closure)
	{
		fenric('config::mailing')->get($resolver)($mail);
	}

	return $mail;
});

/**
 * События для форматирования сниппетов в контенте
 */
fenric('event::snippetable.menu')->subscribe(function($code, & $replace, array $options = null)
{
	$replace = fenric()->callSharedService('menu', [$code, $options]);
});

fenric('event::snippetable.banner')->subscribe(function($code, & $replace, array $options = null)
{
	$replace = fenric()->callSharedService('banner', [$code, $options]);
});

fenric('event::snippetable.snippet')->subscribe(function($code, & $replace, array $options = null)
{
	$replace = fenric()->callSharedService('snippet', [$code, $options]);
});

fenric('event::snippetable.poll')->subscribe(function($code, & $replace, array $options = null)
{
	$replace = fenric()->callSharedService('poll', [$code, $options]);
});

/**
 * Событие наступающее при создания учетной записи
 */
fenric('event::user.created')->subscribe(function(\Propel\Models\User $model)
{
	if ($model->isRegistrationConfirmed())
	{
		return;
	}

	$mail = fenric('mailer');

	$mail->addAddress($model->getEmail());

	$mail->Subject = fenric()->t('user', 'email.registration.subject', [
		'host' => fenric('request')->environment->get('HTTP_HOST') ?: 'localhost',
	]);

	$mail->Body = fenric('view::mails/user.registration.confirmation', [
		'user' => $model,
		'confirm' => sprintf('%s://%s%s/user/registration/confirm/%s/',
			fenric('request')->getScheme(),
			fenric('request')->environment->get('HTTP_HOST') ?: 'localhost',
			fenric('request')->getRoot(),
			$model->getRegistrationConfirmationCode()
		),
	])->render();

	$mail->send();
});

/**
 * Событие наступающее при создании аутентификационного токена для учетной записи
 */
fenric('event::user.authentication.token.created')->subscribe(function(\Propel\Models\User $model)
{
	$mail = fenric('mailer');

	$mail->addAddress($model->getEmail());

	$mail->Subject = fenric()->t('user', 'email.authentication.token.subject', [
		'host' => fenric('request')->environment->get('HTTP_HOST') ?: 'localhost',
	]);

	$mail->Body = fenric('view::mails/user.authentication.token', [
		'user' => $model,
		'token' => sprintf('%s://%s%s/user/login/%s/',
			fenric('request')->getScheme(),
			fenric('request')->environment->get('HTTP_HOST') ?: 'localhost',
			fenric('request')->getRoot(),
			$model->getAuthenticationToken()
		),
	])->render();

	$mail->send();
});

/**
 * Событие наступающее после загрузки изображения для изменения фотографии учетной записи
 */
fenric('event::user.change.photo.after.upload.image')->subscribe(function(array $file)
{
	fenric('user')->setPhoto(
		basename($file['location'])
	);

	fenric('user')->save();
});

/**
 * Короткий способ локализации сообщения
 */
function __(string $section, string $message, array $context = []) : string
{
	return fenric()->t($section, $message, $context);
}

/**
 * Подтверждение местоположения
 */
function is(string $location) : bool
{
	$sanitized = addcslashes($location, '\.+?[^]${}=!|:-#');

	$expression = str_replace(['(', '*', '%', ')'], ['(?:', '[^/]*', '.*?', ')?'], $sanitized);

	return !! preg_match("#^{$expression}$#u", fenric('request')->getPath());
}

/**
 * Сборка URL c возможностью переназначения параметров запроса
 */
function url(array $queries = []) : string
{
	$url = '';

	if (fenric('request')->getHost())
	{
		if (fenric('request')->getScheme())
		{
			$url .= fenric('request')->getScheme() . '://';
		}

		$url .= fenric('request')->getHost();

		if (fenric('request')->getPort())
		{
			$url .= ':' . fenric('request')->getPort();
		}
	}

	if (fenric('request')->getPath())
	{
		$url .= fenric('request')->getPath();
	}

	$query = fenric('request')->query->all();

	if (count($queries) > 0)
	{
		foreach ($queries as $key => $value)
		{
			if (is_null($value))
			{
				unset($query[$key]);

				continue;
			}

			$query[$key] = $value;
		}
	}

	if (count($query) > 0)
	{
		$url .= '?' . http_build_query($query);
	}

	return $url;
}

/**
 * Формирование ревизионного адреса статичного файла
 */
function asset(string $location) : string
{
	$absolute = fenric()->path('public', $location);

	if ($filename = realpath($absolute))
	{
		return sprintf('%s?%s', $location, filemtime($filename));
	}

	return $location;
}

/**
 * Форматирование строки для использования ее в URL
 */
function sluggable(string $value) : string
{
	$value = transliterator_transliterate('Any-Latin; Latin-ASCII; Lower()', $value);

	$value = preg_replace(['/[^a-z0-9-]/', '/-+/'], '-', $value);

	$value = trim($value, '-');

	return $value;
}

/**
 * Форматирование строки для использования ее в поиске
 */
function searchable(string $value, int $maxLength = 64, string $wordSeparator = ' ') : string
{
	$value = mb_strtolower($value, 'UTF-8');

	$value = preg_replace(['/[^\040\p{L}\p{N}]/u', '/\040+/'], ' ', $value);

	$value = trim($value);

	$value = mb_substr($value, 0, $maxLength, 'UTF-8');

	$value = rtrim($value);

	$value = str_replace(' ', $wordSeparator, $value);

	return $value;
}

/**
 * Форматирование сниппетов в строке
 */
function snippetable(string $value = null) : ?string
{
	$expression = '/{#([a-z]{1,255}):([a-zA-Z0-9-]{1,255})(?:\050({[^\050\051]+})\051)?#}/';

	return preg_replace_callback($expression, function($matches)
	{
		$replace = '';

		$options = json_decode($matches[3] ?? '{}', true);

		fenric("event::snippetable.{$matches[1]}")->run([$matches[2], & $replace, $options]);

		return $replace;

	}, $value);
}

/**
 * Экранирование строки
 */
function e(string $value = null) : string
{
	return htmlspecialchars($value, ENT_QUOTES | ENT_SUBSTITUTE, 'UTF-8');
}
