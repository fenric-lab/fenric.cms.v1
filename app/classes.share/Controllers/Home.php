<?php

namespace Fenric\Controllers;

/**
 * Import classes
 */
use DateTime;
use Fenric\Controllers\Abstractable;

use Propel\Models\SectionQuery;
use Propel\Models\PublicationQuery;
use Propel\Models\TagQuery;

use Propel\Runtime\ActiveQuery\Criteria;
use Fenric\Collection;

/**
 * Home
 */
class Home extends Abstractable
{

	/**
	 * Рендеринг контроллера
	 */
	public function render() : void
	{
		// Публикации новостей
		$news = PublicationQuery::create()
			->useSectionQuery()
				->filterByCode('partner-news')
			->endUse()
			//->orderByHits(Criteria::DESC)
			->orderByCreatedAt(Criteria::DESC)
			->limit(5)
			->find();
			
		// Публикации финансов
		$finance = PublicationQuery::create()
			->useSectionQuery()
				->filterByCode('finance')
			->endUse()
			->orderByShowAt(Criteria::DESC)
			->limit(3)
			->find();
			
		// Публикации механика бизнеса
		$mech = PublicationQuery::create()
			->useSectionQuery()
				->filterByCode('mehanika-biznesa')
			->endUse()
			->orderByShowAt(Criteria::DESC)
			->limit(3)
			->find();
			
		// Публикации среда обитания
		$wed = PublicationQuery::create()
			->useSectionQuery()
				->filterByCode('sreda-obitania')
			->endUse()
			->orderByShowAt(Criteria::DESC)
			->limit(3)
			->find();
		
		// Публикация персона
		$person = PublicationQuery::create()
			->useSectionQuery()
				->filterByCode('persona')
			->endUse()
			->orderByShowAt(Criteria::DESC)
			->limit(3)
			->find();
		
		// Проекты
		$projects = SectionQuery::create()
			->filterByCode('project-%', Criteria::LIKE)
			//->filterByCreatedAt(['min' => new DateTime('7 days ago')])
			->orderByCreatedAt(Criteria::DESC)
			->limit(3)
			->find();
			
		// Менеджмент
		$management = PublicationQuery::create()
			->useSectionQuery()
				->filterByCode('menedzment')
			->endUse()
			->orderByShowAt(Criteria::DESC)
			->limit(3)
			->find();
			
		// Деловая афиша
		$business = PublicationQuery::create()
			->useSectionQuery()
				->filterByCode('delovaya-afisha')
			->endUse()
			->orderByShowAt(Criteria::DESC)
			->limit(3)
			->find();
			
		// Власть
		$power = PublicationQuery::create()
			->useSectionQuery()
				->filterByCode('vlast')
			->endUse()
			->orderByShowAt(Criteria::DESC)
			->limit(3)
			->find();
		
		// Топ публикаций за день
		$publicationsTopNow = PublicationQuery::create()
			->filterByCreatedAt(['min' => new DateTime('1 days ago')])
			->orderByHits(Criteria::DESC)
			->limit(5)
			->find();
			
		if (count($publicationsTopNow) < 5)
		{
			$publicationsTopNow = PublicationQuery::create()
				->orderByCreatedAt(Criteria::DESC)
				->limit(5)
				->find();
		}
			
		// Топ публикаций за неделю
		$publicationsTopWeek = PublicationQuery::create()
			->filterByCreatedAt(['min' => new DateTime('7 days ago')])
			->orderByHits(Criteria::DESC)
			->limit(5)
			->find();
		
		if (count($publicationsTopWeek) < 5)
		{
			$publicationsTopWeek = PublicationQuery::create()
				->filterByCreatedAt(['min' => new DateTime('7 days ago'), 'max' => new DateTime('1 days ago')])
				->orderByCreatedAt(Criteria::DESC)
				->limit(5)
				->find();
		}
			
		// Топ публикаций за месяц
		$publicationsTopMonth = PublicationQuery::create()
			->filterByCreatedAt(['min' => new DateTime('30 days ago')])
			->orderByHits(Criteria::DESC)
			->limit(5)
			->find();
			
		if (count($publicationsTopMonth) < 5)
		{
			$publicationsTopMonth = PublicationQuery::create()
				->filterByCreatedAt(['min' => new DateTime('30 days ago'), 'max' => new DateTime('7 days ago')])
				->orderByCreatedAt(Criteria::DESC)
				->limit(5)
				->find();
		}
		
		// БЖ рекомендует
		$recommendedByUs = PublicationQuery::create()
			->useSectionQuery()
				->filterByCode('bz-rekomenduet')
			->endUse()
			->orderByCreatedAt(Criteria::DESC)
			->orderByHits(Criteria::DESC)
			->findOne();
			
		// Публикации с тегом видео
		$video = TagQuery::create()->findOneByCode('video')->getPublications();
		
		$publications = PublicationQuery::create()
			->useSectionQuery()
				->filterByCode('commercial-property', Criteria::NOT_EQUAL)
			->endUse()
			->orderByShowAt(Criteria::DESC)
			->limit(10)
			->find();

		// Все теги
		$tags = TagQuery::create()->filterByCodeOrderByPublicationCreatedAt('video', '<>');
		
		$data['wed'] = $wed;

		$data['news'] = $news;
		
		$data['mech'] = $mech;
		
		$data['tags'] = $tags;
		
		$data['power'] = $power;
		
		$data['video'] = $video;

		$data['person'] = $person;
		
		$data['finance'] = $finance;
		
		$data['business'] = $business;

		$data['projects'] = $projects;
		
		$data['management'] = $management;
		
		$data['publications'] = $publications;

		$data['recommendedByUs'] = $recommendedByUs;

		$data['publicationsTopNow'] = $publicationsTopNow;

		$data['publicationsTopWeek'] = $publicationsTopWeek;

		$data['publicationsTopMonth'] = $publicationsTopMonth;

		$content = fenric('view::home', $data);

		$this->response->setContent($content->render());
	}
}
