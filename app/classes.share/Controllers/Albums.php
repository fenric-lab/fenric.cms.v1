<?php

namespace Fenric\Controllers;

/**
 * Import classes
 */
use Propel\Models\AlbumQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use Fenric\Controllers\Abstractable;

/**
 * Albums
 */
class Albums extends Abstractable
{

	/**
	 * Предварительная инициализация контроллера
	 */
	public function preInit() : bool
	{
		if ($this->request->query->exists('page')) {
			if (! ctype_digit($this->request->query->get('page'))) {
				$this->response->setStatus(400);
				return false;
			}
		}

		if ($this->request->query->exists('limit')) {
			if (! ctype_digit($this->request->query->get('limit'))) {
				$this->response->setStatus(400);
				return false;
			}
		}

		return true;
	}

	/**
	 * Рендеринг контроллера
	 */
	public function render() : void
	{
		$query = AlbumQuery::create();
		$query->filterByStatus('published');
		$query->orderById(Criteria::DESC);

		if ($this->request->query->exists('page')) {
			if ($this->request->query->get('page') >= 1) {
				if ($this->request->query->get('page') <= PHP_INT_MAX) {
					$page = $this->request->query->get('page');
				}
			}
		}

		if ($this->request->query->exists('limit')) {
			if ($this->request->query->get('limit') >= 1) {
				if ($this->request->query->get('limit') <= 100) {
					$limit = $this->request->query->get('limit');
				}
			}
		}

		$albums = $query->paginate($page ?? 1, $limit ?? fenric('parameter::albums-per-page') ?: 25);

		$content = fenric('view::albums', ['albums' => $albums]);

		$this->response->setContent($content->render());
	}
}
