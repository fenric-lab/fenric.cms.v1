<?php

namespace Fenric\Controllers;

/**
 * Import classes
 */
use Propel\Models\AlbumQuery;
use Fenric\Controllers\Abstractable;

/**
 * Album
 */
class Album extends Abstractable
{

	/**
	 * Предварительная инициализация контроллера
	 */
	public function preInit() : bool
	{
		if (! AlbumQuery::existsByCode($this->request->parameters->get('code'))) {
			$this->response->setStatus(404);
			return false;
		}

		return true;
	}

	/**
	 * Рендеринг контроллера
	 */
	public function render() : void
	{
		$album = AlbumQuery::create()->findOneByCode($this->request->parameters->get('code'));

		$content = fenric('view::album');

		if (fenric(sprintf('view::albums/%s', $this->request->parameters->get('code')))->exists()) {
			$content = fenric(sprintf('view::albums/%s', $this->request->parameters->get('code')));
		}

		$this->response->setContent($content->render([
			'album' => $album,
		]));
	}
}
