<?php

namespace Fenric\Controllers\Sitemaps;

/**
 * Import classes
 */
use DateTime;
use SimpleXMLElement;
use Propel\Models\Map\SectionTableMap;
use Fenric\Controllers\Abstractable;

/**
 * Sections
 */
class Sections extends Abstractable
{

	/**
	 * Рендеринг контроллера
	 */
	public function render() : void
	{
		$blank = '<?xml version="1.0" encoding="UTF-8"?>';
		$blank .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" />';

		$sitemap = new SimpleXMLElement($blank);

		$query = fenric('query')
		->cache(600)
		->select(SectionTableMap::COL_CODE)
		->select(SectionTableMap::COL_UPDATED_AT)
		->from(SectionTableMap::TABLE_NAME)
		->order(SectionTableMap::COL_ID)
		->asc();

		if ($rows = $query->toArray())
		{
			foreach ($rows as $row)
			{
				$loc = '%s://%s/%s/';
				$lastmod = new DateTime($row['updated_at']);

				$url = $sitemap->addChild('url');
				$url->addChild('loc', sprintf($loc, $this->request->getScheme(), $this->request->getHost(), $row['code']));
				$url->addChild('lastmod', $lastmod->format(DateTime::W3C));
				$url->addChild('changefreq', 'monthly');
				$url->addChild('priority', '1.0');
			}
		}

		$this->response->setHeader('Content-type: text/xml');
		$this->response->setContent($sitemap->asXML());
	}
}
