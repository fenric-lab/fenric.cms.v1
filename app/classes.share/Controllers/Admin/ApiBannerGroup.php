<?php

namespace Fenric\Controllers\Admin;

/**
 * Import classes
 */
use Propel\Models\BannerGroup;
use Propel\Models\BannerGroupQuery;
use Propel\Models\Map\BannerGroupTableMap;
use Propel\Runtime\ActiveQuery\Criteria;
use Fenric\Controllers\Abstractable\CRUD;

/**
 * ApiBannerGroup
 */
class ApiBannerGroup extends CRUD
{

	/**
	 * Доступ к контроллеру
	 */
	use Access;

	/**
	 * Создание объекта
	 */
	protected function actionCreateViaPOST() : void
	{
		if (strlen($this->request->post->get('code')) === 0) {
			if (strlen($this->request->post->get('title')) > 0) {
				$this->request->post->set('code',
					sluggable($this->request->post->get('title')));
			}
		}

		parent::create(new BannerGroup(), [
			BannerGroupTableMap::COL_CODE => $this->request->post->get('code'),
			BannerGroupTableMap::COL_TITLE => $this->request->post->get('title'),
		]);
	}

	/**
	 * Обновление объекта
	 */
	protected function actionUpdateViaPATCH() : void
	{
		parent::update(BannerGroupQuery::create(), [
			BannerGroupTableMap::COL_CODE => $this->request->post->get('code'),
			BannerGroupTableMap::COL_TITLE => $this->request->post->get('title'),
		]);
	}

	/**
	 * Удаление объекта
	 */
	protected function actionDeleteViaDELETE() : void
	{
		parent::delete(BannerGroupQuery::create());
	}

	/**
	 * Чтение объекта
	 */
	protected function actionReadViaGET() : void
	{
		parent::read(BannerGroupQuery::create(), [
			BannerGroupTableMap::COL_ID,
			BannerGroupTableMap::COL_CODE,
			BannerGroupTableMap::COL_TITLE,
		]);
	}

	/**
	 * Выгрузка объектов
	 */
	protected function actionAllViaGET() : void
	{
		fenric()->callSharedService('event', [self::EVENT_PREPARE_ITEM])->subscribe(function($model, & $item)
		{
			$item['banners'] = $model->getCountBanners();
		});

		$query = BannerGroupQuery::create();
		$query->orderById(Criteria::DESC);

		parent::all($query, [
			BannerGroupTableMap::COL_ID,
			BannerGroupTableMap::COL_CODE,
			BannerGroupTableMap::COL_TITLE,
			BannerGroupTableMap::COL_CREATED_AT,
			BannerGroupTableMap::COL_UPDATED_AT,
		]);
	}

	/**
	 * Простая выгрузка объектов
	 */
	protected function actionUnloadViaGet()
	{
		$this->response->setJsonContent(
			fenric('query')
				->select('id', 'title')
				->from('banner_group')
				->toArray()
		);
	}
}
