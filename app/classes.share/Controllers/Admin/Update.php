<?php

namespace Fenric\Controllers\Admin;

/**
 * Import classes
 */
use Fenric\Controllers\Abstractable\Abstractable;

/**
 * Update
 */
class Update extends Abstractable
{

	/**
	 * Доступ к контроллеру
	 */
	use Access;

	/**
	 * Рендеринг контроллера
	 */
	public function render() : void
	{
		set_time_limit(0);

		$content = '';

		$content .= $this->execute(fenric()->path('.'), fenric('config::environments')->get('git') . ' pull ' . fenric('config::app')->get('repository') . ' master');
		$content .= PHP_EOL . '--------------------' . PHP_EOL . PHP_EOL;

		if (file_exists(fenric()->path('.', 'composer'))) {
			$content .= $this->execute(fenric()->path('.'), fenric('config::environments')->get('php') . ' composer update');
			$content .= PHP_EOL . '--------------------' . PHP_EOL . PHP_EOL;
		}

		if (file_exists(fenric()->path('.', 'composer.phar'))) {
			$content .= $this->execute(fenric()->path('.'), fenric('config::environments')->get('php') . ' composer.phar update');
			$content .= PHP_EOL . '--------------------' . PHP_EOL . PHP_EOL;
		}

		$content .= $this->execute(fenric()->path('app', 'bin'), fenric('config::environments')->get('php') . ' propel migrate');
		$content .= PHP_EOL . '--------------------' . PHP_EOL . PHP_EOL . 'Done.';

		$this->response->setHeader('Content-Type: text/plain; charset=UTF-8');
		$this->response->setContent($content);
	}

	/**
	 * Выполнение команды
	 */
	protected function execute(string $folder, string $command) : string
	{
		$result = '$' . $command . PHP_EOL . PHP_EOL;

		$process = proc_open($command . ' 2>&1', [['pipe', 'r'], ['pipe', 'w'], ['pipe', 'w']], $pipes, $folder);

		if (is_resource($process))
		{
			if (is_resource($pipes[1]))
			{
				$stdout = stream_get_contents($pipes[1]);

				if (strlen($stdout = trim($stdout)) > 0)
				{
					$result .= $stdout . PHP_EOL;
				}

				fclose($pipes[1]);
			}

			if (is_resource($pipes[2]))
			{
				$stderr = stream_get_contents($pipes[2]);

				if (strlen($stderr = trim($stderr)) > 0)
				{
					$result .= $stderr . PHP_EOL;
				}

				fclose($pipes[2]);
			}

			proc_close($process);
		}

		return $result;
	}
}
