<?php

namespace Fenric\Controllers\Admin;

/**
 * Import classes
 */
use Propel\Models\Banner;
use Propel\Models\BannerQuery;
use Propel\Models\Map\BannerTableMap;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Fenric\Controllers\Abstractable\CRUD;

/**
 * ApiBanner
 */
class ApiBanner extends CRUD
{

	/**
	 * Доступ к контроллеру
	 */
	use Access;

	/**
	 * Создание объекта
	 */
	protected function actionCreateViaPOST() : void
	{
		parent::create(new Banner(), [
			BannerTableMap::COL_BANNER_GROUP_ID => $this->request->post->get('banner_group_id'),
			BannerTableMap::COL_TITLE => $this->request->post->get('title'),
			BannerTableMap::COL_DESCRIPTION => $this->request->post->get('description'),
			BannerTableMap::COL_PICTURE => $this->request->post->get('picture'),
			BannerTableMap::COL_PICTURE_ALT => $this->request->post->get('picture_alt'),
			BannerTableMap::COL_PICTURE_TITLE => $this->request->post->get('picture_title'),
			BannerTableMap::COL_HYPERLINK => $this->request->post->get('hyperlink'),
			BannerTableMap::COL_HYPERLINK_TARGET => $this->request->post->get('hyperlink_target'),
			BannerTableMap::COL_SHOW_START => $this->request->post->get('show_start'),
			BannerTableMap::COL_SHOW_END => $this->request->post->get('show_end'),
			BannerTableMap::COL_SHOWS_LIMIT => $this->request->post->get('shows_limit'),
			BannerTableMap::COL_CLICKS_LIMIT => $this->request->post->get('clicks_limit'),
		]);
	}

	/**
	 * Обновление объекта
	 */
	protected function actionUpdateViaPATCH() : void
	{
		parent::update(BannerQuery::create(), [
			BannerTableMap::COL_BANNER_GROUP_ID => $this->request->post->get('banner_group_id'),
			BannerTableMap::COL_TITLE => $this->request->post->get('title'),
			BannerTableMap::COL_DESCRIPTION => $this->request->post->get('description'),
			BannerTableMap::COL_PICTURE => $this->request->post->get('picture'),
			BannerTableMap::COL_PICTURE_ALT => $this->request->post->get('picture_alt'),
			BannerTableMap::COL_PICTURE_TITLE => $this->request->post->get('picture_title'),
			BannerTableMap::COL_HYPERLINK => $this->request->post->get('hyperlink'),
			BannerTableMap::COL_HYPERLINK_TARGET => $this->request->post->get('hyperlink_target'),
			BannerTableMap::COL_SHOW_START => $this->request->post->get('show_start'),
			BannerTableMap::COL_SHOW_END => $this->request->post->get('show_end'),
			BannerTableMap::COL_SHOWS_LIMIT => $this->request->post->get('shows_limit'),
			BannerTableMap::COL_CLICKS_LIMIT => $this->request->post->get('clicks_limit'),
		]);
	}

	/**
	 * Удаление объекта
	 */
	protected function actionDeleteViaDELETE() : void
	{
		parent::delete(BannerQuery::create());
	}

	/**
	 * Чтение объекта
	 */
	protected function actionReadViaGET() : void
	{
		parent::read(BannerQuery::create(), [
			BannerTableMap::COL_ID,
			BannerTableMap::COL_BANNER_GROUP_ID,
			BannerTableMap::COL_TITLE,
			BannerTableMap::COL_DESCRIPTION,
			BannerTableMap::COL_PICTURE,
			BannerTableMap::COL_PICTURE_ALT,
			BannerTableMap::COL_PICTURE_TITLE,
			BannerTableMap::COL_HYPERLINK,
			BannerTableMap::COL_HYPERLINK_TARGET,
			BannerTableMap::COL_SHOW_START,
			BannerTableMap::COL_SHOW_END,
			BannerTableMap::COL_SHOWS_LIMIT,
			BannerTableMap::COL_CLICKS_LIMIT,
		]);
	}

	/**
	 * Выгрузка объектов
	 */
	protected function actionAllViaGET() : void
	{
		fenric()->callSharedService('event', [self::EVENT_PREPARE_ITEM])->subscribe(function($model, & $item)
		{
			$item['group'] = [];

			if ($model->getBannerGroup() instanceof ActiveRecordInterface)
			{
				$item['group']['id'] = $model->getBannerGroup()->getId();
				$item['group']['code'] = $model->getBannerGroup()->getCode();
				$item['group']['title'] = $model->getBannerGroup()->getTitle();
			}
		});

		$query = BannerQuery::create();
		$query->orderById(Criteria::DESC);

		parent::all($query, [
			BannerTableMap::COL_ID,
			BannerTableMap::COL_TITLE,
			BannerTableMap::COL_DESCRIPTION,
			BannerTableMap::COL_PICTURE,
			BannerTableMap::COL_HYPERLINK,
			BannerTableMap::COL_HYPERLINK_TARGET,
			BannerTableMap::COL_SHOW_START,
			BannerTableMap::COL_SHOW_END,
			BannerTableMap::COL_SHOWS,
			BannerTableMap::COL_SHOWS_LIMIT,
			BannerTableMap::COL_CLICKS,
			BannerTableMap::COL_CLICKS_LIMIT,
			BannerTableMap::COL_CREATED_AT,
			BannerTableMap::COL_UPDATED_AT,
		]);
	}
}
