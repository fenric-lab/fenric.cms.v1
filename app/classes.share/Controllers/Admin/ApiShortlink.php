<?php

namespace Fenric\Controllers\Admin;

/**
 * Import classes
 */
use Propel\Models\Shortlink;
use Propel\Models\ShortlinkQuery;
use Propel\Models\Map\ShortlinkTableMap;

use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Fenric\Controllers\Abstractable\CRUD;

/**
 * ApiShortlink
 */
class ApiShortlink extends CRUD
{

	/**
	 * Доступ к контроллеру
	 */
	use Access;

	/**
	 * Создание объекта
	 */
	protected function actionCreateViaPOST() : void
	{
		parent::create(new Shortlink(), [
			ShortlinkTableMap::COL_TITLE => $this->request->post->get('title'),
			ShortlinkTableMap::COL_LOCATION => $this->request->post->get('location'),
		]);
	}

	/**
	 * Обновление объекта
	 */
	protected function actionUpdateViaPATCH() : void
	{
		parent::update(ShortlinkQuery::create(), [
			ShortlinkTableMap::COL_TITLE => $this->request->post->get('title'),
			ShortlinkTableMap::COL_LOCATION => $this->request->post->get('location'),
		]);
	}

	/**
	 * Удаление объекта
	 */
	protected function actionDeleteViaDELETE() : void
	{
		parent::delete(ShortlinkQuery::create());
	}

	/**
	 * Чтение объекта
	 */
	protected function actionReadViaGET() : void
	{
		parent::read(ShortlinkQuery::create(), [
			ShortlinkTableMap::COL_ID,
			ShortlinkTableMap::COL_TITLE,
			ShortlinkTableMap::COL_LOCATION,
		]);
	}

	/**
	 * Выгрузка объектов
	 */
	protected function actionAllViaGET() : void
	{
		fenric()->callSharedService('event', [self::EVENT_PREPARE_ITEM])->subscribe(function($item, & $json)
		{
			$json['uri'] = $item->getUri();

			$json['creator'] = [];
			$json['updater'] = [];

			if ($item->getUserRelatedByCreatedBy() instanceof ActiveRecordInterface) {
				$json['creator']['id'] = $item->getUserRelatedByCreatedBy()->getId();
				$json['creator']['username'] = $item->getUserRelatedByCreatedBy()->getUsername();
			}

			if ($item->getUserRelatedByUpdatedBy() instanceof ActiveRecordInterface) {
				$json['updater']['id'] = $item->getUserRelatedByUpdatedBy()->getId();
				$json['updater']['username'] = $item->getUserRelatedByUpdatedBy()->getUsername();
			}
		});

		$query = ShortlinkQuery::create();

		if (ctype_digit($this->request->query->get('id'))) {
			$query->filterById($this->request->query->get('id'));
		}

		if (ctype_alnum($this->request->query->get('code'))) {
			$query->filterByCode($this->request->query->get('code'));
		}

		switch ($this->request->query->get('sort'))
		{
			case 'id_asc' :
				$query->orderById(Criteria::ASC);
				break;

			case 'id_desc' :
				$query->orderById(Criteria::DESC);
				break;

			case 'created_at_asc' :
				$query->orderByCreatedAt(Criteria::ASC);
				break;

			case 'created_at_desc' :
				$query->orderByCreatedAt(Criteria::DESC);
				break;

			case 'updated_at_asc' :
				$query->orderByUpdatedAt(Criteria::ASC);
				break;

			case 'updated_at_desc' :
				$query->orderByUpdatedAt(Criteria::DESC);
				break;
		}

		parent::all($query, [
			ShortlinkTableMap::COL_ID,
			ShortlinkTableMap::COL_TITLE,
			ShortlinkTableMap::COL_LOCATION,
			ShortlinkTableMap::COL_REFERRALS,
			ShortlinkTableMap::COL_CREATED_AT,
			ShortlinkTableMap::COL_UPDATED_AT,
		]);
	}
}
