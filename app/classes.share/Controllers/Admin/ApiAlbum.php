<?php

namespace Fenric\Controllers\Admin;

/**
 * Import classes
 */
use Propel\Models\Album;
use Propel\Models\Photo;
use Propel\Models\AlbumQuery;
use Propel\Models\PhotoQuery;
use Propel\Models\Map\AlbumTableMap;
use Propel\Models\Map\PhotoTableMap;
use Propel\Runtime\ActiveQuery\Criteria;
use Fenric\Controllers\Abstractable\CRUD;

/**
 * ApiAlbum
 */
class ApiAlbum extends CRUD
{

	/**
	 * Доступ к контроллеру
	 */
	use Access;

	/**
	 * Создание объекта
	 */
	protected function actionCreateViaPOST() : void
	{
		if (strlen($this->request->post->get('code')) === 0) {
			if (strlen($this->request->post->get('header')) > 0) {
				$this->request->post->set('code',
					sluggable($this->request->post->get('header')));
			}
		}

		parent::create(new Album(), [
			AlbumTableMap::COL_CODE => $this->request->post->get('code'),
			AlbumTableMap::COL_HEADER => $this->request->post->get('header'),
			AlbumTableMap::COL_CONTENT => $this->request->post->get('content'),
			AlbumTableMap::COL_META_TITLE => $this->request->post->get('meta_title'),
			AlbumTableMap::COL_META_AUTHOR => $this->request->post->get('meta_author'),
			AlbumTableMap::COL_META_KEYWORDS => $this->request->post->get('meta_keywords'),
			AlbumTableMap::COL_META_DESCRIPTION => $this->request->post->get('meta_description'),
			AlbumTableMap::COL_META_ROBOTS => $this->request->post->get('meta_robots'),
			AlbumTableMap::COL_STATUS => $this->request->post->get('status'),
		]);
	}

	/**
	 * Обновление объекта
	 */
	protected function actionUpdateViaPATCH() : void
	{
		parent::update(AlbumQuery::create(), [
			AlbumTableMap::COL_CODE => $this->request->post->get('code'),
			AlbumTableMap::COL_HEADER => $this->request->post->get('header'),
			AlbumTableMap::COL_CONTENT => $this->request->post->get('content'),
			AlbumTableMap::COL_META_TITLE => $this->request->post->get('meta_title'),
			AlbumTableMap::COL_META_AUTHOR => $this->request->post->get('meta_author'),
			AlbumTableMap::COL_META_KEYWORDS => $this->request->post->get('meta_keywords'),
			AlbumTableMap::COL_META_DESCRIPTION => $this->request->post->get('meta_description'),
			AlbumTableMap::COL_META_ROBOTS => $this->request->post->get('meta_robots'),
			AlbumTableMap::COL_STATUS => $this->request->post->get('status'),
		]);
	}

	/**
	 * Удаление объекта
	 */
	protected function actionDeleteViaDELETE() : void
	{
		parent::delete(AlbumQuery::create());
	}

	/**
	 * Чтение объекта
	 */
	protected function actionReadViaGET() : void
	{
		parent::read(AlbumQuery::create(), [
			AlbumTableMap::COL_ID,
			AlbumTableMap::COL_CODE,
			AlbumTableMap::COL_HEADER,
			AlbumTableMap::COL_CONTENT,
			AlbumTableMap::COL_META_TITLE,
			AlbumTableMap::COL_META_AUTHOR,
			AlbumTableMap::COL_META_KEYWORDS,
			AlbumTableMap::COL_META_DESCRIPTION,
			AlbumTableMap::COL_META_ROBOTS,
			AlbumTableMap::COL_STATUS,
		]);
	}

	/**
	 * Выгрузка объектов
	 */
	protected function actionAllViaGET() : void
	{
		fenric()->callSharedService('event', [self::EVENT_PREPARE_ITEM])->subscribe(function($model, & $item)
		{
			$item['cover'] = $model->getCover();
			$item['photos'] = $model->getCountPhotos();
		});

		$query = AlbumQuery::create();
		$query->orderById(Criteria::DESC);

		parent::all($query, [
			AlbumTableMap::COL_ID,
			AlbumTableMap::COL_CODE,
			AlbumTableMap::COL_HEADER,
			AlbumTableMap::COL_META_TITLE,
			AlbumTableMap::COL_META_AUTHOR,
			AlbumTableMap::COL_META_KEYWORDS,
			AlbumTableMap::COL_META_DESCRIPTION,
			AlbumTableMap::COL_META_ROBOTS,
			AlbumTableMap::COL_CREATED_AT,
			AlbumTableMap::COL_UPDATED_AT,
			AlbumTableMap::COL_STATUS,
		]);
	}

	/**
	 * Выгрузка дочерних объектов
	 */
	protected function actionChildrenViaGET() : void
	{
		$query = PhotoQuery::create();
		$query->orderBySequence(Criteria::ASC);

		$query->filterByAlbumId(
			$this->request->parameters->get('id')
		);

		$columns = [
			PhotoTableMap::COL_ID,
			PhotoTableMap::COL_FILE,
		];

		$options = [
			'limit' => PHP_INT_MAX,
		];

		parent::all($query, $columns, $options);
	}

	/**
	 * Простая выгрузка объектов
	 */
	protected function actionUnloadViaGet()
	{
		$this->response->setJsonContent(
			fenric('query')
				->select('id', 'header')
				->from('album')
				->toArray()
		);
	}
}
