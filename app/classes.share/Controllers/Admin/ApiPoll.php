<?php

namespace Fenric\Controllers\Admin;

/**
 * Import classes
 */
use Propel\Models\Poll;
use Propel\Models\PollQuery;
use Propel\Models\Map\PollTableMap;
use Propel\Runtime\ActiveQuery\Criteria;
use Fenric\Controllers\Abstractable\CRUD;

/**
 * ApiPoll
 */
class ApiPoll extends CRUD
{

	/**
	 * Доступ к контроллеру
	 */
	use Access;

	/**
	 * Создание объекта
	 */
	protected function actionCreateViaPOST() : void
	{
		if (strlen($this->request->post->get('code')) === 0) {
			if (strlen($this->request->post->get('title')) > 0) {
				$slug = sluggable($this->request->post->get('title'));
				$this->request->post->set('code', $slug);
			}
		}

		parent::create(new Poll(), [
			PollTableMap::COL_CODE => $this->request->post->get('code'),
			PollTableMap::COL_TITLE => $this->request->post->get('title'),
			PollTableMap::COL_MULTIPLE => $this->request->post->get('multiple'),
			PollTableMap::COL_OPEN_AT => $this->request->post->get('open_at'),
			PollTableMap::COL_CLOSE_AT => $this->request->post->get('close_at'),
		]);
	}

	/**
	 * Обновление объекта
	 */
	protected function actionUpdateViaPATCH() : void
	{
		parent::update(PollQuery::create(), [
			PollTableMap::COL_CODE => $this->request->post->get('code'),
			PollTableMap::COL_TITLE => $this->request->post->get('title'),
			PollTableMap::COL_MULTIPLE => $this->request->post->get('multiple'),
			PollTableMap::COL_OPEN_AT => $this->request->post->get('open_at'),
			PollTableMap::COL_CLOSE_AT => $this->request->post->get('close_at'),
		]);
	}

	/**
	 * Удаление объекта
	 */
	protected function actionDeleteViaDELETE() : void
	{
		parent::delete(PollQuery::create());
	}

	/**
	 * Чтение объекта
	 */
	protected function actionReadViaGET() : void
	{
		parent::read(PollQuery::create(), [
			PollTableMap::COL_ID,
			PollTableMap::COL_CODE,
			PollTableMap::COL_TITLE,
			PollTableMap::COL_MULTIPLE,
			PollTableMap::COL_OPEN_AT,
			PollTableMap::COL_CLOSE_AT,
		]);
	}

	/**
	 * Выгрузка объектов
	 */
	protected function actionAllViaGET() : void
	{
		fenric()->callSharedService('event', [self::EVENT_PREPARE_ITEM])->subscribe(function($model, & $item) {
			$item['votes'] = $model->getCountVotes();
			$item['variants'] = $model->getCountVariants();
		});

		$query = PollQuery::create();
		$query->orderById(Criteria::DESC);

		parent::all($query, [
			PollTableMap::COL_ID,
			PollTableMap::COL_CODE,
			PollTableMap::COL_TITLE,
			PollTableMap::COL_MULTIPLE,
			PollTableMap::COL_OPEN_AT,
			PollTableMap::COL_CLOSE_AT,
			PollTableMap::COL_CREATED_AT,
			PollTableMap::COL_UPDATED_AT,
		]);
	}

	/**
	 * Простая выгрузка объектов
	 */
	protected function actionUnloadViaGet() : void
	{
		$this->response->setJsonContent(
			fenric('query')
				->select('id', 'title')
				->from('poll')
			->toArray()
		);
	}
}
