<?php

namespace Fenric\Controllers\Admin;

/**
 * Import classes
 */
use DateTime;
use Propel\Models\Publication;
use Propel\Models\PublicationQuery;
use Propel\Models\Map\PublicationTableMap;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\ObjectCollection;
use Fenric\Controllers\Abstractable\CRUD;

/**
 * ApiPublication
 */
class ApiPublication extends CRUD
{

	/**
	 * Доступ к контроллеру
	 */
	use Access;

	/**
	 * Создание объекта
	 */
	protected function actionCreateViaPOST() : void
	{
		if (strlen($this->request->post->get('code')) === 0) {
			if (strlen($this->request->post->get('header')) > 0) {
				$this->request->post->set('code',
					sluggable($this->request->post->get('header')));
			}
		}

		fenric()->callSharedService('event', [self::EVENT_BEFORE_SAVE])->subscribe(function(ActiveRecordInterface $model) : void
		{
			$model->attachTags($this->data['tags'] ?? []);
		});

		parent::create(new Publication(), [
			PublicationTableMap::COL_SECTION_ID => $this->request->post->get('section_id'),
			PublicationTableMap::COL_HOT => $this->request->post->get('hot'),
			PublicationTableMap::COL_CODE => $this->request->post->get('code'),
			PublicationTableMap::COL_HEADER => $this->request->post->get('header'),
			PublicationTableMap::COL_PICTURE => $this->request->post->get('picture'),
			PublicationTableMap::COL_PICTURE_SIGNATURE => $this->request->post->get('picture_signature'),
			PublicationTableMap::COL_ANONS => $this->request->post->get('anons'),
			PublicationTableMap::COL_CONTENT => $this->request->post->get('content'),
			PublicationTableMap::COL_META_TITLE => $this->request->post->get('meta_title'),
			PublicationTableMap::COL_META_AUTHOR => $this->request->post->get('meta_author'),
			PublicationTableMap::COL_META_KEYWORDS => $this->request->post->get('meta_keywords'),
			PublicationTableMap::COL_META_DESCRIPTION => $this->request->post->get('meta_description'),
			PublicationTableMap::COL_META_ROBOTS => $this->request->post->get('meta_robots'),
			PublicationTableMap::COL_SHOW_AT => $this->request->post->get('show_at'),
			PublicationTableMap::COL_HIDE_AT => $this->request->post->get('hide_at'),
		]);
	}

	/**
	 * Обновление объекта
	 */
	protected function actionUpdateViaPATCH() : void
	{
		fenric()->callSharedService('event', [self::EVENT_BEFORE_SAVE])->subscribe(function(ActiveRecordInterface $model) : void
		{
			$model->attachTags($this->data['tags'] ?? []);
		});

		parent::update(PublicationQuery::create(), [
			PublicationTableMap::COL_SECTION_ID => $this->request->post->get('section_id'),
			PublicationTableMap::COL_HOT => $this->request->post->get('hot'),
			PublicationTableMap::COL_CODE => $this->request->post->get('code'),
			PublicationTableMap::COL_HEADER => $this->request->post->get('header'),
			PublicationTableMap::COL_PICTURE => $this->request->post->get('picture'),
			PublicationTableMap::COL_PICTURE_SIGNATURE => $this->request->post->get('picture_signature'),
			PublicationTableMap::COL_ANONS => $this->request->post->get('anons'),
			PublicationTableMap::COL_CONTENT => $this->request->post->get('content'),
			PublicationTableMap::COL_META_TITLE => $this->request->post->get('meta_title'),
			PublicationTableMap::COL_META_AUTHOR => $this->request->post->get('meta_author'),
			PublicationTableMap::COL_META_KEYWORDS => $this->request->post->get('meta_keywords'),
			PublicationTableMap::COL_META_DESCRIPTION => $this->request->post->get('meta_description'),
			PublicationTableMap::COL_META_ROBOTS => $this->request->post->get('meta_robots'),
			PublicationTableMap::COL_SHOW_AT => $this->request->post->get('show_at'),
			PublicationTableMap::COL_HIDE_AT => $this->request->post->get('hide_at'),
		]);
	}

	/**
	 * Удаление объекта
	 */
	protected function actionDeleteViaDELETE() : void
	{
		parent::delete(PublicationQuery::create());
	}

	/**
	 * Чтение объекта
	 */
	protected function actionReadViaGET() : void
	{
		fenric()->callSharedService('event', [self::EVENT_PREPARE_ITEM])->subscribe(function($model, & $item)
		{
			$item['attached_tags'] = [];

			if ($model->getPublicationTags() instanceof ObjectCollection)
			{
				foreach ($model->getPublicationTags() as $publicationTag)
				{
					$item['attached_tags'][] = $publicationTag->getTagId();
				}
			}
		});

		parent::read(PublicationQuery::create(), [
			PublicationTableMap::COL_ID,
			PublicationTableMap::COL_SECTION_ID,
			PublicationTableMap::COL_HOT,
			PublicationTableMap::COL_CODE,
			PublicationTableMap::COL_HEADER,
			PublicationTableMap::COL_PICTURE,
			PublicationTableMap::COL_PICTURE_SIGNATURE,
			PublicationTableMap::COL_ANONS,
			PublicationTableMap::COL_CONTENT,
			PublicationTableMap::COL_META_TITLE,
			PublicationTableMap::COL_META_AUTHOR,
			PublicationTableMap::COL_META_KEYWORDS,
			PublicationTableMap::COL_META_DESCRIPTION,
			PublicationTableMap::COL_META_ROBOTS,
			PublicationTableMap::COL_SHOW_AT,
			PublicationTableMap::COL_HIDE_AT,
		]);
	}

	/**
	 * Выгрузка объектов
	 */
	protected function actionAllViaGET() : void
	{
		fenric()->callSharedService('event', [self::EVENT_PREPARE_ITEM])->subscribe(function($model, & $item)
		{
			$item['tags'] = [];
			$item['section'] = [];
			$item['disabled'] = false;

			if ($model->getPublicationTags() instanceof ObjectCollection) {
				foreach ($model->getPublicationTags() as $i => $publicationTag) {
					$item['tags'][$i]['id'] = $publicationTag->getTag()->getId();
					$item['tags'][$i]['code'] = $publicationTag->getTag()->getCode();
					$item['tags'][$i]['header'] = $publicationTag->getTag()->getHeader();
				}
			}

			if ($model->getSection() instanceof ActiveRecordInterface) {
				$item['section']['id'] = $model->getSection()->getId();
				$item['section']['code'] = $model->getSection()->getCode();
				$item['section']['header'] = $model->getSection()->getHeader();
			}

			if ($model->getShowAt() instanceof DateTime) {
				if ($model->getShowAt() > new DateTime('now')) {
					$item['disabled'] = true;
				}
			}

			if ($model->getHideAt() instanceof DateTime) {
				if ($model->getHideAt() < new DateTime('now')) {
					$item['disabled'] = true;
				}
			}
		});

		$query = PublicationQuery::create();
		$query->orderById(Criteria::DESC);

		if (ctype_digit($this->request->query->get('section'))) {
			$query->filterBySectionId($this->request->query->get('section'));
		}

		parent::all($query, [
			PublicationTableMap::COL_ID,
			PublicationTableMap::COL_HOT,
			PublicationTableMap::COL_CODE,
			PublicationTableMap::COL_HEADER,
			PublicationTableMap::COL_PICTURE,
			PublicationTableMap::COL_META_TITLE,
			PublicationTableMap::COL_META_AUTHOR,
			PublicationTableMap::COL_META_KEYWORDS,
			PublicationTableMap::COL_META_DESCRIPTION,
			PublicationTableMap::COL_META_ROBOTS,
			PublicationTableMap::COL_CREATED_AT,
			PublicationTableMap::COL_UPDATED_AT,
			PublicationTableMap::COL_SHOW_AT,
			PublicationTableMap::COL_HIDE_AT,
			PublicationTableMap::COL_HITS,
		]);
	}
}
