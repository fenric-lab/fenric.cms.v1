<?php

namespace Fenric\Controllers\Admin;

/**
 * Import classes
 */
use Propel\Models\Parameter;
use Propel\Models\ParameterQuery;
use Propel\Models\Map\ParameterTableMap;
use Propel\Runtime\ActiveQuery\Criteria;
use Fenric\Controllers\Abstractable\CRUD;

/**
 * ApiParameter
 */
class ApiParameter extends CRUD
{

	/**
	 * Доступ к контроллеру
	 */
	use Access;

	/**
	 * Создание объекта
	 */
	protected function actionCreateViaPOST() : void
	{
		parent::create(new Parameter(), [
			ParameterTableMap::COL_CODE => $this->request->post->get('code'),
			ParameterTableMap::COL_LABEL => $this->request->post->get('label'),
			ParameterTableMap::COL_VALUE => $this->request->post->get('value'),
		]);
	}

	/**
	 * Обновление объекта
	 */
	protected function actionUpdateViaPATCH() : void
	{
		parent::update(ParameterQuery::create(), [
			ParameterTableMap::COL_CODE => $this->request->post->get('code'),
			ParameterTableMap::COL_LABEL => $this->request->post->get('label'),
			ParameterTableMap::COL_VALUE => $this->request->post->get('value'),
		]);
	}

	/**
	 * Удаление объекта
	 */
	protected function actionDeleteViaDELETE() : void
	{
		parent::delete(ParameterQuery::create());
	}

	/**
	 * Чтение объекта
	 */
	protected function actionReadViaGET() : void
	{
		parent::read(ParameterQuery::create(), [
			ParameterTableMap::COL_ID,
			ParameterTableMap::COL_CODE,
			ParameterTableMap::COL_LABEL,
			ParameterTableMap::COL_VALUE,
		]);
	}

	/**
	 * Выгрузка объектов
	 */
	protected function actionAllViaGET() : void
	{
		$query = ParameterQuery::create();
		$query->orderById(Criteria::ASC);

		parent::all($query, [
			ParameterTableMap::COL_ID,
			ParameterTableMap::COL_CODE,
			ParameterTableMap::COL_LABEL,
		]);
	}
}
