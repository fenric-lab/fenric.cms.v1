<?php

namespace Fenric\Controllers\Admin;

/**
 * Import classes
 */
use Propel\Models\Photo;
use Propel\Models\PhotoQuery;
use Propel\Models\Map\PhotoTableMap;
use Fenric\Controllers\Abstractable\CRUD;

/**
 * ApiPhoto
 */
class ApiPhoto extends CRUD
{

	/**
	 * Доступ к контроллеру
	 */
	use Access;

	/**
	 * Создание объекта
	 */
	protected function actionCreateViaPOST() : void
	{
		parent::create(new Photo(), [
			PhotoTableMap::COL_ALBUM_ID => $this->request->post->get('album_id'),
			PhotoTableMap::COL_FILE => $this->request->post->get('file'),
		]);
	}

	/**
	 * Сортировка объектов
	 */
	protected function actionSortViaPATCH() : void
	{
		$sequence = 0;

		foreach ($this->request->post->all() as $id)
		{
			fenric('query')->update('photo', ['sequence' => ++$sequence])->where('id', '=', $id)->limit(1)->shutdown();
		}
	}

	/**
	 * Удаление объекта
	 */
	protected function actionDeleteViaDELETE() : void
	{
		parent::delete(PhotoQuery::create());
	}
}
