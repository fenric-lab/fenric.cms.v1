<?php

namespace Fenric\Controllers\Admin;

/**
 * Import classes
 */
use Propel\Models\Menu;
use Propel\Models\MenuQuery;
use Propel\Models\Map\MenuTableMap;

use Propel\Models\MenuItem;
use Propel\Models\MenuItemQuery;
use Propel\Models\Map\MenuItemTableMap;

use Propel\Runtime\ActiveQuery\Criteria;
use Fenric\Controllers\Abstractable\CRUD;

/**
 * ApiMenu
 */
class ApiMenu extends CRUD
{

	/**
	 * Доступ к контроллеру
	 */
	use Access;

	/**
	 * Создание объекта
	 */
	protected function actionCreateViaPOST() : void
	{
		if (strlen($this->request->post->get('code')) === 0) {
			if (strlen($this->request->post->get('title')) > 0) {
				$this->request->post->set('code',
					sluggable($this->request->post->get('title')));
			}
		}

		parent::create(new Menu(), [
			MenuTableMap::COL_CODE => $this->request->post->get('code'),
			MenuTableMap::COL_TITLE => $this->request->post->get('title'),
		]);
	}

	/**
	 * Обновление объекта
	 */
	protected function actionUpdateViaPATCH() : void
	{
		parent::update(MenuQuery::create(), [
			MenuTableMap::COL_CODE => $this->request->post->get('code'),
			MenuTableMap::COL_TITLE => $this->request->post->get('title'),
		]);
	}

	/**
	 * Удаление объекта
	 */
	protected function actionDeleteViaDELETE() : void
	{
		parent::delete(MenuQuery::create());
	}

	/**
	 * Чтение объекта
	 */
	protected function actionReadViaGET() : void
	{
		parent::read(MenuQuery::create(), [
			MenuTableMap::COL_ID,
			MenuTableMap::COL_CODE,
			MenuTableMap::COL_TITLE,
		]);
	}

	/**
	 * Выгрузка объектов
	 */
	protected function actionAllViaGET() : void
	{
		fenric()->callSharedService('event', [self::EVENT_PREPARE_ITEM])->subscribe(function($model, & $item)
		{
			$item['items'] = $model->getCountItems();
		});

		$query = MenuQuery::create();
		$query->orderById(Criteria::DESC);

		parent::all($query, [
			MenuTableMap::COL_ID,
			MenuTableMap::COL_CODE,
			MenuTableMap::COL_TITLE,
			MenuTableMap::COL_CREATED_AT,
			MenuTableMap::COL_UPDATED_AT,
		]);
	}

	/**
	 * Простая выгрузка объектов
	 */
	protected function actionUnloadViaGET() : void
	{
		$query = fenric('query')
			->select('id', 'title')
			->from('menu')
			->order('id')
			->desc();

		$this->response->setJsonContent($query->toArray());
	}

	/**
	 * Сортировка объектов
	 */
	protected function actionSortViaPATCH() : void
	{
		$sequence = 0;

		foreach ($this->request->post->all() as $itemId => $parentId)
		{
			fenric('query')->update('menu_item', ['parent_id' => (int) $parentId ?: null, 'sequence' => ++$sequence])->where('id', '=', $itemId)->limit(1)->shutdown();
		}
	}

	/**
	 * Выгрузка дочерних объектов
	 */
	protected function actionChildrenViaGET() : void
	{
		$query = MenuItemQuery::create();
		$query->orderBySequence(Criteria::ASC);

		$query->filterByMenuId(
			$this->request->parameters->get('id')
		);

		$columns = [
			MenuItemTableMap::COL_ID,
			MenuItemTableMap::COL_PARENT_ID,
			MenuItemTableMap::COL_CONTENT,
			MenuItemTableMap::COL_HREF,
			MenuItemTableMap::COL_TARGET,
			MenuItemTableMap::COL_DISPLAY,
			MenuItemTableMap::COL_CREATED_AT,
			MenuItemTableMap::COL_UPDATED_AT,
		];

		$options = [
			'limit' => PHP_INT_MAX,
		];

		parent::all($query, $columns, $options);
	}

	/**
	 * Создание дочернего объекта
	 */
	protected function actionCreateChildViaPOST() : void
	{
		parent::create(new MenuItem(), [
			MenuItemTableMap::COL_MENU_ID => $this->request->parameters->get('id'),
			MenuItemTableMap::COL_CONTENT => $this->request->post->get('content'),
			MenuItemTableMap::COL_HREF => $this->request->post->get('href'),
			MenuItemTableMap::COL_TARGET => $this->request->post->get('target'),
			MenuItemTableMap::COL_DISPLAY => $this->request->post->get('display'),
		]);
	}

	/**
	 * Обновление дочернего объекта
	 */
	protected function actionUpdateChildViaPATCH() : void
	{
		parent::update(MenuItemQuery::create(), [
			MenuItemTableMap::COL_CONTENT => $this->request->post->get('content'),
			MenuItemTableMap::COL_HREF => $this->request->post->get('href'),
			MenuItemTableMap::COL_TARGET => $this->request->post->get('target'),
			MenuItemTableMap::COL_DISPLAY => $this->request->post->get('display'),
		]);
	}

	/**
	 * Удаление дочернего объекта
	 */
	protected function actionDeleteChildViaDELETE() : void
	{
		parent::delete(MenuItemQuery::create());
	}

	/**
	 * Чтение дочернего объекта
	 */
	protected function actionReadChildViaGET() : void
	{
		parent::read(MenuItemQuery::create(), [
			MenuItemTableMap::COL_ID,
			MenuItemTableMap::COL_CONTENT,
			MenuItemTableMap::COL_HREF,
			MenuItemTableMap::COL_TARGET,
			MenuItemTableMap::COL_DISPLAY,
		]);
	}
}
