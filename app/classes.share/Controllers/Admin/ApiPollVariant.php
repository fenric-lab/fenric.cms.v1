<?php

namespace Fenric\Controllers\Admin;

/**
 * Import classes
 */
use Propel\Models\PollVariant;
use Propel\Models\PollVariantQuery;
use Propel\Models\Map\PollVariantTableMap;
use Propel\Runtime\ActiveQuery\Criteria;
use Fenric\Controllers\Abstractable\CRUD;

/**
 * ApiPollVariant
 */
class ApiPollVariant extends CRUD
{

	/**
	 * Доступ к контроллеру
	 */
	use Access;

	/**
	 * Создание объекта
	 */
	protected function actionCreateViaPOST() : void
	{
		parent::create(new PollVariant(), [
			PollVariantTableMap::COL_POLL_ID => $this->request->parameters->get('id'),
			PollVariantTableMap::COL_TITLE => $this->request->post->get('title'),
		]);
	}

	/**
	 * Обновление объекта
	 */
	protected function actionUpdateViaPATCH() : void
	{
		parent::update(PollVariantQuery::create(), [
			PollVariantTableMap::COL_TITLE => $this->request->post->get('title'),
		]);
	}

	/**
	 * Удаление объекта
	 */
	protected function actionDeleteViaDELETE() : void
	{
		parent::delete(PollVariantQuery::create());
	}

	/**
	 * Чтение объекта
	 */
	protected function actionReadViaGET() : void
	{
		parent::read(PollVariantQuery::create(), [
			PollVariantTableMap::COL_ID,
			PollVariantTableMap::COL_TITLE,
		]);
	}

	/**
	 * Выгрузка объектов
	 */
	protected function actionAllViaGET() : void
	{
		fenric()->callSharedService('event', [self::EVENT_PREPARE_ITEM])->subscribe(function($model, & $item) {
			$item['votes'] = $model->getCountVotes();
		});

		$query = PollVariantQuery::create();
		$query->orderById(Criteria::ASC);

		if (ctype_digit($this->request->query->get('poll_id'))) {
			$query->filterByPollId($this->request->query->get('poll_id'));
		}

		parent::all($query, [
			PollVariantTableMap::COL_ID,
			PollVariantTableMap::COL_TITLE,
		]);
	}
}
