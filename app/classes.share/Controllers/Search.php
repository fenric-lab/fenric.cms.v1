<?php

namespace Fenric\Controllers;

/**
 * Import classes
 */
use DateTime;
use Propel\Models\PublicationQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use Fenric\Controllers\Abstractable;

/**
 * Search
 */
class Search extends Abstractable
{

	/**
	 * Предварительная инициализация контроллера
	 */
	public function preInit() : bool
	{
		if ($this->request->query->exists('q')) {
			if (! is_string($this->request->query->get('q'))) {
				$this->response->setStatus(400);
				return false;
			}
		}

		if ($this->request->query->exists('page')) {
			if (! ctype_digit($this->request->query->get('page'))) {
				$this->response->setStatus(400);
				return false;
			}
		}

		if ($this->request->query->exists('limit')) {
			if (! ctype_digit($this->request->query->get('limit'))) {
				$this->response->setStatus(400);
				return false;
			}
		}

		return true;
	}

	/**
	 * Рендеринг контроллера
	 */
	public function render() : void
	{
		$content = fenric('view::search');

		if ($this->request->query->exists('q')) {
			$q = searchable($this->request->query->get('q'), 128, '%');

			if ($this->request->query->exists('page')) {
				if ($this->request->query->get('page') >= 1) {
					if ($this->request->query->get('page') <= PHP_INT_MAX) {
						$page = $this->request->query->get('page');
					}
				}
			}

			if ($this->request->query->exists('limit')) {
				if ($this->request->query->get('limit') >= 1) {
					if ($this->request->query->get('limit') <= 100) {
						$limit = $this->request->query->get('limit');
					}
				}
			}
			
			// Топ публикаций за день
			$publicationsTopNow = PublicationQuery::create()
				->filterByCreatedAt(['min' => new DateTime('1 days ago')])
				->orderByHits(Criteria::DESC)
				->limit(5)
				->find();
				
			if (count($publicationsTopNow) < 5)
			{
				$publicationsTopNow = PublicationQuery::create()
					->orderByCreatedAt(Criteria::DESC)
					->limit(5)
					->find();
			}
				
			// Топ публикаций за неделю
			$publicationsTopWeek = PublicationQuery::create()
				->filterByCreatedAt(['min' => new DateTime('7 days ago')])
				->orderByHits(Criteria::DESC)
				->limit(5)
				->find();
			
			if (count($publicationsTopWeek) < 5)
			{
				$publicationsTopWeek = PublicationQuery::create()
					->filterByCreatedAt(['min' => new DateTime('7 days ago'), 'max' => new DateTime('1 days ago')])
					->orderByCreatedAt(Criteria::DESC)
					->limit(5)
					->find();
			}
				
			// Топ публикаций за месяц
			$publicationsTopMonth = PublicationQuery::create()
				->filterByCreatedAt(['min' => new DateTime('30 days ago')])
				->orderByHits(Criteria::DESC)
				->limit(5)
				->find();
				
			if (count($publicationsTopMonth) < 5)
			{
				$publicationsTopMonth = PublicationQuery::create()
					->filterByCreatedAt(['min' => new DateTime('30 days ago'), 'max' => new DateTime('7 days ago')])
					->orderByCreatedAt(Criteria::DESC)
					->limit(5)
					->find();
			}

			$query = PublicationQuery::create();
			$query->filterByHeader(sprintf('%%%s%%', $q), Criteria::LIKE);
			$query->filterByShowAt(null)->_or()->filterByShowAt(new DateTime('now'), Criteria::LESS_EQUAL);
			$query->filterByHideAt(null)->_or()->filterByHideAt(new DateTime('now'), Criteria::GREATER_EQUAL);
			$query->orderById(Criteria::DESC);
			
			$data['publicationsTopNow'] = $publicationsTopNow;

			$data['publicationsTopWeek'] = $publicationsTopWeek;

			$data['publicationsTopMonth'] = $publicationsTopMonth;
			
			$data['publications'] = $query->paginate(
				$page ?? 1, $limit ?? fenric('parameter::search-per-page') ?: 25
			);

			$content = fenric('view::search', $data);
		}

		$this->response->setContent($content->render());
	}
}
