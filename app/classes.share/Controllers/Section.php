<?php

namespace Fenric\Controllers;

/**
 * Import classes
 */
use DateTime;
use Propel\Models\SectionQuery;
use Propel\Models\PublicationQuery;
use Propel\Models\UserQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use Fenric\Controllers\Abstractable;

/**
 * Section
 */
class Section extends Abstractable
{

	/**
	 * Предварительная инициализация контроллера
	 */
	public function preInit() : bool
	{
		if (! SectionQuery::existsByCode($this->request->parameters->get('code'))) {
			$this->response->setStatus(404);
			return false;
		}

		if ($this->request->query->exists('page')) {
			if (! ctype_digit($this->request->query->get('page'))) {
				$this->response->setStatus(400);
				return false;
			}
		}

		if ($this->request->query->exists('limit')) {
			if (! ctype_digit($this->request->query->get('limit'))) {
				$this->response->setStatus(400);
				return false;
			}
		}

		return true;
	}

	/**
	 * Рендеринг контроллера
	 */
	public function render() : void
	{
		$section = SectionQuery::create()->findOneByCode($this->request->parameters->get('code'));

		$publicationQuery = PublicationQuery::create();
		$publicationQuery->filterByShowAt(null)->_or()->filterByShowAt(new DateTime('now'), Criteria::LESS_EQUAL);
		$publicationQuery->filterByHideAt(null)->_or()->filterByHideAt(new DateTime('now'), Criteria::GREATER_EQUAL);
		$publicationQuery->filterBySectionId($section->getId());
		$publicationQuery->orderByCreatedAt(Criteria::DESC);

		if ($this->request->query->exists('page')) {
			if ($this->request->query->get('page') >= 1) {
				if ($this->request->query->get('page') <= PHP_INT_MAX) {
					$page = $this->request->query->get('page');
				}
			}
		}

		if ($this->request->query->exists('limit')) {
			if ($this->request->query->get('limit') >= 1) {
				if ($this->request->query->get('limit') <= 100) {
					$limit = $this->request->query->get('limit');
				}
			}
		}
		
		// Топ публикаций за день
		$publicationsTopNow = PublicationQuery::create()
			->filterByCreatedAt(['min' => new DateTime('1 days ago')])
			->filterBySectionId($section->getId())
			->orderByHits(Criteria::DESC)
			->limit(5);
			
		if ($publicationsTopNow->count() < 5)
		{
			$publicationsTopNow->clear();
			
			$publicationsTopNow->orderByCreatedAt(Criteria::DESC)
				->limit(5);
		}
		
		$publicationsTopNow->find();
			
		// Топ публикаций за неделю
		$publicationsTopWeek = PublicationQuery::create()
			->filterByCreatedAt(['min' => new DateTime('7 days ago')])
			->filterBySectionId($section->getId())
			->orderByHits(Criteria::DESC)
			->limit(5);
			
		if ($publicationsTopWeek->count() < 5)
		{
			$publicationsTopWeek->clear();
			
			$publicationsTopWeek->filterByCreatedAt(['min' => new DateTime('7 days ago'), 'max' => new DateTime('1 days ago')])
				->orderByCreatedAt(Criteria::DESC)
				->limit(5);
		}
		
		$publicationsTopWeek->find();
			
		// Топ публикаций за месяц
		$publicationsTopMonth = PublicationQuery::create()
			->filterByCreatedAt(['min' => new DateTime('30 days ago')])
			->filterBySectionId($section->getId())
			->orderByHits(Criteria::DESC)
			->limit(5);
			
		if ($publicationsTopMonth->count() < 5)
		{
			$publicationsTopMonth->clear();
			
			$publicationsTopMonth->filterByCreatedAt(['min' => new DateTime('30 days ago'), 'max' => new DateTime('7 days ago')])
				->orderByCreatedAt(Criteria::DESC)
				->limit(5);
		}
		
		$publicationsTopMonth->find();
		
		// БЖ рекомендует
		$recommendedByUs = PublicationQuery::create()
			->useSectionQuery()
				->filterByCode('bz-rekomenduet')
			->endUse()
			->orderByCreatedAt(Criteria::DESC)
			->orderByHits(Criteria::DESC)
			->findOne();
			
		// Проекты.... Один большой костыль
		$projects = SectionQuery::create()
			->filterByCode('project-%', Criteria::LIKE)
			->filterById($section->getId(), Criteria::NOT_EQUAL)
			->orderByCreatedAt(Criteria::DESC)
			->find();
			
		$users = UserQuery::create();
		
		$users->filterByRole('administrator', Criteria::EQUAL);
			
		$publications = $publicationQuery->paginate($page ?? 1, $limit ?? fenric('parameter::publications-per-page') ?: 10);
			
		$data['recommendedByUs'] = $recommendedByUs;
		
		$data['publicationsTopNow'] = $publicationsTopNow;
		
		$data['publicationsTopWeek'] = $publicationsTopWeek;
		
		$data['publicationsTopMonth'] = $publicationsTopMonth;
		
		$data['users'] = $users->find();
		
		$data['section'] = $section;
		
		$data['publications'] = $publications;
		
		$data['projects'] = $projects;

		$content = fenric('view::section', $data);

		if (fenric(sprintf('view::sections/%s/section', $this->request->parameters->get('code')))->exists()) {
			$content = fenric(sprintf('view::sections/%s/section', $this->request->parameters->get('code')), $data);
		}
		
		if (strpos($section->getCode(), 'project-') === 0)
		{
			$content = fenric('view::sections/projects/show', $data);
		}

		$this->response->setContent($content->render());
	}
}
