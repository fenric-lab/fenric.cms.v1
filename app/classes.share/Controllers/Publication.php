<?php

namespace Fenric\Controllers;

/**
 * Import classes
 */
use Propel\Models\PublicationQuery;
use Fenric\Controllers\Abstractable;
use Propel\Runtime\ActiveQuery\Criteria;
use DateTime;

/**
 * Publication
 */
class Publication extends Abstractable
{

	/**
	 * Предварительная инициализация контроллера
	 */
	public function preInit() : bool
	{
		if (! PublicationQuery::checkNestingByCode(
			$this->request->parameters->get('pcode'),
			$this->request->parameters->get('scode')
		)) {
			$this->response->setStatus(404);
			return false;
		}

		if ($this->request->environment->exists('HTTP_IF_MODIFIED_SINCE')) {
			if ($timestamp = strtotime($this->request->environment->get('HTTP_IF_MODIFIED_SINCE'))) {
				if (! PublicationQuery::checkModifiedByCode($this->request->parameters->get('pcode'), $timestamp)) {
					$this->response->setStatus(304);
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Рендеринг контроллера
	 */
	public function render() : void
	{
		$p = $this->request->parameters;

		$publication = PublicationQuery::create()->findOneByCode($p->get('pcode'));
		
		// Топ публикаций за день
		$publicationsTopNow = PublicationQuery::create()
			->filterByCreatedAt(['min' => new DateTime('1 days ago')])
			->filterById($publication->getId(), Criteria::NOT_EQUAL)
			->orderByHits(Criteria::DESC)
			->limit(5)
			->find();
			
		if (count($publicationsTopNow) < 5)
		{
			$publicationsTopNow = PublicationQuery::create()
				->filterById($publication->getId(), Criteria::NOT_EQUAL)
				->orderByHits(Criteria::DESC)
				->limit(5)
				->find();
		}
			
		// Топ публикаций за неделю
		$publicationsTopWeek = PublicationQuery::create()
			->filterByCreatedAt(['min' => new DateTime('7 days ago')])
			->filterById($publication->getId(), Criteria::NOT_EQUAL)
			->orderByHits(Criteria::DESC)
			->limit(5)
			->find();
			
		// Топ публикаций за месяц
		$publicationsTopMonth = PublicationQuery::create()
			->filterByCreatedAt(['min' => new DateTime('30 days ago')])
			->filterById($publication->getId(), Criteria::NOT_EQUAL)
			->orderByHits(Criteria::DESC)
			->limit(5)
			->find();
		
		// БЖ рекомендует
		$recommendedByUs = PublicationQuery::create()
			->useSectionQuery()
				->filterByCode('bz-rekomenduet')
			->endUse()
			->filterById($publication->getId(), Criteria::NOT_EQUAL)
			->orderByCreatedAt(Criteria::DESC)
			->orderByHits(Criteria::DESC)
			->findOne();
			
		$data['publication'] = $publication;
		
		$data['recommendedByUs'] = $recommendedByUs;
		
		$data['publicationsTopNow'] = $publicationsTopNow;
		
		$data['publicationsTopWeek'] = $publicationsTopWeek;
		
		$data['publicationsTopMonth'] = $publicationsTopMonth;

		$content = fenric('view::publication', $data);

		if (fenric(sprintf('view::sections/%s/publication', $p->get('scode')))->exists()) {
			$content = fenric(sprintf('view::sections/%s/publication', $p->get('scode')), $data);
		}

		$this->response->setHeader(sprintf('Last-Modified: %s', $publication->getUpdatedAt()->format('D, d M Y H:i:s T')));

		$this->response->setContent($content->render());

		$publication->registerHit();
	}
}
