<?php

namespace Fenric\Controllers\User;

/**
 * Import classes
 */
use Fenric\Controllers\Abstractable;

/**
 * ProfileSettingsSave
 */
class ProfileSettingsSave extends Abstractable
{

	/**
	 * Предварительная инициализация контроллера
	 */
	public function preInit() : bool
	{
		if (fenric('user')->isLogged())
		{
			if (is_string($this->request->post->get('email')) &&
				is_string($this->request->post->get('password')) &&
				is_string($this->request->post->get('password_confirmation')) &&
				is_string($this->request->post->get('firstname')) &&
				is_string($this->request->post->get('lastname')) &&
				is_string($this->request->post->get('gender')) &&
				is_string($this->request->post->get('birthday')) &&
				is_string($this->request->post->get('signature')) &&
				is_string($this->request->post->get('about'))
			) {
				return true;
			}
		}

		$this->response->setStatus(400);
		return false;
	}

	/**
	 * Рендеринг контроллера
	 */
	public function render() : void
	{
		// Lazy load fields...
		fenric('user')->getPassword();
		fenric('user')->getAbout();
		fenric('user')->getParams();

		if ($this->request->post->get('email')) {
			fenric('user')->setEmail(
				$this->request->post->get('email')
			);
			fenric('session')->set('user.settings.email',
				$this->request->post->get('email')
			);
		}

		if ($this->request->post->get('password')) {
			fenric('user')->setPassword(
				$this->request->post->get('password')
			);
			fenric('user')->setVirtualColumn('password_confirmation',
				$this->request->post->get('password_confirmation')
			);
		}

		fenric('user')->setFirstname(
			$this->request->post->get('firstname')
		);
		fenric('session')->set('user.settings.firstname',
			$this->request->post->get('firstname')
		);

		fenric('user')->setLastname(
			$this->request->post->get('lastname')
		);
		fenric('session')->set('user.settings.lastname',
			$this->request->post->get('lastname')
		);

		fenric('user')->setGender(
			$this->request->post->get('gender')
		);
		fenric('session')->set('user.settings.gender',
			$this->request->post->get('gender')
		);

		fenric('user')->setBirthday(
			$this->request->post->get('birthday')
		);
		fenric('session')->set('user.settings.birthday',
			$this->request->post->get('birthday')
		);

		fenric('user')->setSignature(
			$this->request->post->get('signature')
		);
		fenric('session')->set('user.settings.signature',
			$this->request->post->get('signature')
		);

		fenric('user')->setAbout(
			$this->request->post->get('about')
		);
		fenric('session')->set('user.settings.about',
			$this->request->post->get('about')
		);

		$errors = [];

		if (fenric('user')->validate())
		{
			fenric('user')->save();
		}
		else foreach (fenric('user')->getValidationFailures() as $failure)
		{
			$errors[$failure->getPropertyPath()][] = $failure->getMessage();
		}

		fenric('session')->set('user.settings.errors', $errors);

		$this->backward();
	}
}
