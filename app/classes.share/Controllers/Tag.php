<?php

namespace Fenric\Controllers;

/**
 * Import classes
 */
use DateTime;
use Propel\Models\TagQuery;
use Propel\Models\PublicationQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use Fenric\Controllers\Abstractable;

/**
 * Tag
 */
class Tag extends Abstractable
{

	/**
	 * Предварительная инициализация контроллера
	 */
	public function preInit() : bool
	{
		if (! TagQuery::existsByCode($this->request->parameters->get('code'))) {
			$this->response->setStatus(404);
			return false;
		}

		if ($this->request->query->exists('page')) {
			if (! ctype_digit($this->request->query->get('page'))) {
				$this->response->setStatus(400);
				return false;
			}
		}

		if ($this->request->query->exists('limit')) {
			if (! ctype_digit($this->request->query->get('limit'))) {
				$this->response->setStatus(400);
				return false;
			}
		}

		return true;
	}

	/**
	 * Рендеринг контроллера
	 */
	public function render() : void
	{
		$tag = TagQuery::create()->findOneByCode($this->request->parameters->get('code'));

		$publicationQuery = PublicationQuery::create();
		$publicationQuery->innerJoinPublicationTag();

		$publicationTagQuery = $publicationQuery->usePublicationTagQuery();
		$publicationTagQuery->filterByTagId($tag->getId());
		$publicationTagQuery->endUse();

		$publicationQuery->filterByShowAt(null)->_or()->filterByShowAt(new DateTime('now'), Criteria::LESS_EQUAL);
		$publicationQuery->filterByHideAt(null)->_or()->filterByHideAt(new DateTime('now'), Criteria::GREATER_EQUAL);
		$publicationQuery->orderById(Criteria::DESC);

		if ($this->request->query->exists('page')) {
			if ($this->request->query->get('page') >= 1) {
				if ($this->request->query->get('page') <= PHP_INT_MAX) {
					$page = $this->request->query->get('page');
				}
			}
		}

		if ($this->request->query->exists('limit')) {
			if ($this->request->query->get('limit') >= 1) {
				if ($this->request->query->get('limit') <= 100) {
					$limit = $this->request->query->get('limit');
				}
			}
		}

		$publications = $publicationQuery->paginate($page ?? 1, $limit ?? 25);

		$content = fenric('view::tag', ['tag' => $tag, 'publications' => $publications]);

		if (fenric(sprintf('view::tags/%s', $this->request->parameters->get('code')))->exists()) {
			$content = fenric(sprintf('view::tags/%s', $this->request->parameters->get('code')), [
				'tag' => $tag, 'publications' => $publications,
			]);
		}

		$this->response->setContent($content->render());
	}
}
