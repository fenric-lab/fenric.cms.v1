<?php

namespace Fenric\Controllers;

/**
 * Import classes
 */
use Propel\Models\Map\BannerTableMap;
use Fenric\Controllers\Abstractable;

/**
 * Banner
 */
class Banner extends Abstractable
{

	/**
	 * Предварительная инициализация контроллера
	 */
	public function preInit() : bool
	{
		$id = $this->request->parameters->get('id');

		$query = fenric('query')
		->select(BannerTableMap::COL_ID)
		->select(BannerTableMap::COL_HYPERLINK)
		->select(BannerTableMap::COL_CLICKS)
		->from(BannerTableMap::TABLE_NAME)
		->where(BannerTableMap::COL_ID, '=', $id);

		if ($banner = $query->readRow())
		{
			(function() use($banner)
			{
				$session_clicked_banners = fenric('session')->get('clicked.banners');

				if (empty($session_clicked_banners[$banner->id]))
				{
					$session_clicked_banners[$banner->id] = true;

					fenric('session')
					->set('clicked.banners', $session_clicked_banners);

					fenric('query')
					->update(BannerTableMap::TABLE_NAME, [
						BannerTableMap::COL_CLICKS => $banner->clicks + 1,
					])
					->where(BannerTableMap::COL_ID, '=', $banner->id)
					->limit(1)
					->shutdown();
				}
			})();

			$this->redirect($banner->hyperlink, 302);
			return true;
		}

		$this->response->setStatus(404);
		return false;
	}

	/**
	 * Рендеринг контроллера
	 */
	public function render() : void
	{}
}
