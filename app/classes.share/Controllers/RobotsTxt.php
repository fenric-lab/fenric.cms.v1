<?php

namespace Fenric\Controllers;

/**
 * Import classes
 */
use Fenric\Controllers\Abstractable;

/**
 * RobotsTxt
 */
class RobotsTxt extends Abstractable
{

	/**
	 * Рендеринг контроллера
	 */
	public function render() : void
	{
		$this->response->setHeader(
			'Content-type: text/plain; charset=UTF-8'
		);

		$this->response->setContent(
			fenric('parameter::robots-txt', '')
		);
	}
}
