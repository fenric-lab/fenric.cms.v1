<?php

namespace Fenric\Controllers;

/**
 * Import classes
 */
use Fenric\Controllers\Abstractable\Abstractable as BaseAbstractable;

/**
 * Abstractable
 */
abstract class Abstractable extends BaseAbstractable
{}
