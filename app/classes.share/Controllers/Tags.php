<?php

namespace Fenric\Controllers;

/**
 * Import classes
 */
use Propel\Models\TagQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use Fenric\Controllers\Abstractable;

/**
 * Tags
 */
class Tags extends Abstractable
{

	/**
	 * Рендеринг контроллера
	 */
	public function render() : void
	{
		$query = TagQuery::create();
		$query->orderById(Criteria::DESC);

		$content = fenric('view::tags', ['tags' => $query->find()]);

		$this->response->setContent($content->render());
	}
}
