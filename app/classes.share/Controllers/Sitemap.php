<?php

namespace Fenric\Controllers;

/**
 * Import classes
 */
use DateTime;
use SimpleXMLElement;
use Propel\Models\Map\SectionTableMap;
use Propel\Models\Map\PublicationTableMap;
use Fenric\Controllers\Abstractable;

/**
 * Sitemap
 */
class Sitemap extends Abstractable
{

	/**
	 * Рендеринг контроллера
	 */
	public function render() : void
	{
		$blank = '<?xml version="1.0" encoding="UTF-8"?>';
		$blank .= '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" />';

		$sitemapindex = new SimpleXMLElement($blank);

		$sitemap = $sitemapindex->addChild('sitemap');
		$sitemap->addChild('loc', sprintf('%s://%s/albums.xml', $this->request->getScheme(), $this->request->getHost()));

		$sitemap = $sitemapindex->addChild('sitemap');
		$sitemap->addChild('loc', sprintf('%s://%s/sections.xml', $this->request->getScheme(), $this->request->getHost()));

		$query = fenric('query')
		->cache(600)
		->select(SectionTableMap::COL_CODE)
		->from(PublicationTableMap::TABLE_NAME)
		->inner()->join(SectionTableMap::TABLE_NAME)
		->on(PublicationTableMap::COL_SECTION_ID, '=', SectionTableMap::COL_ID)
		->group(PublicationTableMap::COL_SECTION_ID)
		->order(PublicationTableMap::COL_SECTION_ID)
		->asc();

		if ($rows = $query->toArray())
		{
			foreach ($rows as $row)
			{
				$sitemap = $sitemapindex->addChild('sitemap');
				$sitemap->addChild('loc', sprintf('%s://%s/%s.xml', $this->request->getScheme(), $this->request->getHost(), $row['code']));
			}
		}

		$this->response->setHeader('Content-type: text/xml');
		$this->response->setContent($sitemapindex->asXML());
	}
}
