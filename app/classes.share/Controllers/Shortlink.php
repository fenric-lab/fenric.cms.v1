<?php

namespace Fenric\Controllers;

/**
 * Import classes
 */
use Propel\Models\Map\ShortlinkTableMap;
use Fenric\Controllers\Abstractable;

/**
 * Shortlink
 */
class Shortlink extends Abstractable
{

	/**
	 * Предварительная инициализация контроллера
	 */
	public function preInit() : bool
	{
		$code = $this->request->parameters->get('code');

		$query = fenric('query')
		->select(ShortlinkTableMap::COL_ID)
		->select(ShortlinkTableMap::COL_LOCATION)
		->select(ShortlinkTableMap::COL_REFERRALS)
		->from(ShortlinkTableMap::TABLE_NAME)
		->where(ShortlinkTableMap::COL_CODE, '=', $code);

		if ($shortlink = $query->readRow())
		{
			fenric('query')
			->update(ShortlinkTableMap::TABLE_NAME, [
				ShortlinkTableMap::COL_REFERRALS => $shortlink->referrals + 1,
			])
			->where(ShortlinkTableMap::COL_ID, '=', $shortlink->id)
			->limit(1)
			->shutdown();

			$this->redirect($shortlink->location, 302);
			return true;
		}

		$this->response->setStatus(404);
		return false;
	}

	/**
	 * Рендеринг контроллера
	 */
	public function render() : void
	{}
}
