<?php

/**
 * Package
 */
namespace Fenric;

/**
 * Import classes
 */
use Closure;
use SessionHandler;

/**
 * Web app
 */
final class App
{

	/**
	 * Запуск приложения
	 */
	public function run(Closure $middleware)
	{
		$this->setTimezone();

		ini_set('session.use_cookies', fenric('config::app')
			->get('session.use_cookies', '1')
		);
		ini_set('session.use_only_cookies', fenric('config::app')
			->get('session.use_only_cookies', '1')
		);
		ini_set('session.use_strict_mode', fenric('config::app')
			->get('session.use_strict_mode', '1')
		);
		ini_set('session.use_trans_sid', fenric('config::app')
			->get('session.use_trans_sid', '0')
		);
		ini_set('session.cookie_path', fenric('config::app')
			->get('session.cookie_path', '/')
		);
		ini_set('session.cookie_domain', fenric('config::app')
			->get('session.cookie_domain', '')
		);
		ini_set('session.cookie_secure', fenric('config::app')
			->get('session.cookie_secure', '0')
		);
		ini_set('session.cookie_httponly', fenric('config::app')
			->get('session.cookie_httponly', '0')
		);
		ini_set('session.cookie_lifetime', fenric('config::app')
			->get('session.cookie_lifetime', '0')
		);
		ini_set('session.cache_limiter', fenric('config::app')
			->get('session.cache_limiter', 'nocache')
		);

		fenric('session')->start(
			new SessionHandler()
		);

		$middleware();

		fenric('router')->run(fenric('request'), fenric('response'), function($router, $request, $response)
		{
			$request->isAjax() or $response->setContent(
				fenric(sprintf('view::errors/http/%d', $response->getStatus()))->render()
			);
		});

		fenric('response')->send();
	}

	/**
	 * Маршрутизация приложения
	 */
	public function rounting()
	{
		// Главная страница
		fenric('router')->get('/', \Fenric\Controllers\Home::class);

		// Панель управления сайтом
		fenric('router')->get('/admin(/)', \Fenric\Controllers\Admin\Index::class, function($router, $request, $response, $controller) {
			$controller->trailingSlashes();
		});

		// Общее API сайта
		fenric('router')->any('/api/<action:[a-z][a-z0-9-]*>/(<id:[1-9][0-9]{0,10}>/)', \Fenric\Controllers\Api::class);
		// Общее API для пользователя
		fenric('router')->any('/user/api/<action:[a-z][a-z0-9-]*>/(<id:[1-9][0-9]{0,10}>/)', \Fenric\Controllers\User\Api::class);
		// Общее API для администратора
		fenric('router')->any('/admin/api/<action:[a-z][a-z0-9-]*>/(<id:[1-9][0-9]{0,10}>/)', \Fenric\Controllers\Admin\Api::class);

		// Управления пользователями
		fenric('router')->any('/admin/api/user/<action:[a-z][a-z0-9-]*>/(<id:[1-9][0-9]{0,10}>/)', \Fenric\Controllers\Admin\ApiUser::class);
		// Управление альбомами
		fenric('router')->any('/admin/api/album/<action:[a-z][a-z0-9-]*>/(<id:[1-9][0-9]{0,10}>/)', \Fenric\Controllers\Admin\ApiAlbum::class);
		// Управление фотографиями
		fenric('router')->any('/admin/api/photo/<action:[a-z][a-z0-9-]*>/(<id:[1-9][0-9]{0,10}>/)', \Fenric\Controllers\Admin\ApiPhoto::class);
		// Управления разделами
		fenric('router')->any('/admin/api/section/<action:[a-z][a-z0-9-]*>/(<id:[1-9][0-9]{0,10}>/)', \Fenric\Controllers\Admin\ApiSection::class);
		// Управление публикациями
		fenric('router')->any('/admin/api/publication/<action:[a-z][a-z0-9-]*>/(<id:[1-9][0-9]{0,10}>/)', \Fenric\Controllers\Admin\ApiPublication::class);
		// Управления тегами
		fenric('router')->any('/admin/api/tag/<action:[a-z][a-z0-9-]*>/(<id:[1-9][0-9]{0,10}>/)', \Fenric\Controllers\Admin\ApiTag::class);
		// Управление меню
		fenric('router')->any('/admin/api/menu/<action:[a-z][a-z0-9-]*>/(<id:[1-9][0-9]{0,10}>/)', \Fenric\Controllers\Admin\ApiMenu::class);
		// Управление баннерами
		fenric('router')->any('/admin/api/banner/<action:[a-z][a-z0-9-]*>/(<id:[1-9][0-9]{0,10}>/)', \Fenric\Controllers\Admin\ApiBanner::class);
		// Управление группами баннеров
		fenric('router')->any('/admin/api/banner-group/<action:[a-z][a-z0-9-]*>/(<id:[1-9][0-9]{0,10}>/)', \Fenric\Controllers\Admin\ApiBannerGroup::class);
		// Управление опросами
		fenric('router')->any('/admin/api/poll/<action:[a-z][a-z0-9-]*>/(<id:[1-9][0-9]{0,10}>/)', \Fenric\Controllers\Admin\ApiPoll::class);
		// Управление вариантами опросов
		fenric('router')->any('/admin/api/poll-variant/<action:[a-z][a-z0-9-]*>/(<id:[1-9][0-9]{0,10}>/)', \Fenric\Controllers\Admin\ApiPollVariant::class);
		// Управление сниппетами
		fenric('router')->any('/admin/api/snippet/<action:[a-z][a-z0-9-]*>/(<id:[1-9][0-9]{0,10}>/)', \Fenric\Controllers\Admin\ApiSnippet::class);
		// Управление параметрами
		fenric('router')->any('/admin/api/parameter/<action:[a-z][a-z0-9-]*>/(<id:[1-9][0-9]{0,10}>/)', \Fenric\Controllers\Admin\ApiParameter::class);
		// Управление ссылками
		fenric('router')->any('/admin/api/shortlink/<action:[a-z][a-z0-9-]*>/(<id:[1-9][0-9]{0,10}>/)', \Fenric\Controllers\Admin\ApiShortlink::class);

		// Обновление системы
		fenric('router')->get('/admin/update/', \Fenric\Controllers\Admin\Update::class);

		// Страница учетной записи
		fenric('router')->get('/user(/<id:[1-9][0-9]*>)(/)', \Fenric\Controllers\User\Profile::class, function($router, $request, $response, $controller) {
			$controller->trailingSlashes();
		});

		// Страница с настройками учетной записи
		fenric('router')->get('/user/settings(/)', \Fenric\Controllers\User\ProfileSettings::class, function($router, $request, $response, $controller) {
			$controller->trailingSlashes();
		});
		fenric('router')->post('/user/settings/save(/)', \Fenric\Controllers\User\ProfileSettingsSave::class, function($router, $request, $response, $controller) {
			$controller->trailingSlashes();
		});

		// Аутентификация учетной записи
		fenric('router')->get('/user/login(/)', \Fenric\Controllers\User\Authentication::class, function($router, $request, $response, $controller) {
			$controller->trailingSlashes();
		});
		fenric('router')->post('/user/login/process(/)', \Fenric\Controllers\User\AuthenticationProcess::class, function($router, $request, $response, $controller) {
			$controller->trailingSlashes();
		});

		// Создание аутентификационного токена для учетной записи
		fenric('router')->get('/user/login/token/create(/)', \Fenric\Controllers\User\AuthenticationTokenCreate::class, function($router, $request, $response, $controller) {
			$controller->trailingSlashes();
		});
		fenric('router')->post('/user/login/token/create/process(/)', \Fenric\Controllers\User\AuthenticationTokenCreateProcess::class, function($router, $request, $response, $controller) {
			$controller->trailingSlashes();
		});

		// Аутентификация учетной записи по токену
		fenric('router')->get('/user/login/<code:[a-z0-9]{40}>(/)', \Fenric\Controllers\User\AuthenticationByToken::class, function($router, $request, $response, $controller) {
			$controller->trailingSlashes();
		});

		// Разавторизация учетной записи
		fenric('router')->get('/user/logout(/)', \Fenric\Controllers\User\SignOut::class, function($router, $request, $response, $controller) {
			$controller->trailingSlashes();
		});

		// Регистрация учетной записи
		fenric('router')->get('/user/registration(/)', \Fenric\Controllers\User\Registration::class, function($router, $request, $response, $controller) {
			$controller->trailingSlashes();
		});
		fenric('router')->post('/user/registration/process(/)', \Fenric\Controllers\User\RegistrationProcess::class, function($router, $request, $response, $controller) {
			$controller->trailingSlashes();
		});

		// Подтверждение регистрации учетной записи
		fenric('router')->get('/user/registration/confirm/<code:[a-z0-9]{40}>(/)', \Fenric\Controllers\User\RegistrationConfirm::class, function($router, $request, $response, $controller) {
			$controller->trailingSlashes();
		});

		// Переход по короткой ссылке
		fenric('router')->get('/goto/<code:[a-z0-9]{32}>(/)', \Fenric\Controllers\Shortlink::class, function($router, $request, $response, $controller) {
			$controller->trailingSlashes();
		});

		// Переход по рекламному баннеру
		fenric('router')->get('/click/<id:[1-9][0-9]{0,10}>(/)', \Fenric\Controllers\Banner::class, function($router, $request, $response, $controller) {
			$controller->trailingSlashes();
		});



		/**
		 * Code recheck...
		 */



		// Список альбомов
		fenric('router')->get('/albums(/)', \Fenric\Controllers\Albums::class, function($router, $request, $response, $controller) {
			$controller->trailingSlashes();
		});

		// Список фотографий альбома
		fenric('router')->get('/albums/<code:[a-z0-9-]+>(/)', \Fenric\Controllers\Album::class, function($router, $request, $response, $controller) {
			$controller->trailingSlashes();
		});

		// Список тегов
		fenric('router')->get('/tags(/)', \Fenric\Controllers\Tags::class, function($router, $request, $response, $controller) {
			$controller->trailingSlashes();
		});

		// Список публикаций тега
		fenric('router')->get('/tags/<code:[a-z0-9-]+>(/)', \Fenric\Controllers\Tag::class, function($router, $request, $response, $controller) {
			$controller->trailingSlashes();
		});

		// Поиск по сайту
		fenric('router')->get('/search(/)', \Fenric\Controllers\Search::class, function($router, $request, $response, $controller) {
			$controller->trailingSlashes();
		});

		// Страница раздела
		fenric('router')->get('/<code:[a-z0-9-]+>(/)', \Fenric\Controllers\Section::class, function($router, $request, $response, $controller) {
			$controller->trailingSlashes();
		});

		// Страница публикации
		fenric('router')->get('/<scode:[a-z0-9-]+>/<pcode:[a-z0-9-]+>(/)', \Fenric\Controllers\Publication::class, function($router, $request, $response, $controller) {
			$controller->trailingSlashes();
		});

		// Robots.txt
		fenric('router')->get('/robots.txt', \Fenric\Controllers\RobotsTxt::class);
		// Главная карта сайта
		fenric('router')->get('/sitemap.xml', \Fenric\Controllers\Sitemap::class);
		// Карта сайта с альбомами
		fenric('router')->get('/albums.xml', \Fenric\Controllers\Sitemaps\Albums::class);
		// Карта сайта с разделами
		fenric('router')->get('/sections.xml', \Fenric\Controllers\Sitemaps\Sections::class);
		// Карта сайта с публикациями
		fenric('router')->get('/<code:[a-z][a-z0-9-]*>.xml', \Fenric\Controllers\Sitemaps\Publications::class);
	}

	/**
	 * Установка часового пояса
	 */
	public function setTimezone() : void
	{
		date_default_timezone_set(
			fenric('parameter::timezone') ?: 'Europe/Moscow'
		);

		fenric('query')->getPdo()->exec(
			sprintf('SET @@session.time_zone = "%s";', date('P'))
		);
	}
}
