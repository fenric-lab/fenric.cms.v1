<?php

namespace Propel\Models;

use Propel\Models\Base\ShortlinkQuery as BaseShortlinkQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'shortlink' table.
 *
 * You should add additional methods to this class to meet the application requirements.
 * This class will only be generated as long as it does not already exist in the output directory.
 */
class ShortlinkQuery extends BaseShortlinkQuery
{}
