<?php

namespace Propel\Models\Map;

use Propel\Models\MenuItem;
use Propel\Models\MenuItemQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'menu_item' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class MenuItemTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Propel.Models.Map.MenuItemTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'menu_item';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Propel\\Models\\MenuItem';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Propel.Models.MenuItem';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 12;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 12;

    /**
     * the column name for the id field
     */
    const COL_ID = 'menu_item.id';

    /**
     * the column name for the menu_id field
     */
    const COL_MENU_ID = 'menu_item.menu_id';

    /**
     * the column name for the parent_id field
     */
    const COL_PARENT_ID = 'menu_item.parent_id';

    /**
     * the column name for the content field
     */
    const COL_CONTENT = 'menu_item.content';

    /**
     * the column name for the href field
     */
    const COL_HREF = 'menu_item.href';

    /**
     * the column name for the target field
     */
    const COL_TARGET = 'menu_item.target';

    /**
     * the column name for the display field
     */
    const COL_DISPLAY = 'menu_item.display';

    /**
     * the column name for the sequence field
     */
    const COL_SEQUENCE = 'menu_item.sequence';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'menu_item.created_at';

    /**
     * the column name for the created_by field
     */
    const COL_CREATED_BY = 'menu_item.created_by';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'menu_item.updated_at';

    /**
     * the column name for the updated_by field
     */
    const COL_UPDATED_BY = 'menu_item.updated_by';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'MenuId', 'ParentId', 'Content', 'Href', 'Target', 'Display', 'Sequence', 'CreatedAt', 'CreatedBy', 'UpdatedAt', 'UpdatedBy', ),
        self::TYPE_CAMELNAME     => array('id', 'menuId', 'parentId', 'content', 'href', 'target', 'display', 'sequence', 'createdAt', 'createdBy', 'updatedAt', 'updatedBy', ),
        self::TYPE_COLNAME       => array(MenuItemTableMap::COL_ID, MenuItemTableMap::COL_MENU_ID, MenuItemTableMap::COL_PARENT_ID, MenuItemTableMap::COL_CONTENT, MenuItemTableMap::COL_HREF, MenuItemTableMap::COL_TARGET, MenuItemTableMap::COL_DISPLAY, MenuItemTableMap::COL_SEQUENCE, MenuItemTableMap::COL_CREATED_AT, MenuItemTableMap::COL_CREATED_BY, MenuItemTableMap::COL_UPDATED_AT, MenuItemTableMap::COL_UPDATED_BY, ),
        self::TYPE_FIELDNAME     => array('id', 'menu_id', 'parent_id', 'content', 'href', 'target', 'display', 'sequence', 'created_at', 'created_by', 'updated_at', 'updated_by', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'MenuId' => 1, 'ParentId' => 2, 'Content' => 3, 'Href' => 4, 'Target' => 5, 'Display' => 6, 'Sequence' => 7, 'CreatedAt' => 8, 'CreatedBy' => 9, 'UpdatedAt' => 10, 'UpdatedBy' => 11, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'menuId' => 1, 'parentId' => 2, 'content' => 3, 'href' => 4, 'target' => 5, 'display' => 6, 'sequence' => 7, 'createdAt' => 8, 'createdBy' => 9, 'updatedAt' => 10, 'updatedBy' => 11, ),
        self::TYPE_COLNAME       => array(MenuItemTableMap::COL_ID => 0, MenuItemTableMap::COL_MENU_ID => 1, MenuItemTableMap::COL_PARENT_ID => 2, MenuItemTableMap::COL_CONTENT => 3, MenuItemTableMap::COL_HREF => 4, MenuItemTableMap::COL_TARGET => 5, MenuItemTableMap::COL_DISPLAY => 6, MenuItemTableMap::COL_SEQUENCE => 7, MenuItemTableMap::COL_CREATED_AT => 8, MenuItemTableMap::COL_CREATED_BY => 9, MenuItemTableMap::COL_UPDATED_AT => 10, MenuItemTableMap::COL_UPDATED_BY => 11, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'menu_id' => 1, 'parent_id' => 2, 'content' => 3, 'href' => 4, 'target' => 5, 'display' => 6, 'sequence' => 7, 'created_at' => 8, 'created_by' => 9, 'updated_at' => 10, 'updated_by' => 11, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('menu_item');
        $this->setPhpName('MenuItem');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Propel\\Models\\MenuItem');
        $this->setPackage('Propel.Models');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('menu_id', 'MenuId', 'INTEGER', 'menu', 'id', false, null, null);
        $this->addForeignKey('parent_id', 'ParentId', 'INTEGER', 'menu_item', 'id', false, null, null);
        $this->addColumn('content', 'Content', 'VARCHAR', true, 255, null);
        $this->getColumn('content')->setPrimaryString(true);
        $this->addColumn('href', 'Href', 'VARCHAR', false, 255, null);
        $this->addColumn('target', 'Target', 'VARCHAR', false, 255, null);
        $this->addColumn('display', 'Display', 'BOOLEAN', true, 1, true);
        $this->addColumn('sequence', 'Sequence', 'NUMERIC', true, null, 0);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addForeignKey('created_by', 'CreatedBy', 'INTEGER', 'user', 'id', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addForeignKey('updated_by', 'UpdatedBy', 'INTEGER', 'user', 'id', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Menu', '\\Propel\\Models\\Menu', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':menu_id',
    1 => ':id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('MenuItemRelatedByParentId', '\\Propel\\Models\\MenuItem', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':parent_id',
    1 => ':id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('UserRelatedByCreatedBy', '\\Propel\\Models\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':created_by',
    1 => ':id',
  ),
), 'SET NULL', 'CASCADE', null, false);
        $this->addRelation('UserRelatedByUpdatedBy', '\\Propel\\Models\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':updated_by',
    1 => ':id',
  ),
), 'SET NULL', 'CASCADE', null, false);
        $this->addRelation('MenuItemRelatedById', '\\Propel\\Models\\MenuItem', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':parent_id',
    1 => ':id',
  ),
), 'CASCADE', 'CASCADE', 'MenuItemsRelatedById', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'Fenric\Propel\Behaviors\Authorable' => array('create_enable' => 'true', 'create_column' => 'created_by', 'update_enable' => 'true', 'update_column' => 'updated_by', ),
            'Fenric\Propel\Behaviors\Timestampable' => array('create_enable' => 'true', 'create_column' => 'created_at', 'update_enable' => 'true', 'update_column' => 'updated_at', ),
            'validate' => array('86e0d555-2c8e-44bf-85bd-f4a8c0f84529' => array ('column' => 'menu_id','validator' => 'NotBlank',), '59339faa-eb19-4a03-b138-dcd9ecdc8ac7' => array ('column' => 'content','validator' => 'NotBlank',), '5177a745-c64d-4fde-82ec-fffcf52a3598' => array ('column' => 'content','validator' => 'Length','options' => array ('max' => 255,),), ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to menu_item     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        MenuItemTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? MenuItemTableMap::CLASS_DEFAULT : MenuItemTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (MenuItem object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = MenuItemTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = MenuItemTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + MenuItemTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = MenuItemTableMap::OM_CLASS;
            /** @var MenuItem $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            MenuItemTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = MenuItemTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = MenuItemTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var MenuItem $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                MenuItemTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(MenuItemTableMap::COL_ID);
            $criteria->addSelectColumn(MenuItemTableMap::COL_MENU_ID);
            $criteria->addSelectColumn(MenuItemTableMap::COL_PARENT_ID);
            $criteria->addSelectColumn(MenuItemTableMap::COL_CONTENT);
            $criteria->addSelectColumn(MenuItemTableMap::COL_HREF);
            $criteria->addSelectColumn(MenuItemTableMap::COL_TARGET);
            $criteria->addSelectColumn(MenuItemTableMap::COL_DISPLAY);
            $criteria->addSelectColumn(MenuItemTableMap::COL_SEQUENCE);
            $criteria->addSelectColumn(MenuItemTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(MenuItemTableMap::COL_CREATED_BY);
            $criteria->addSelectColumn(MenuItemTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(MenuItemTableMap::COL_UPDATED_BY);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.menu_id');
            $criteria->addSelectColumn($alias . '.parent_id');
            $criteria->addSelectColumn($alias . '.content');
            $criteria->addSelectColumn($alias . '.href');
            $criteria->addSelectColumn($alias . '.target');
            $criteria->addSelectColumn($alias . '.display');
            $criteria->addSelectColumn($alias . '.sequence');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.created_by');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.updated_by');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(MenuItemTableMap::DATABASE_NAME)->getTable(MenuItemTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(MenuItemTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(MenuItemTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new MenuItemTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a MenuItem or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or MenuItem object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MenuItemTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Propel\Models\MenuItem) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(MenuItemTableMap::DATABASE_NAME);
            $criteria->add(MenuItemTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = MenuItemQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            MenuItemTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                MenuItemTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the menu_item table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return MenuItemQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a MenuItem or Criteria object.
     *
     * @param mixed               $criteria Criteria or MenuItem object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MenuItemTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from MenuItem object
        }

        if ($criteria->containsKey(MenuItemTableMap::COL_ID) && $criteria->keyContainsValue(MenuItemTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.MenuItemTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = MenuItemQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // MenuItemTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
MenuItemTableMap::buildTableMap();
