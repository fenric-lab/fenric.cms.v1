<?php

namespace Propel\Models\Map;

use Propel\Models\Album;
use Propel\Models\AlbumQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'album' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class AlbumTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Propel.Models.Map.AlbumTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'album';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Propel\\Models\\Album';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Propel.Models.Album';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 14;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 1;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 13;

    /**
     * the column name for the id field
     */
    const COL_ID = 'album.id';

    /**
     * the column name for the code field
     */
    const COL_CODE = 'album.code';

    /**
     * the column name for the header field
     */
    const COL_HEADER = 'album.header';

    /**
     * the column name for the content field
     */
    const COL_CONTENT = 'album.content';

    /**
     * the column name for the status field
     */
    const COL_STATUS = 'album.status';

    /**
     * the column name for the meta_title field
     */
    const COL_META_TITLE = 'album.meta_title';

    /**
     * the column name for the meta_author field
     */
    const COL_META_AUTHOR = 'album.meta_author';

    /**
     * the column name for the meta_keywords field
     */
    const COL_META_KEYWORDS = 'album.meta_keywords';

    /**
     * the column name for the meta_description field
     */
    const COL_META_DESCRIPTION = 'album.meta_description';

    /**
     * the column name for the meta_robots field
     */
    const COL_META_ROBOTS = 'album.meta_robots';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'album.created_at';

    /**
     * the column name for the created_by field
     */
    const COL_CREATED_BY = 'album.created_by';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'album.updated_at';

    /**
     * the column name for the updated_by field
     */
    const COL_UPDATED_BY = 'album.updated_by';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Code', 'Header', 'Content', 'Status', 'MetaTitle', 'MetaAuthor', 'MetaKeywords', 'MetaDescription', 'MetaRobots', 'CreatedAt', 'CreatedBy', 'UpdatedAt', 'UpdatedBy', ),
        self::TYPE_CAMELNAME     => array('id', 'code', 'header', 'content', 'status', 'metaTitle', 'metaAuthor', 'metaKeywords', 'metaDescription', 'metaRobots', 'createdAt', 'createdBy', 'updatedAt', 'updatedBy', ),
        self::TYPE_COLNAME       => array(AlbumTableMap::COL_ID, AlbumTableMap::COL_CODE, AlbumTableMap::COL_HEADER, AlbumTableMap::COL_CONTENT, AlbumTableMap::COL_STATUS, AlbumTableMap::COL_META_TITLE, AlbumTableMap::COL_META_AUTHOR, AlbumTableMap::COL_META_KEYWORDS, AlbumTableMap::COL_META_DESCRIPTION, AlbumTableMap::COL_META_ROBOTS, AlbumTableMap::COL_CREATED_AT, AlbumTableMap::COL_CREATED_BY, AlbumTableMap::COL_UPDATED_AT, AlbumTableMap::COL_UPDATED_BY, ),
        self::TYPE_FIELDNAME     => array('id', 'code', 'header', 'content', 'status', 'meta_title', 'meta_author', 'meta_keywords', 'meta_description', 'meta_robots', 'created_at', 'created_by', 'updated_at', 'updated_by', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Code' => 1, 'Header' => 2, 'Content' => 3, 'Status' => 4, 'MetaTitle' => 5, 'MetaAuthor' => 6, 'MetaKeywords' => 7, 'MetaDescription' => 8, 'MetaRobots' => 9, 'CreatedAt' => 10, 'CreatedBy' => 11, 'UpdatedAt' => 12, 'UpdatedBy' => 13, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'code' => 1, 'header' => 2, 'content' => 3, 'status' => 4, 'metaTitle' => 5, 'metaAuthor' => 6, 'metaKeywords' => 7, 'metaDescription' => 8, 'metaRobots' => 9, 'createdAt' => 10, 'createdBy' => 11, 'updatedAt' => 12, 'updatedBy' => 13, ),
        self::TYPE_COLNAME       => array(AlbumTableMap::COL_ID => 0, AlbumTableMap::COL_CODE => 1, AlbumTableMap::COL_HEADER => 2, AlbumTableMap::COL_CONTENT => 3, AlbumTableMap::COL_STATUS => 4, AlbumTableMap::COL_META_TITLE => 5, AlbumTableMap::COL_META_AUTHOR => 6, AlbumTableMap::COL_META_KEYWORDS => 7, AlbumTableMap::COL_META_DESCRIPTION => 8, AlbumTableMap::COL_META_ROBOTS => 9, AlbumTableMap::COL_CREATED_AT => 10, AlbumTableMap::COL_CREATED_BY => 11, AlbumTableMap::COL_UPDATED_AT => 12, AlbumTableMap::COL_UPDATED_BY => 13, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'code' => 1, 'header' => 2, 'content' => 3, 'status' => 4, 'meta_title' => 5, 'meta_author' => 6, 'meta_keywords' => 7, 'meta_description' => 8, 'meta_robots' => 9, 'created_at' => 10, 'created_by' => 11, 'updated_at' => 12, 'updated_by' => 13, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('album');
        $this->setPhpName('Album');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Propel\\Models\\Album');
        $this->setPackage('Propel.Models');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('code', 'Code', 'VARCHAR', true, 255, null);
        $this->addColumn('header', 'Header', 'VARCHAR', true, 255, null);
        $this->getColumn('header')->setPrimaryString(true);
        $this->addColumn('content', 'Content', 'LONGVARCHAR', false, null, null);
        $this->addColumn('status', 'Status', 'VARCHAR', true, 255, 'published');
        $this->addColumn('meta_title', 'MetaTitle', 'VARCHAR', false, 255, null);
        $this->addColumn('meta_author', 'MetaAuthor', 'VARCHAR', false, 255, null);
        $this->addColumn('meta_keywords', 'MetaKeywords', 'VARCHAR', false, 255, null);
        $this->addColumn('meta_description', 'MetaDescription', 'VARCHAR', false, 255, null);
        $this->addColumn('meta_robots', 'MetaRobots', 'VARCHAR', false, 255, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addForeignKey('created_by', 'CreatedBy', 'INTEGER', 'user', 'id', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addForeignKey('updated_by', 'UpdatedBy', 'INTEGER', 'user', 'id', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('UserRelatedByCreatedBy', '\\Propel\\Models\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':created_by',
    1 => ':id',
  ),
), 'SET NULL', 'CASCADE', null, false);
        $this->addRelation('UserRelatedByUpdatedBy', '\\Propel\\Models\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':updated_by',
    1 => ':id',
  ),
), 'SET NULL', 'CASCADE', null, false);
        $this->addRelation('Photo', '\\Propel\\Models\\Photo', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':album_id',
    1 => ':id',
  ),
), 'CASCADE', 'CASCADE', 'Photos', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'Fenric\Propel\Behaviors\Authorable' => array('create_enable' => 'true', 'create_column' => 'created_by', 'update_enable' => 'true', 'update_column' => 'updated_by', ),
            'Fenric\Propel\Behaviors\Timestampable' => array('create_enable' => 'true', 'create_column' => 'created_at', 'update_enable' => 'true', 'update_column' => 'updated_at', ),
            'validate' => array('914517d4-7ea7-4f95-95e5-47bde0af7556' => array ('column' => 'code','validator' => 'NotBlank',), 'a936db3e-c131-427b-9d53-4673c37590a9' => array ('column' => 'code','validator' => 'Length','options' => array ('max' => 255,),), '5d81d4d2-c298-4b3c-b9f3-6ee7f2ef2035' => array ('column' => 'code','validator' => 'Regex','options' => array ('pattern' => '/^[a-z0-9-]+$/',),), 'a5bf371c-a8cb-4256-94d4-f4e4175ac62d' => array ('column' => 'code','validator' => 'Unique',), '15de4059-1bd8-4e6f-a291-ed23ae9e37e0' => array ('column' => 'header','validator' => 'NotBlank',), '5595f685-34ab-49b9-a4c9-2445b9425265' => array ('column' => 'header','validator' => 'Length','options' => array ('max' => 255,),), 'b372842c-3990-449a-bfc0-6fc65a6d8a78' => array ('column' => 'status','validator' => 'NotBlank',), '09af7984-f5a4-46b5-a01c-cb872782b563' => array ('column' => 'status','validator' => 'Regex','options' => array ('pattern' => '/^(?:published|unpublished)$/',),), ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to album     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        PhotoTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? AlbumTableMap::CLASS_DEFAULT : AlbumTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Album object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = AlbumTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = AlbumTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + AlbumTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = AlbumTableMap::OM_CLASS;
            /** @var Album $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            AlbumTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = AlbumTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = AlbumTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Album $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                AlbumTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(AlbumTableMap::COL_ID);
            $criteria->addSelectColumn(AlbumTableMap::COL_CODE);
            $criteria->addSelectColumn(AlbumTableMap::COL_HEADER);
            $criteria->addSelectColumn(AlbumTableMap::COL_STATUS);
            $criteria->addSelectColumn(AlbumTableMap::COL_META_TITLE);
            $criteria->addSelectColumn(AlbumTableMap::COL_META_AUTHOR);
            $criteria->addSelectColumn(AlbumTableMap::COL_META_KEYWORDS);
            $criteria->addSelectColumn(AlbumTableMap::COL_META_DESCRIPTION);
            $criteria->addSelectColumn(AlbumTableMap::COL_META_ROBOTS);
            $criteria->addSelectColumn(AlbumTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(AlbumTableMap::COL_CREATED_BY);
            $criteria->addSelectColumn(AlbumTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(AlbumTableMap::COL_UPDATED_BY);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.code');
            $criteria->addSelectColumn($alias . '.header');
            $criteria->addSelectColumn($alias . '.status');
            $criteria->addSelectColumn($alias . '.meta_title');
            $criteria->addSelectColumn($alias . '.meta_author');
            $criteria->addSelectColumn($alias . '.meta_keywords');
            $criteria->addSelectColumn($alias . '.meta_description');
            $criteria->addSelectColumn($alias . '.meta_robots');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.created_by');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.updated_by');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(AlbumTableMap::DATABASE_NAME)->getTable(AlbumTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(AlbumTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(AlbumTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new AlbumTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Album or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Album object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AlbumTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Propel\Models\Album) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(AlbumTableMap::DATABASE_NAME);
            $criteria->add(AlbumTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = AlbumQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            AlbumTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                AlbumTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the album table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return AlbumQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Album or Criteria object.
     *
     * @param mixed               $criteria Criteria or Album object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AlbumTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Album object
        }

        if ($criteria->containsKey(AlbumTableMap::COL_ID) && $criteria->keyContainsValue(AlbumTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.AlbumTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = AlbumQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // AlbumTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
AlbumTableMap::buildTableMap();
