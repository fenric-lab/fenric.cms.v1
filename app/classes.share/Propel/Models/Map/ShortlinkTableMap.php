<?php

namespace Propel\Models\Map;

use Propel\Models\Shortlink;
use Propel\Models\ShortlinkQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'shortlink' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ShortlinkTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Propel.Models.Map.ShortlinkTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'shortlink';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Propel\\Models\\Shortlink';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Propel.Models.Shortlink';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 9;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 9;

    /**
     * the column name for the id field
     */
    const COL_ID = 'shortlink.id';

    /**
     * the column name for the code field
     */
    const COL_CODE = 'shortlink.code';

    /**
     * the column name for the title field
     */
    const COL_TITLE = 'shortlink.title';

    /**
     * the column name for the location field
     */
    const COL_LOCATION = 'shortlink.location';

    /**
     * the column name for the referrals field
     */
    const COL_REFERRALS = 'shortlink.referrals';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'shortlink.created_at';

    /**
     * the column name for the created_by field
     */
    const COL_CREATED_BY = 'shortlink.created_by';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'shortlink.updated_at';

    /**
     * the column name for the updated_by field
     */
    const COL_UPDATED_BY = 'shortlink.updated_by';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Code', 'Title', 'Location', 'Referrals', 'CreatedAt', 'CreatedBy', 'UpdatedAt', 'UpdatedBy', ),
        self::TYPE_CAMELNAME     => array('id', 'code', 'title', 'location', 'referrals', 'createdAt', 'createdBy', 'updatedAt', 'updatedBy', ),
        self::TYPE_COLNAME       => array(ShortlinkTableMap::COL_ID, ShortlinkTableMap::COL_CODE, ShortlinkTableMap::COL_TITLE, ShortlinkTableMap::COL_LOCATION, ShortlinkTableMap::COL_REFERRALS, ShortlinkTableMap::COL_CREATED_AT, ShortlinkTableMap::COL_CREATED_BY, ShortlinkTableMap::COL_UPDATED_AT, ShortlinkTableMap::COL_UPDATED_BY, ),
        self::TYPE_FIELDNAME     => array('id', 'code', 'title', 'location', 'referrals', 'created_at', 'created_by', 'updated_at', 'updated_by', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Code' => 1, 'Title' => 2, 'Location' => 3, 'Referrals' => 4, 'CreatedAt' => 5, 'CreatedBy' => 6, 'UpdatedAt' => 7, 'UpdatedBy' => 8, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'code' => 1, 'title' => 2, 'location' => 3, 'referrals' => 4, 'createdAt' => 5, 'createdBy' => 6, 'updatedAt' => 7, 'updatedBy' => 8, ),
        self::TYPE_COLNAME       => array(ShortlinkTableMap::COL_ID => 0, ShortlinkTableMap::COL_CODE => 1, ShortlinkTableMap::COL_TITLE => 2, ShortlinkTableMap::COL_LOCATION => 3, ShortlinkTableMap::COL_REFERRALS => 4, ShortlinkTableMap::COL_CREATED_AT => 5, ShortlinkTableMap::COL_CREATED_BY => 6, ShortlinkTableMap::COL_UPDATED_AT => 7, ShortlinkTableMap::COL_UPDATED_BY => 8, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'code' => 1, 'title' => 2, 'location' => 3, 'referrals' => 4, 'created_at' => 5, 'created_by' => 6, 'updated_at' => 7, 'updated_by' => 8, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('shortlink');
        $this->setPhpName('Shortlink');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Propel\\Models\\Shortlink');
        $this->setPackage('Propel.Models');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('code', 'Code', 'VARCHAR', true, 255, null);
        $this->addColumn('title', 'Title', 'VARCHAR', true, 255, null);
        $this->addColumn('location', 'Location', 'VARCHAR', true, 255, null);
        $this->getColumn('location')->setPrimaryString(true);
        $this->addColumn('referrals', 'Referrals', 'BIGINT', false, null, 0);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addForeignKey('created_by', 'CreatedBy', 'INTEGER', 'user', 'id', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addForeignKey('updated_by', 'UpdatedBy', 'INTEGER', 'user', 'id', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('UserRelatedByCreatedBy', '\\Propel\\Models\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':created_by',
    1 => ':id',
  ),
), 'SET NULL', 'CASCADE', null, false);
        $this->addRelation('UserRelatedByUpdatedBy', '\\Propel\\Models\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':updated_by',
    1 => ':id',
  ),
), 'SET NULL', 'CASCADE', null, false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'Fenric\Propel\Behaviors\Authorable' => array('create_enable' => 'true', 'create_column' => 'created_by', 'update_enable' => 'true', 'update_column' => 'updated_by', ),
            'Fenric\Propel\Behaviors\Timestampable' => array('create_enable' => 'true', 'create_column' => 'created_at', 'update_enable' => 'true', 'update_column' => 'updated_at', ),
            'validate' => array('1bdd859a-2b20-4bc8-91eb-17ecdb8f73ca' => array ('column' => 'code','validator' => 'NotBlank',), '14eee152-ce74-47d7-a815-0c9c74df93d3' => array ('column' => 'code','validator' => 'Length','options' => array ('min' => 32,'max' => 32,),), '855f6973-c36a-4e5e-806b-a8d5017847ba' => array ('column' => 'code','validator' => 'Regex','options' => array ('pattern' => '/^[a-z0-9]+$/',),), '29f17d49-8a3f-4b8a-9908-64c7febbd3d5' => array ('column' => 'code','validator' => 'Unique',), 'f52af41e-226a-450f-b33d-dbb45dac4ede' => array ('column' => 'title','validator' => 'NotBlank',), '06a12e28-d61b-44b3-911f-3ba01c378812' => array ('column' => 'title','validator' => 'Length','options' => array ('max' => 255,),), '89c1e386-dcc9-4cff-84af-bfcdc750a3bd' => array ('column' => 'location','validator' => 'NotBlank',), '5aea66a2-f19c-4d8d-973a-d1bffd3b354a' => array ('column' => 'location','validator' => 'Length','options' => array ('max' => 255,),), 'e6f3cdb0-cbb8-462f-bcca-ab80d2780999' => array ('column' => 'location','validator' => 'Url',), ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ShortlinkTableMap::CLASS_DEFAULT : ShortlinkTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Shortlink object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ShortlinkTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ShortlinkTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ShortlinkTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ShortlinkTableMap::OM_CLASS;
            /** @var Shortlink $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ShortlinkTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ShortlinkTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ShortlinkTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Shortlink $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ShortlinkTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ShortlinkTableMap::COL_ID);
            $criteria->addSelectColumn(ShortlinkTableMap::COL_CODE);
            $criteria->addSelectColumn(ShortlinkTableMap::COL_TITLE);
            $criteria->addSelectColumn(ShortlinkTableMap::COL_LOCATION);
            $criteria->addSelectColumn(ShortlinkTableMap::COL_REFERRALS);
            $criteria->addSelectColumn(ShortlinkTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(ShortlinkTableMap::COL_CREATED_BY);
            $criteria->addSelectColumn(ShortlinkTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(ShortlinkTableMap::COL_UPDATED_BY);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.code');
            $criteria->addSelectColumn($alias . '.title');
            $criteria->addSelectColumn($alias . '.location');
            $criteria->addSelectColumn($alias . '.referrals');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.created_by');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.updated_by');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ShortlinkTableMap::DATABASE_NAME)->getTable(ShortlinkTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ShortlinkTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ShortlinkTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ShortlinkTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Shortlink or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Shortlink object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ShortlinkTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Propel\Models\Shortlink) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ShortlinkTableMap::DATABASE_NAME);
            $criteria->add(ShortlinkTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = ShortlinkQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ShortlinkTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ShortlinkTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the shortlink table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ShortlinkQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Shortlink or Criteria object.
     *
     * @param mixed               $criteria Criteria or Shortlink object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ShortlinkTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Shortlink object
        }

        if ($criteria->containsKey(ShortlinkTableMap::COL_ID) && $criteria->keyContainsValue(ShortlinkTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ShortlinkTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = ShortlinkQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ShortlinkTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ShortlinkTableMap::buildTableMap();
