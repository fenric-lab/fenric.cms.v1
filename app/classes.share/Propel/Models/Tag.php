<?php

namespace Propel\Models;

use Propel\Models\Base\Tag as BaseTag;
use Propel\Models\Map\PublicationTagTableMap;

use Propel\Models\PublicationQuery;
use Propel\Runtime\Connection\ConnectionInterface;
use DateTime;

/**
 * Skeleton subclass for representing a row from the 'tag' table.
 *
 * You should add additional methods to this class to meet the application requirements.
 * This class will only be generated as long as it does not already exist in the output directory.
 */
class Tag extends BaseTag
{

	/**
	 * Получение адреса тега
	 */
	public function getUri() : string
	{
		return sprintf('/tags/%s/', $this->getCode());
	}

	/**
	 * Получение количества публикаций по тегу
	 */
	public function getCountPublications() : int
	{
		return fenric('query')
		->count(PublicationTagTableMap::COL_ID)
		->from(PublicationTagTableMap::TABLE_NAME)
		->where(PublicationTagTableMap::COL_TAG_ID, '=', $this->getId())
		->readOne();
	}

	/**
	 * Форматирование сниппетов в контенте
	 */
	public function getSnippetableContent(ConnectionInterface $connection = null)
	{
		return snippetable(parent::getContent($connection));
	}

	/**
	 * Получение последней активной публикации по тегу
	 */
	public function getLatestPublication()
	{
		$code = $this->getCode();

		$query = fenric('query')
			->select('publication.id')
			->from('publication')
			->inner()
				->join('publication_tag')
				->on('publication_tag.publication_id', '=', 'publication.id')
			->inner()
				->join('tag')
				->on('tag.id', '=', 'publication_tag.tag_id')
			->where('publication.show_at', '<', new DateTime('NOW'))
			->where('publication.hide_at', '>', new DateTime('NOW'))->or_()->where('publication.hide_at', 'is', null)
			->where('tag.code', '=', $code)
			->order('publication.updated_at')->desc()
			->limit(1)
			->readOne();

		return PublicationQuery::create()->findPk($query);
	}
	
	/**
	 * Получение всех публикаций тега
	 *
	 * @return	collection
	 */
	public function getPublications()
	{
		$code = $this->getCode();

		$query = fenric('query')
			->select('publication.id')
			->from('publication')
			->inner()
				->join('publication_tag')
				->on('publication_tag.publication_id', '=', 'publication.id')
			->inner()
				->join('tag')
				->on('tag.id', '=', 'publication_tag.tag_id')
			->where('publication.show_at', '<', new DateTime('NOW'))
			->where('publication.hide_at', '>', new DateTime('NOW'))->or_()->where('publication.hide_at', 'is', null)
			->where('tag.code', '=', $code)
			->order('publication.updated_at')->desc()
			->readCol();

		return PublicationQuery::create()->findPks($query);
	}
}
