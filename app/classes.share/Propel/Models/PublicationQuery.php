<?php

namespace Propel\Models;

use DateTime;
use Propel\Models\Base\PublicationQuery as BasePublicationQuery;
use Propel\Models\Map\PublicationTableMap;
use Propel\Models\Map\SectionTableMap;

/**
 * Skeleton subclass for performing query and update operations on the 'publication' table.
 *
 * You should add additional methods to this class to meet the application requirements.
 * This class will only be generated as long as it does not already exist in the output directory.
 */
class PublicationQuery extends BasePublicationQuery
{

	/**
	 * Проверка существования объекта по символьному коду
	 */
	public static function existsByCode(string $publicationCode) : bool
	{
		$query = fenric('query')
		->select(PublicationTableMap::COL_ID)
		->from(PublicationTableMap::TABLE_NAME)
		->where(PublicationTableMap::COL_CODE, '=', $publicationCode);

		return $query->readOne() ? true : false;
	}

	/**
	 * Проверка вложенности объекта по символьным кодам раздела и публикации
	 */
	public static function checkNestingByCode(string $publicationCode, string $sectionCode) : bool
	{
		$query = fenric('query')
		->select(PublicationTableMap::COL_ID)
		->from(PublicationTableMap::TABLE_NAME)
		->inner()->join(SectionTableMap::TABLE_NAME)
		->on(PublicationTableMap::COL_SECTION_ID, '=', SectionTableMap::COL_ID)
		->where(PublicationTableMap::COL_CODE, '=', $publicationCode)
		->where(SectionTableMap::COL_CODE, '=', $sectionCode);

		return $query->readOne() ? true : false;
	}

	/**
	 * Проверка модифицированности объекта по символьному коду
	 */
	public static function checkModifiedByCode(string $publicationCode, int $timestamp) : bool
	{
		$datetime = new DateTime(fenric('query')
		->select(PublicationTableMap::COL_UPDATED_AT)
		->from(PublicationTableMap::TABLE_NAME)
		->where(PublicationTableMap::COL_CODE, '=', $publicationCode)
		->readOne());

		return $datetime->getTimestamp() > $timestamp;
	}
}
