<?php

namespace Propel\Models;

use Propel\Models\Base\Album as BaseAlbum;
use Propel\Models\Map\PhotoTableMap;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;

/**
 * Skeleton subclass for representing a row from the 'album' table.
 *
 * You should add additional methods to this class to meet the application requirements.
 * This class will only be generated as long as it does not already exist in the output directory.
 */
class Album extends BaseAlbum
{

	/**
	 * Получение адреса альбома
	 */
	public function getUri() : string
	{
		return sprintf('/albums/%s/', $this->getCode());
	}

	/**
	 * Получение обложки альбома
	 */
	public function getCover()
	{
		return fenric('query')
		->select(PhotoTableMap::COL_FILE)
		->from(PhotoTableMap::TABLE_NAME)
		->where(PhotoTableMap::COL_ALBUM_ID, '=', $this->getId())
		->order(PhotoTableMap::COL_SEQUENCE)->asc()
		->readOne();
	}

	/**
	 * Получение количества фотографий в альбоме
	 */
	public function getCountPhotos() : int
	{
		return fenric('query')
		->count(PhotoTableMap::COL_ID)
		->from(PhotoTableMap::TABLE_NAME)
		->where(PhotoTableMap::COL_ALBUM_ID, '=', $this->getId())
		->readOne();
	}

	/**
	 * Форматирование сниппетов в контенте
	 */
	public function getSnippetableContent(ConnectionInterface $connection = null)
	{
		return snippetable(parent::getContent($connection));
	}

	/**
	 * Получение отсортированных фотографий альбома
	 */
	public function getSortablePhotos() : ObjectCollection
	{
		$query = PhotoQuery::create();

		$query->filterByAlbumId($this->getId());
		$query->orderBySequence(Criteria::ASC);

		return $query->find();
	}
}
