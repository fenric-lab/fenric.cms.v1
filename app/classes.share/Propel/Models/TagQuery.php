<?php

namespace Propel\Models;

use Propel\Models\Map\TagTableMap;
use Propel\Models\Base\TagQuery as BaseTagQuery;

use Propel\Runtime\Collection\ObjectCollection;

/**
 * Skeleton subclass for performing query and update operations on the 'tag' table.
 *
 * You should add additional methods to this class to meet the application requirements.
 * This class will only be generated as long as it does not already exist in the output directory.
 */
class TagQuery extends BaseTagQuery
{

	/**
	 * Проверка существования объекта по символьному коду
	 */
	public static function existsByCode(string $code) : bool
	{
		$query = fenric('query')
		->select(TagTableMap::COL_ID)
		->from(TagTableMap::TABLE_NAME)
		->where(TagTableMap::COL_CODE, '=', $code);

		return $query->readOne() ? true : false;
	}
	
	/**
	 * Сортировка тегов по дате создания публикаций
	 *
	 * @param	string	  $code
	 * @param	criteria  $criteria
	 */
	public function filterByCodeOrderByPublicationCreatedAt($code, $criteria)
	{
		$query = fenric('query');
		
		$query->select('tag.id')
			->from('tag')
			->inner()
				->join('publication_tag')->alias('ptag')
				->on('ptag.tag_id', '=', 'tag.id')
			->inner()
				->join('publication')
				->on('publication.id', '=', 'ptag.publication_id')
			->where('tag.code', $criteria, $code)
			->group('tag.id')
			->order('publication.created_at')->desc();
			
		$collection = new ObjectCollection();
			
		foreach ($query->readCol() as $id)
		{
			$collection->set($id, TagQuery::create()->findPk($id));
		}
			
		return $collection;
	}
}
