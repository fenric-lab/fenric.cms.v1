<?php

namespace Propel\Models;

use Propel\Models\Base\MenuItem as BaseMenuItem;
use Propel\Models\Map\MenuItemTableMap;
use Propel\Runtime\Connection\ConnectionInterface;

/**
 * Skeleton subclass for representing a row from the 'menu_item' table.
 *
 * You should add additional methods to this class to meet the application requirements.
 * This class will only be generated as long as it does not already exist in the output directory.
 */
class MenuItem extends BaseMenuItem
{

	/**
	 * Code to be run before inserting to database
	 */
	public function preInsert(ConnectionInterface $connection = null)
	{
		$this->setSequence(fenric('query')
		->max(MenuItemTableMap::COL_SEQUENCE)
		->from(MenuItemTableMap::TABLE_NAME)
		->where(MenuItemTableMap::COL_MENU_ID, '=', $this->getMenu()->getId())
		->readOne() + 1);

		return true;
	}
}
