<?php

namespace Propel\Models;

use DateTime;
use Propel\Models\Map\BannerTableMap;
use Propel\Models\Base\BannerGroup as BaseBannerGroup;

/**
 * Skeleton subclass for representing a row from the 'banner_group' table.
 *
 * You should add additional methods to this class to meet the application requirements.
 * This class will only be generated as long as it does not already exist in the output directory.
 */
class BannerGroup extends BaseBannerGroup
{

	/**
	 * Получение количества баннеров в группе
	 */
	public function getCountBanners() : int
	{
		return fenric('query')
		->count(BannerTableMap::COL_ID)
		->from(BannerTableMap::TABLE_NAME)
		->where(BannerTableMap::COL_BANNER_GROUP_ID, '=', $this->getId())
		->readOne();
	}

	/**
	 * Получение баннера
	 */
	public function getBanner() : ?string
	{
		$query = fenric('query')

		// ->cache(600) // enable cache on 10 min.

		->select(BannerTableMap::COL_ID)
		->select(BannerTableMap::COL_TITLE)
		->select(BannerTableMap::COL_PICTURE)
		->select(BannerTableMap::COL_PICTURE_ALT)
		->select(BannerTableMap::COL_PICTURE_TITLE)
		->select(BannerTableMap::COL_HYPERLINK)
		->select(BannerTableMap::COL_HYPERLINK_TARGET)
		->select(BannerTableMap::COL_SHOWS)
		->from(BannerTableMap::TABLE_NAME)

		->where(BannerTableMap::COL_BANNER_GROUP_ID, '=', $this->getId())
			->and_open()
				->where(BannerTableMap::COL_SHOW_START, 'is', null)
					->or_()->where(BannerTableMap::COL_SHOW_START, '<=', new DateTime('now'))

			->close_and_open()
				->where(BannerTableMap::COL_SHOW_END, 'is', null)
					->or_()->where(BannerTableMap::COL_SHOW_END, '>=', new DateTime('now'))

			->close_and_open()
				->where(BannerTableMap::COL_SHOWS_LIMIT, 'is', null)
					->or_()->where(BannerTableMap::COL_SHOWS, '<', function() {
						return BannerTableMap::COL_SHOWS_LIMIT;
					})

			->close_and_open()
				->where(BannerTableMap::COL_CLICKS_LIMIT, 'is', null)
					->or_()->where(BannerTableMap::COL_CLICKS, '<', function() {
						return BannerTableMap::COL_CLICKS_LIMIT;
					})

		->order('shows')
		->asc();

		if ($banner = $query->readRow())
		{
			$update = [BannerTableMap::COL_SHOWS => $banner->shows + 1];

			fenric('query')
			->update(BannerTableMap::TABLE_NAME, $update)
			->where(BannerTableMap::COL_ID, '=', $banner->id)
			->limit(1)
			->shutdown();

			$html = '';

			$html .= '<a href="/click/' . $banner->id . '/" target="' . $banner->hyperlink_target . '">';
				$html .= '<img src="/upload/' . $banner->picture . '" alt="' . $banner->picture_alt . '" title="' . $banner->picture_title . '" />';
			$html .= '</a>';

			return $html;
		}

		return null;
	}
}
