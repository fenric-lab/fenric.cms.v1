<?php

namespace Propel\Models;

use Propel\Models\Base\PollVariant as BasePollVariant;
use Propel\Models\Map\PollVoteTableMap;

/**
 * Skeleton subclass for representing a row from the 'poll_variant' table.
 *
 * You should add additional methods to this class to meet the application requirements.
 * This class will only be generated as long as it does not already exist in the output directory.
 */
class PollVariant extends BasePollVariant
{

	/**
	 * Получение количества голосов
	 */
	public function getCountVotes() : int
	{
		return fenric('query')
		->count(PollVoteTableMap::COL_ID)
		->from(PollVoteTableMap::TABLE_NAME)
		->where(PollVoteTableMap::COL_POLL_VARIANT_ID, '=', $this->getId())
		->readOne();
	}
}
