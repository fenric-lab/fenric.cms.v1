<?php

namespace Propel\Models;

use Propel\Models\Base\Publication as BasePublication;
use Propel\Models\Map\PublicationTableMap;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;

use Propel\Models\TagQuery;

/**
 * Skeleton subclass for representing a row from the 'publication' table.
 *
 * You should add additional methods to this class to meet the application requirements.
 * This class will only be generated as long as it does not already exist in the output directory.
 */
class Publication extends BasePublication
{

	/**
	 * Получение адреса публикации
	 */
	public function getUri() : string
	{
		return sprintf('/%s/%s/', $this->getSection()->getCode(), $this->getCode());
	}

	/**
	 * Форматирование сниппетов в контенте
	 */
	public function getSnippetableAnons()
	{
		return snippetable(parent::getAnons());
	}

	/**
	 * Форматирование сниппетов в контенте
	 */
	public function getSnippetableContent(ConnectionInterface $connection = null)
	{
		return snippetable(parent::getContent($connection));
	}

	/**
	 * Прикрепление тегов к публикации
	 */
	public function attachTags(array $tagsIds) : void
	{
		if (count($tagsIds) > 0)
		{
			if ($this->getPublicationTags() instanceof ObjectCollection)
			{
				foreach ($this->getPublicationTags() as $publicationTag)
				{
					if (($i = array_search($publicationTag->getTagId(), $tagsIds)) !== false)
					{
						unset($tagsIds[$i]);

						continue;
					}

					$publicationTag->delete();
				}
			}

			if (count($tagsIds) > 0)
			{
				$tags = TagQuery::create()->findPks($tagsIds);

				if ($tags instanceof ObjectCollection)
				{
					foreach ($tags as $tag)
					{
						$publicationTag = new PublicationTag();

						$publicationTag->setPublication($this);
						$publicationTag->setTag($tag);

						$this->addPublicationTag($publicationTag);
					}
				}
			}
		}
	}

	/**
	 * Регистрация «хита»
	 */
	public function registerHit() : void
	{
		$hits = fenric('session')->get('publication.hits');

		if (empty($hits[$this->getId()]))
		{
			$hits[$this->getId()] = true;

			fenric('session')->set('publication.hits', $hits);

			fenric('query')
			->update(PublicationTableMap::TABLE_NAME, [
				PublicationTableMap::COL_HITS => $this->getHits() + 1,
			])
			->where(PublicationTableMap::COL_ID, '=', $this->getId())
			->limit(1)
			->shutdown();
		}
	}
	
	/**
	 * Получение тегов публикации
	 */
	public function getTags()
	{
		$query = fenric('query');
		
		$query->select('tag.id')
			->from('tag')
			->inner()
				->join('publication_tag')->alias('p_tag')
				->on('p_tag.tag_id', '=', 'tag.id')
			->inner()
				->join('publication')->alias('pub')
				->on('pub.id', '=', 'p_tag.publication_id')
			->where('pub.id', '=', $this->getId());
			
		$tagIds = $query->readCol();
		
		$tags = TagQuery::create()->findPks($tagIds);
		
		return $tags;
	}
	
	/**
	 * Получение полного имени автора
	 */
	public function getAuthor()
	{
		if ($this->getUserRelatedByCreatedBy())
		{
			return sprintf('%s %s', $this->getUserRelatedByCreatedBy()->getFirstname(), $this->getUserRelatedByCreatedBy()->getLastname());
		}
	}
}
