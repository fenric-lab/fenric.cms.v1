<?php

namespace Propel\Models;

use Propel\Models\Base\AlbumQuery as BaseAlbumQuery;
use Propel\Models\Map\AlbumTableMap;

/**
 * Skeleton subclass for performing query and update operations on the 'album' table.
 *
 * You should add additional methods to this class to meet the application requirements.
 * This class will only be generated as long as it does not already exist in the output directory.
 */
class AlbumQuery extends BaseAlbumQuery
{

	/**
	 * Проверка существования объекта по символьному коду
	 */
	public static function existsByCode(string $code) : bool
	{
		$query = fenric('query')
		->select(AlbumTableMap::COL_ID)
		->from(AlbumTableMap::TABLE_NAME)
		->where(AlbumTableMap::COL_CODE, '=', $code);

		return $query->readOne() ? true : false;
	}
}
