<?php

namespace Propel\Models;

use DateTime;
use Propel\Models\Base\Poll as BasePoll;
use Propel\Models\Map\PollVariantTableMap;
use Propel\Models\Map\PollVoteTableMap;
use Propel\Models\PollVariantQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\Collection\ObjectCollection;

/**
 * Skeleton subclass for representing a row from the 'poll' table.
 *
 * You should add additional methods to this class to meet the application requirements.
 * This class will only be generated as long as it does not already exist in the output directory.
 */
class Poll extends BasePoll
{

	/**
	 * Получение формы опроса
	 */
	public function getForm() : ?string
	{
		$query = PollVariantQuery::create();

		$query->filterByPollId($this->getId());
		$query->orderById(Criteria::ASC);

		$variants = $query->find();

		if ($variants instanceof ObjectCollection)
		{
			if ($this->isOpen())
			{
				if ($this->isPrimaryVoting())
				{
					return fenric('view::poll.form', [
						'poll' => $this,
						'variants' => $variants,
					])->render();
				}
			}

			return fenric('view::poll.chart', [
				'poll' => $this,
				'variants' => $variants,
			])->render();
		}
	}

	/**
	 * Получение уникального идентификатора голоса респондента
	 */
	public function getRespondentVoteId() : string
	{
		return hash('md5', $this->getId() .
			fenric('request')->environment->get('REMOTE_ADDR') .
			fenric('request')->environment->get('HTTP_USER_AGENT')
		);
	}

	/**
	 * Получение уникального идентификатора респондента
	 */
	public function getRespondentId() : string
	{
		return hash('md5',
			fenric('request')->environment->get('REMOTE_ADDR') .
			fenric('request')->environment->get('HTTP_USER_AGENT')
		);
	}

	/**
	 * Получение количества голосов
	 */
	public function getCountVotes() : int
	{
		return fenric('query')
		->count(PollVoteTableMap::COL_ID)
		->from(PollVoteTableMap::TABLE_NAME)
		->inner()->join(PollVariantTableMap::TABLE_NAME)
		->on(PollVoteTableMap::COL_POLL_VARIANT_ID, '=', PollVariantTableMap::COL_ID)
		->where(PollVariantTableMap::COL_POLL_ID, '=', $this->getId())
		->readOne();
	}

	/**
	 * Получение количества вариантов
	 */
	public function getCountVariants() : int
	{
		return fenric('query')
		->count(PollVariantTableMap::COL_ID)
		->from(PollVariantTableMap::TABLE_NAME)
		->where(PollVariantTableMap::COL_POLL_ID, '=', $this->getId())
		->readOne();
	}

	/**
	 * Является ли опрос открытым
	 */
	public function isOpen() : bool
	{
		if ($this->getOpenAt() instanceof DateTime) {
			if ($this->getOpenAt()->getTimestamp() > time()) {
				return false;
			}
		}

		if ($this->getCloseAt() instanceof DateTime) {
			if ($this->getCloseAt()->getTimestamp() < time()) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Является ли участие в опросе первичным для респондента
	 */
	public function isPrimaryVoting() : bool
	{
		return ! fenric('query')
			->count(PollVoteTableMap::COL_ID)
			->from(PollVoteTableMap::TABLE_NAME)
			->where(PollVoteTableMap::COL_RESPONDENT_VOTE_ID, '=', $this->getRespondentVoteId())
		->readOne();
	}
}
