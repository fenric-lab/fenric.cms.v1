<?php

namespace Propel\Models;

use Propel\Models\Base\Menu as BaseMenu;
use Propel\Models\Map\MenuItemTableMap;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\Collection\ObjectCollection;

/**
 * Skeleton subclass for representing a row from the 'menu' table.
 *
 * You should add additional methods to this class to meet the application requirements.
 * This class will only be generated as long as it does not already exist in the output directory.
 */
class Menu extends BaseMenu
{

	/**
	 * Получение количества элементов меню в позиции
	 */
	public function getCountItems() : int
	{
		return fenric('query')
		->count(MenuItemTableMap::COL_ID)
		->from(MenuItemTableMap::TABLE_NAME)
		->where(MenuItemTableMap::COL_MENU_ID, '=', $this->getId())
		->readOne();
	}

	/**
	 * Получение дерева
	 */
	public function getTree(array $options = []) : ?string
	{
		$query = MenuItemQuery::create();

		$query->filterByMenuId($this->getId());
		$query->filterByDisplay(true);
		$query->orderBySequence(Criteria::ASC);

		$items = $query->find();

		if ($items instanceof ObjectCollection)
		{
			return $this->buildTree($items, $options);
		}

		return null;
	}

	/**
	 * Построение дерева
	 */
	protected function buildTree(ObjectCollection $items, array $options, int $parentId = null, int $depth = 0) : string
	{
		$depth++;

		$html = '<ul';

		$ul_classes = [];

		if (isset($options['ul-class'])) {
			$ul_classes[] = $options['ul-class'];
		}
		if (isset($options['ul-first-class']) && $depth === 1) {
			$ul_classes[] = $options['ul-first-class'];
		}
		if (isset($options['ul-drowned-class']) && $depth > 1) {
			$ul_classes[] = $options['ul-drowned-class'];
		}
		if (count($ul_classes) > 0) {
			$html .= ' class="' . implode(' ', $ul_classes) . '"';
		}

		$html .= '>' . PHP_EOL;

		$childs = 0;

		foreach ($items as $item)
		{
			if ($parentId === $item->getParentId())
			{
				$childs++;

				$active = false;

				if ($path = parse_url($item->getHref(), PHP_URL_PATH)) {
					if (is($path)) {
						$active = true;
					}
				}

				$html .= '<li';

				$li_classes = [];

				if (isset($options['li-class'])) {
					$li_classes[] = $options['li-class'];
				}
				if (isset($options['li-first-class']) && $depth === 1) {
					$li_classes[] = $options['li-first-class'];
				}
				if (isset($options['li-drowned-class']) && $depth > 1) {
					$li_classes[] = $options['li-drowned-class'];
				}
				if (isset($options['li-active-class']) && $active) {
					$li_classes[] = $options['li-active-class'];
				}
				if (count($li_classes) > 0) {
					$html .= ' class="' . implode(' ', $li_classes) . '"';
				}

				$html .= '>' . PHP_EOL;

				if (strlen($item->getHref()) > 0)
				{
					$html .= '<a';

					$a_classes = [];

					if (isset($options['a-class'])) {
						$a_classes[] = $options['a-class'];
					}
					if (isset($options['a-first-class']) && $depth === 1) {
						$a_classes[] = $options['a-first-class'];
					}
					if (isset($options['a-drowned-class']) && $depth > 1) {
						$a_classes[] = $options['a-drowned-class'];
					}
					if (isset($options['a-active-class']) && $active) {
						$a_classes[] = $options['a-active-class'];
					}
					if (count($a_classes) > 0) {
						$html .= ' class="' . implode(' ', $a_classes) . '"';
					}

					$html .= ' href="' . $item->getHref() . '"';

					if (strlen($item->getTarget()) > 0) {
						$html .= ' target="' . $item->getTarget() . '"';
					}

					$html .= '>' . $item->getContent() . '</a>' . PHP_EOL;
				}

				else
				{
					$html .= '<span>' . $item->getContent() . '</span>' . PHP_EOL;
				}

				$html .= $this->buildTree($items, $options, $item->getId(), $depth) . PHP_EOL . '</li>' . PHP_EOL;
			}
		}

		$html .= '</ul>';

		if ($childs > 0)
		{
			return $html;
		}

		return '';
	}
}
