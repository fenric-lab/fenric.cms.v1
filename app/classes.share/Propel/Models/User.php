<?php

namespace Propel\Models;

use DateTime;
use DateInterval;
use Propel\Models\Base\User as BaseUser;
use Propel\Models\Map\UserTableMap;
use Propel\Runtime\Connection\ConnectionInterface;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Skeleton subclass for representing a row from the 'user' table.
 *
 * You should add additional methods to this class to meet the application requirements.
 * This class will only be generated as long as it does not already exist in the output directory.
 */
class User extends BaseUser
{

	/**
	 * Проверка прав доступа
	 */
	public function checkAccess(string $code) : bool
	{
		$options = fenric('config::user')->all();

		return !! ($options['roles'][$this->getRole()]['accesses'][$code] ?? false);
	}

	/**
	 * Проверка прав доступа к рабочему столу
	 */
	public function haveAccessToDesktop() : bool
	{
		return $this->checkAccess('desktop');
	}

	/**
	 * Регистрация неудачной попытки входа
	 */
	public function registerUnsuccessfulLoginAttempt() : void
	{
		$this->setAuthenticationAttemptCount(
			$this->getAuthenticationAttemptCount() + 1
		);

		$this->save();
	}

	/**
	 * Сброс зарегистрированных неудачных попыток входа
	 */
	public function resetUnsuccessfulLoginAttempts() : void
	{
		$this->setAuthenticationAttemptCount(0);

		$this->save();
	}

	/**
	 * Генерация случайного кода
	 */
	public function generateCode() : string
	{
		$bytes = random_bytes(16);

		$uniqueString = bin2hex($bytes);
		$uniqueString .= microtime(true);

		return hash('sha1', $uniqueString);
	}

	/**
	 * Является ли учетная запись текущей
	 */
	public function isMe() : bool
	{
		if (fenric('user')->isLogged())
		{
			if (strcmp(fenric('user')->getId(), $this->getId()) === 0)
			{
				return true;
			}
		}

		return false;
	}

	/**
	 * Получение имени роли
	 */
	public function getRoleName() : string
	{
		$options = fenric('config::user')->all();

		return $options['roles'][$this->getRole()]['name'] ?? $this->getRole();
	}

	/**
	 * Имеет ли учетная запись права администратора
	 */
	public function isAdministrator() : bool
	{
		return strcmp($this->getRole(), 'administrator') === 0;
	}

	/**
	 * Имеет ли учетная запись права редактора
	 */
	public function isRedactor() : bool
	{
		return strcmp($this->getRole(), 'redactor') === 0;
	}

	/**
	 * Имеет ли учетная запись права модератора
	 */
	public function isModerator() : bool
	{
		return strcmp($this->getRole(), 'moderator') === 0;
	}

	/**
	 * Имеет ли учетная запись права пользователя
	 */
	public function isUser() : bool
	{
		return strcmp($this->getRole(), 'user') === 0;
	}

	/**
	 * Принадлежит ли учетная запись мужчине
	 */
	public function isMale() : bool
	{
		return strcmp($this->getGender(), 'male') === 0;
	}

	/**
	 * Принадлежит ли учетная запись женщине
	 */
	public function isFemale() : bool
	{
		return strcmp($this->getGender(), 'female') === 0;
	}

	/**
	 * Является ли учетная запись заблокированной
	 */
	public function isBlocked() : bool
	{
		if ($this->getBanFrom() instanceof DateTime)
		{
			if ($this->getBanUntil() instanceof DateTime)
			{
				if ($this->getBanFrom()->getTimestamp() < time())
				{
					if ($this->getBanUntil()->getTimestamp() > time())
					{
						return true;
					}
				}
			}
		}

		return false;
	}

	public function isUnblocked() : bool
	{
		return ! $this->isBlocked();
	}

	/**
	 * Является ли учетная запись «онлайн»
	 */
	public function isOnline() : bool
	{
		if ($this->getTrackAt() instanceof DateTime)
		{
			if ($this->getTrackAt()->getTimestamp() > time() - 600)
			{
				return true;
			}
		}

		return false;
	}

	public function isOffline() : bool
	{
		return ! $this->isOnline();
	}

	/**
	 * Получение возраста пользователя
	 */
	public function getAge() : ?DateInterval
	{
		if ($this->getBirthday() instanceof DateTime)
		{
			return (new DateTime('now'))->diff(
				$this->getBirthday()
			);
		}

		return null;
	}

	/**
	 * Является ли пользователь взрослым
	 */
	public function isAdult() : bool
	{
		$adulthood = fenric('config::user')->get('adulthood', 18);

		if ($this->getAge() instanceof DateInterval)
		{
			if ($this->getAge()->y >= $adulthood)
			{
				return true;
			}
		}

		return false;
	}

	/**
	 * Получение имени пользователя
	 */
	public function getName() : ?string
	{
		$parts = [];

		if ($this->getFirstname()) {
			$parts[] = $this->getFirstname();
		}
		if ($this->getLastname()) {
			$parts[] = $this->getLastname();
		}

		if (count($parts)) {
			return implode(' ', $parts);
		}

		return null;
	}

	/**
	 * Установка даты рождения с предварительной валидацией,
	 * ORM создает сразу экземпляр DateTime, который может выбросить исключение в случаи некорректной даты,
	 * что может вызвать дополнительные проблемы для пользователя...
	 */
	public function setBirthday($value)
	{
		if (strlen($value) > 0)
		{
			if (preg_match('/^(?<year>\d{4})-(?<month>\d{2})-(?<day>\d{2})$/', $value, $matches))
			{
				if (checkdate($matches['month'], $matches['day'], $matches['year']))
				{
					parent::setBirthday($value);
				}
			}
		}

		return $this;
	}

	/**
	 * Верификация пароля
	 */
	public function verifyPassword(string $password, ConnectionInterface $connection = null) : bool
	{
		return password_verify($password, $this->getPassword($connection));
	}

	/**
	 * Установка параметров
	 */
	public function setParams($params)
	{
		return parent::setParams(json_encode($params));
	}

	/**
	 * Получение параметров
	 */
	public function getParams(ConnectionInterface $connection = null)
	{
		return json_decode(parent::getParams($connection), true);
	}

	/**
	 * Code to be run before persisting the object
	 *
	 * @param   ConnectionInterface   $connection
	 *
	 * @access  public
	 * @return  bool
	 */
	public function preSave(ConnectionInterface $connection = null)
	{
		if ($this->isColumnModified(UserTableMap::COL_PASSWORD))
		{
			$this->setVirtualColumn('password', $this->getPassword());

			$this->setPassword(password_hash($this->getPassword(), PASSWORD_BCRYPT, ['cost' => 12]));
		}

		return true;
	}

	/**
	 * Code to be run before inserting to database
	 *
	 * @param   ConnectionInterface   $connection
	 *
	 * @access  public
	 * @return  bool
	 */
	public function preInsert(ConnectionInterface $connection = null)
	{
		$this->setParams(
			['desktop' => new \stdClass]
		);

		$this->setRegistrationAt(
			new DateTime('now')
		);

		$this->setRegistrationIp(
			fenric('request')->environment->get('REMOTE_ADDR', '127.0.0.1')
		);

		$this->setRegistrationConfirmationCode(
			$this->generateCode()
		);

		return true;
	}

	/**
	 * Code to be run after inserting to database
	 *
	 * @param   ConnectionInterface   $connection
	 *
	 * @access  public
	 * @return  void
	 */
	public function postInsert(ConnectionInterface $connection = null)
	{
		fenric('event::user.created')->run([$this, $connection]);
	}

	/**
	 * Configure validators constraints.
	 *
	 * The Validator object uses this method to perform object validation
	 *
	 * @param   ClassMetadata   $metadata
	 *
	 * @access  public
	 * @return  void
	 */
	public static function loadValidatorMetadata(ClassMetadata $metadata)
	{
		$metadata->addPropertyConstraint('role', new Callback(function($value, ExecutionContextInterface $context, $payload)
		{
			$roles = ['administrator', 'redactor', 'moderator', 'user'];

			if ($context->getRoot()->isMe()) {
				if ($context->getRoot()->isColumnModified(UserTableMap::COL_ROLE)) {
					$context->addViolation(fenric()->t('user', 'validation.error.role.permission'));
					return false;
				}
			}

			if ($context->getRoot()->getRole()) {
				if (! in_array($context->getRoot()->getRole(), $roles, true)) {
					$context->addViolation(fenric()->t('user', 'validation.error.role.unknown'));
					return false;
				}
			}
		}));

		$metadata->addPropertyConstraint('password', new Callback(function($value, ExecutionContextInterface $context, $payload)
		{
			if ($context->getRoot()->isColumnModified(UserTableMap::COL_PASSWORD)) {
				if (stripos($context->getRoot()->getPassword(), strstr($context->getRoot()->getEmail(), '@', true)) !== false) {
					$context->addViolation(fenric()->t('user', 'validation.error.password.have.in.email'));
					return false;
				}
				if (stripos($context->getRoot()->getPassword(), $context->getRoot()->getUsername()) !== false) {
					$context->addViolation(fenric()->t('user', 'validation.error.password.have.in.username'));
					return false;
				}
				if ($context->getRoot()->hasVirtualColumn('password_confirmation')) {
					if (strcmp($context->getRoot()->getPassword(), $context->getRoot()->getVirtualColumn('password_confirmation')) !== 0) {
						$context->addViolation(fenric()->t('user', 'validation.error.password.comparison'));
						return false;
					}
				}
			}
		}));

		$metadata->addPropertyConstraint('gender', new Callback(function($value, ExecutionContextInterface $context, $payload)
		{
			if ($context->getRoot()->getGender()) {
				if (! preg_match('/^(?:male|female)$/', $context->getRoot()->getGender())) {
					$context->addViolation(fenric()->t('user', 'validation.error.gender.invalid'));
					return false;
				}
			}
		}));

		$metadata->addPropertyConstraint('firstname', new Callback(function($value, ExecutionContextInterface $context, $payload)
		{
			if ($context->getRoot()->getFirstname()) {
				if (mb_strlen($context->getRoot()->getFirstname(), 'utf-8') > 64) {
					$context->addViolation(fenric()->t('user', 'validation.error.firstname.length'));
					return false;
				}
				if (! preg_match('/^[\p{L}]+$/u', $context->getRoot()->getFirstname())) {
					$context->addViolation(fenric()->t('user', 'validation.error.firstname.characters'));
					return false;
				}
			}
		}));

		$metadata->addPropertyConstraint('lastname', new Callback(function($value, ExecutionContextInterface $context, $payload)
		{
			if ($context->getRoot()->getLastname()) {
				if (mb_strlen($context->getRoot()->getLastname(), 'utf-8') > 64) {
					$context->addViolation(fenric()->t('user', 'validation.error.lastname.length'));
					return false;
				}
				if (! preg_match('/^[\p{L}]+$/u', $context->getRoot()->getLastname())) {
					$context->addViolation(fenric()->t('user', 'validation.error.lastname.characters'));
					return false;
				}
			}
		}));

		$metadata->addPropertyConstraint('signature', new Callback(function($value, ExecutionContextInterface $context, $payload)
		{
			if ($context->getRoot()->getSignature()) {
				if (mb_strlen($context->getRoot()->getSignature(), 'utf-8') > 255) {
					$context->addViolation(fenric()->t('user', 'validation.error.signature.length'));
					return false;
				}
			}
		}));

		$metadata->addPropertyConstraint('ban_from', new Callback(function($value, ExecutionContextInterface $context, $payload)
		{
			if (! empty($value) && $context->getRoot()->isMe() && $context->getRoot()->isColumnModified(UserTableMap::COL_BAN_FROM)) {
				$context->addViolation(fenric()->t('user', 'validation.error.ban.permission'));
				return false;
			}
		}));

		$metadata->addPropertyConstraint('ban_until', new Callback(function($value, ExecutionContextInterface $context, $payload)
		{
			if (! empty($value) && $context->getRoot()->isMe() && $context->getRoot()->isColumnModified(UserTableMap::COL_BAN_UNTIL)) {
				$context->addViolation(fenric()->t('user', 'validation.error.ban.permission'));
				return false;
			}
		}));

		$metadata->addPropertyConstraint('ban_reason', new Callback(function($value, ExecutionContextInterface $context, $payload)
		{
			if (! empty($value) && $context->getRoot()->isMe() && $context->getRoot()->isColumnModified(UserTableMap::COL_BAN_REASON)) {
				$context->addViolation(fenric()->t('user', 'validation.error.ban.permission'));
				return false;
			}
		}));

		fenric('event::user.validation')->run([$metadata]);
	}
}
