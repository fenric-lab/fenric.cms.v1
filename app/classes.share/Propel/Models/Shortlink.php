<?php

namespace Propel\Models;

use Propel\Models\Base\Shortlink as BaseShortlink;

/**
 * Skeleton subclass for representing a row from the 'shortlink' table.
 *
 * You should add additional methods to this class to meet the application requirements.
 * This class will only be generated as long as it does not already exist in the output directory.
 */
class Shortlink extends BaseShortlink
{

	/**
	 * Получение URI
	 */
	public function getUri() : string
	{
		return sprintf('/goto/%s/', $this->getCode());
	}

	/**
	 * Установка ссылки
	 */
	public function setLocation($location)
	{
		$uid = uniqid($location, true);

		$this->setCode(hash('md5', $uid));

		return parent::setLocation($location);
	}
}
