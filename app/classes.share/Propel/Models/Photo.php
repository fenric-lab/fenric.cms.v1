<?php

namespace Propel\Models;

use Propel\Models\Base\Photo as BasePhoto;
use Propel\Models\Map\PhotoTableMap;
use Propel\Runtime\Connection\ConnectionInterface;

/**
 * Skeleton subclass for representing a row from the 'photo' table.
 *
 * You should add additional methods to this class to meet the application requirements.
 * This class will only be generated as long as it does not already exist in the output directory.
 */
class Photo extends BasePhoto
{

	/**
	 * Получение абсолютного пути фотографии
	 */
	public function getPath() : string
	{
		return fenric()->path('upload',
			substr($this->getFile(), 0, 2),
			substr($this->getFile(), 2, 2),
			substr($this->getFile(), 4, 2),
			$this->getFile()
		);
	}

	/**
	 * Code to be run before inserting to database
	 */
	public function preInsert(ConnectionInterface $connection = null)
	{
		$this->setSequence(fenric('query')
		->max(PhotoTableMap::COL_SEQUENCE)
		->from(PhotoTableMap::TABLE_NAME)
		->where(PhotoTableMap::COL_ALBUM_ID, '=', $this->getAlbum()->getId())
		->readOne() + 1);

		return true;
	}

	/**
	 * Code to be run after deleting the object in database
	 */
	public function postDelete(ConnectionInterface $con = null)
	{
		unlink($this->getPath());
	}
}
