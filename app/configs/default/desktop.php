<?php

$options = [];

$options['modules'] = [
	'icon',
	'modal',
	'notify',
	'params',
	'request',
];

$options['components'] = [
	'admin',
	'albums',
	'sections',
	'publications',
	'tags',
	'snippets',
	'parameters',
	'menu',
	'menu.items',
	'banners',
	'banners.groups',
	'polls',
	'polls.variants',
	'shortlinks',
	'users',
	'uploader',
	'ckeditor',
	'formhandle',
	'settings',
	'update',
];

return $options;
