'use strict';

(function()
{
	var $component;

	/**
	 * Конструктор компонента
	 *
	 * @access  public
	 * @return  void
	 */
	$component = function()
	{
		// ID компонента
		this.id = 'update';

		// Имя компонента
		this.name = 'Обновление системы';

		// Иконка компонента для рабочего стола
		this.appicon = this.root + '/res/icon@32.png';

		// Иконка компонента для модального окна
		this.favicon = 'refresh';

		// Экземпляр модального окна компонента
		this.modal = $desktop.module('modal').create({
			icon: this.favicon, title: this.name,
		});
	};

	/**
	 * Запуск обновления
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.run = function()
	{
		var container;

		this.with(function(self)
		{
			self.modal.open(800, 600).block();

			$desktop.module('request').get('/admin/update/', {repeat: true, success: function(response)
			{
				container = document.createElement('pre');

				container.appendChild(document.createTextNode(response));

				self.modal.content(container).unblock();
			}});
		});
	};

	/**
	 * Инициализация компонента
	 *
	 * @param   callback   complete
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.__init__ = function(complete)
	{
		this.with(function(self)
		{
			$desktop.module('icon').add({id: self.id, label: self.name, image: self.appicon, click: function(event)
			{
				self.run();
			}});
		});

		complete();
	};

	/**
	 * Регистрация компонента на рабочем столе
	 */
	$desktop.regcom('update', $component);
})();
