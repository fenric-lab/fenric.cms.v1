<form class="form">
	<nav class="top">
		<div class="btn-group">
			<button type="submit" class="btn btn-sm btn-default">
				<i class="fa fa-search" aria-hidden="true"></i> Поиск
			</button>
		</div>
	</nav>
	<div class="form-group">
		<label>Сортировка объектов</label>
		<select class="form-control input-sm" name="sort">
			<option value="id_asc" {{when params.sort is equal | id_asc}}selected{{endwhen params.sort}}>&darr; ID</option>
			<option value="id_desc" {{when params.sort is equal | id_desc}}selected{{endwhen params.sort}}>&uarr; ID</option>
			<option value="created_at_asc" {{when params.sort is equal | created_at_asc}}selected{{endwhen params.sort}}>&darr; Дата создания</option>
			<option value="created_at_desc" {{when params.sort is equal | created_at_desc}}selected{{endwhen params.sort}}>&uarr; Дата создания</option>
			<option value="updated_at_asc" {{when params.sort is equal | updated_at_asc}}selected{{endwhen params.sort}}>&darr; Дата обновления</option>
			<option value="updated_at_desc" {{when params.sort is equal | updated_at_desc}}selected{{endwhen params.sort}}>&uarr; Дата обновления</option>
		</select>
	</div>
	<div class="form-group">
		<label>Объектов на странице</label>
		<select class="form-control input-sm" name="limit">
			<option value="1" {{when params.limit is equal | 1}}selected{{endwhen params.limit}}>1</option>
			<option value="5" {{when params.limit is equal | 5}}selected{{endwhen params.limit}}>5</option>
			<option value="25" {{when params.limit is equal | 25}}selected{{endwhen params.limit}}>25</option>
			<option value="50" {{when params.limit is equal | 50}}selected{{endwhen params.limit}}>50</option>
			<option value="75" {{when params.limit is equal | 75}}selected{{endwhen params.limit}}>75</option>
			<option value="100" {{when params.limit is equal | 100}}selected{{endwhen params.limit}}>100</option>
			<option value="250" {{when params.limit is equal | 250}}selected{{endwhen params.limit}}>250</option>
		</select>
	</div>
	<hr>
	<div class="form-group">
		<label>ID</label>
		<input class="form-control input-sm" type="text" name="id" value="{{params.id}}" />
	</div>
</form>
