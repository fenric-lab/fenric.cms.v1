'use strict';

(function()
{
	var $component;

	/**
	 * Конструктор компонента
	 *
	 * @access  public
	 * @return  void
	 */
	$component = function()
	{
		this.id = 'menu';
		this.title = 'Меню';
		this.icon = this.root + '/res/icon@32.png';

		this.modals = {};
		this.params = $desktop.module('params').create();

		this.params.default = {};
		this.params.default.page = 1;
		this.params.default.limit = 25;

		this.params.load(this.params.default);

		this.routes = {};
		this.routes.all = '/admin/api/menu/all/?&{params}';
		this.routes.unload = '/admin/api/menu/unload/';
		this.routes.children = '/admin/api/menu/children/{id}/';
		this.routes.create = '/admin/api/menu/create/';
		this.routes.update = '/admin/api/menu/update/{id}/';
		this.routes.delete = '/admin/api/menu/delete/{id}/';
		this.routes.read = '/admin/api/menu/read/{id}/';

		this.routes.items = {};
		this.routes.items.sort = '/admin/api/menu/sort/{menuId}/';
		this.routes.items.delete = '/admin/api/menu/delete-child/{itemId}/';

		this.templates = {};
		this.templates.list = '/assets/views/menu-list.tpl';
		this.templates.form = '/assets/views/menu-form.tpl';
		this.templates.tree = '/assets/views/menu-tree.tpl';
		this.templates.item = '/assets/views/menu-item.tpl';
	};

	/**
	 * Список объектов
	 *
	 * @param   object   options
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.list = function(options)
	{
		options = options || {};

		if (options.page === undefined) {
			options.page = 1;
		}

		if (options.reset === true) {
			this.params.clear(this.params.default);
		}

		for (var key in options) {
			this.params.set(key, options[key]);
		}

		this.with(function(self)
		{
			self.modal().title(self.title).open().block();

			$desktop.module('request').get(self.routes.all, {repeat: true, params: self.params.toSerialize(), success: function(items)
			{
				$bugaboo.load(self.templates.list, function(tpl)
				{
					self.modal().content(tpl.format({
						params: self.params,
						items: items,
					})).unblock();

					self.modal().search('[data-toggle=tooltip]').forEach(function(element)
					{
						jQuery(element).tooltip();
					});

					self.modal().search('.delete[data-toggle=confirmation]').forEach(function(element)
					{
						jQuery(element).confirmation({onConfirm: function()
						{
							self.delete(element.getAttribute('data-id'));
						}});
					});

					self.modal().submit(function(event)
					{
						self.params.load(this);

						self.list();
					});

					self.modal().getBodyNode().scrollTop = 0;
				});
			}});
		});
	};

	/**
	 * Добавление объекта
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.add = function()
	{
		var modal;

		this.with(function(self)
		{
			modal = self.modal(Math.random());

			modal.title('Создание меню').open();

			self.form(modal, null, function(event, form)
			{
				modal.block();

				$desktop.module('request').post(self.routes.create, form, {repeat: true, success: function(response)
				{
					if (response.success)
					{
						self.edit(response.created.id);

						modal.close();

						return;
					}

					$desktop.component('formhandle').handle(form, response);

				}}).complete(function()
				{
					modal.unblock();
				});
			});
		});
	};

	/**
	 * Редактирование объекта
	 *
	 * @param   number   id
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.edit = function(id)
	{
		var modal;

		this.with(function(self)
		{
			modal = self.modal(id);

			modal.title('...').open().block();

			$desktop.module('request').get(self.routes.read, {repeat: true, id: id, success: function(item)
			{
				modal.title(item.title);

				self.form(modal, item, function(event, form)
				{
					modal.block();

					$desktop.module('request').patch(self.routes.update, form, {repeat: true, id: item.id, success: function(response)
					{
						$desktop.component('formhandle').handle(form, response);

					}}).complete(function()
					{
						modal.unblock();
					});
				});
			}});
		});
	};

	/**
	 * Удаление объекта
	 *
	 * @param   number   id
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.delete = function(id)
	{
		this.with(function(self)
		{
			$desktop.module('request').delete(self.routes.delete, {repeat: true, id: id, success: function(response)
			{
				self.list();
			}});
		});
	};

	/**
	 * Простая выгрузка объектов
	 *
	 * @param   callback   complete
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.unload = function(complete)
	{
		this.with(function(self)
		{
			$desktop.module('request').get(self.routes.unload, {repeat: true, success: function(items)
			{
				complete.call(this, items);
			}});
		});
	};

	/**
	 * Выгрузка элементов меню
	 *
	 * @param   int   menuId
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.items = function(menuId)
	{
		var modal, tree;

		this.with(function(self)
		{
			modal = self.modal(menuId * 1000000);

			modal.title('...').open().block();

			$desktop.module('request').get(self.routes.read, {repeat: true, id: menuId, success: function(menu)
			{
				modal.title(menu.title);

				$desktop.module('request').get(self.routes.children, {repeat: true, id: menuId, success: function(children)
				{
					$bugaboo.load(self.templates.tree, function(tplTree)
					{
						$bugaboo.load(self.templates.item, function(tplItem)
						{
							modal.content(tplTree.format({
								menu: menu,
								children: children,
							})).unblock();

							if (children.count > 0)
							{
								modal.replace('div.tree', self.tree(tplItem, children.items));

								jQuery(modal.find('div.tree > ul')).nestedSortable({listType: 'ul', items: 'li', toleranceElement: '> div', placeholder: 'ui-state-highlight', forcePlaceholderSize: true, update: function(event, ui)
								{
									var nesting = jQuery(this).nestedSortable('serialize', {attribute: 'data-nesting'});

									$desktop.module('request').patch(self.routes.items.sort, nesting, {repeat: true, menuId: menuId});
								}});

								modal.search('.delete').forEach(function(element)
								{
									jQuery(element).confirmation({onConfirm: function()
									{
										$desktop.module('request').delete(self.routes.items.delete, {repeat: true, itemId: element.getAttribute('data-id'), success: function(response)
										{
											self.items(menuId);
										}});
									}});
								});

								modal.search('[data-toggle=tooltip]').forEach(function(element)
								{
									jQuery(element).tooltip({
										// @continue
									});
								});
							}
						});
					});
				}});
			}});
		});
	};

	/**
	 * Формирование дерева объектов
	 *
	 * @param   object   tpl
	 * @param   array    items
	 * @param   int      parentId
	 *
	 * @access  public
	 * @return  Node
	 */
	$component.prototype.tree = function(tpl, items, parentId)
	{
		var i, ul, li;

		ul = document.createElement('ul');

		for (i = 0; i < items.length; i++)
		{
			if (items[i].parent_id == parentId)
			{
				li = document.createElement('li');

				li.setAttribute('data-nesting', 'list-' + items[i].id);

				li.appendChild(tpl.format(items[i]));

				li.appendChild(this.tree(tpl, items, items[i].id, false));

				ul.appendChild(li);
			}
		}

		return ul;
	};

	/**
	 * Основная форма компонента
	 *
	 * @param   object     modal
	 * @param   mixed      params
	 * @param   callback   onsubmit
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.form = function(modal, params, onsubmit)
	{
		params = params || {};

		this.with(function(self)
		{
			$bugaboo.load(self.templates.form, function(tpl)
			{
				modal.content(tpl.format(params)).unblock();

				modal.submit(function(event)
				{
					onsubmit.call(this, event, this);
				});
			});
		});
	};

	/**
	 * Модальные окна компонента
	 *
	 * @param   mixed   key
	 *
	 * @access  public
	 * @return  object
	 */
	$component.prototype.modal = function(key)
	{
		if (this.modals[key] === undefined) {
			this.modals[key] = $desktop.module('modal').create({icon: 'fa-bars'});
		}

		return this.modals[key];
	};

	/**
	 * Инициализация компонента
	 *
	 * @param   callback   complete
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.__init__ = function(complete)
	{
		this.with(function(self)
		{
			$desktop.module('icon').add({id: self.id, label: self.title, image: self.icon, click: function(event)
			{
				self.list();
			}});
		});

		complete();
	};

	/**
	 * Регистрация компонента на рабочем столе
	 */
	$desktop.regcom('menu', $component);
})();
