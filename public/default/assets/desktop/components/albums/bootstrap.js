'use strict';

(function()
{
	var $component;

	/**
	 * Конструктор компонента
	 *
	 * @access  public
	 * @return  void
	 */
	$component = function()
	{
		this.id = 'albums';
		this.title = 'Альбомы';
		this.icon = this.root + '/res/icon@32.png';

		this.modals = {};
		this.params = $desktop.module('params').create();

		this.params.default = {};
		this.params.default.page = 1;
		this.params.default.limit = 25;

		this.params.load(this.params.default);

		this.routes = {};
		this.routes.all = '/admin/api/album/all/?&{params}';
		this.routes.unload = '/admin/api/album/unload/';
		this.routes.children = '/admin/api/album/children/{id}/';
		this.routes.create = '/admin/api/album/create/';
		this.routes.update = '/admin/api/album/update/{id}/';
		this.routes.delete = '/admin/api/album/delete/{id}/';
		this.routes.read = '/admin/api/album/read/{id}/';

		this.routes.photos = {};
		this.routes.photos.create = '/admin/api/photo/create/';
		this.routes.photos.delete = '/admin/api/photo/delete/{id}/';
		this.routes.photos.sort = '/admin/api/photo/sort/';

		this.templates = {};
		this.templates.list = '/assets/views/album-list.tpl';
		this.templates.photo = '/assets/views/album-photo.tpl';
		this.templates.photos = '/assets/views/album-photos.tpl';
		this.templates.form = '/assets/views/album-form.tpl';
	};

	/**
	 * Список объектов
	 *
	 * @param   object   options
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.list = function(options)
	{
		options = options || {};

		if (options.page === undefined) {
			options.page = 1;
		}

		if (options.reset === true) {
			this.params.clear(this.params.default);
		}

		for (var key in options) {
			this.params.set(key, options[key]);
		}

		this.with(function(self)
		{
			self.modal().title(self.title).open().block();

			$desktop.module('request').get(self.routes.all, {repeat: true, params: self.params.toSerialize(), success: function(items)
			{
				$bugaboo.load(self.templates.list, function(tpl)
				{
					self.modal().content(tpl.format({
						params: self.params,
						items: items,
					})).unblock();

					self.modal().search('[data-toggle=tooltip]').forEach(function(element)
					{
						jQuery(element).tooltip();
					});

					self.modal().search('.delete[data-toggle=confirmation]').forEach(function(element)
					{
						jQuery(element).confirmation({onConfirm: function()
						{
							self.delete(element.getAttribute('data-id'));
						}});
					});

					self.modal().submit(function(event)
					{
						self.params.load(this);

						self.list();
					});

					self.modal().getBodyNode().scrollTop = 0;
				});
			}});
		});
	};

	/**
	 * Добавление объекта
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.add = function()
	{
		var modal;

		this.with(function(self)
		{
			modal = self.modal(Math.random());

			modal.title('Создание альбома').open();

			self.form(modal, null, function(event, form)
			{
				modal.block();

				$desktop.module('request').post(self.routes.create, form, {repeat: true, success: function(response)
				{
					if (response.success)
					{
						self.edit(response.created.id);

						modal.close();

						return;
					}

					$desktop.component('formhandle').handle(form, response);

				}}).complete(function()
				{
					modal.unblock();
				});
			});
		});
	};

	/**
	 * Редактирование объекта
	 *
	 * @param   number   id
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.edit = function(id)
	{
		var modal;

		this.with(function(self)
		{
			modal = self.modal(id);

			modal.title('...').open().block();

			$desktop.module('request').get(self.routes.read, {repeat: true, id: id, success: function(item)
			{
				modal.title(item.header);

				self.form(modal, item, function(event, form)
				{
					modal.block();

					$desktop.module('request').patch(self.routes.update, form, {repeat: true, id: item.id, success: function(response)
					{
						$desktop.component('formhandle').handle(form, response);

					}}).complete(function()
					{
						modal.unblock();
					});
				});
			}});
		});
	};

	/**
	 * Удаление объекта
	 *
	 * @param   number   id
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.delete = function(id)
	{
		this.with(function(self)
		{
			$desktop.module('request').delete(self.routes.delete, {repeat: true, id: id, success: function(response)
			{
				self.list();
			}});
		});
	};

	/**
	 * Простая выгрузка объектов
	 *
	 * @param   callback   complete
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.unload = function(complete)
	{
		this.with(function(self)
		{
			$desktop.module('request').get(self.routes.unload, {repeat: true, success: function(items)
			{
				complete.call(this, items);
			}});
		});
	};

	/**
	 * Управление фотографиями альбома
	 *
	 * @param   number   albumId
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.photos = function(albumId)
	{
		var modal, gallery;

		this.with(function(self)
		{
			modal = self.modal(albumId * 1000000);

			modal.title('...').open().block();

			$desktop.module('request').get(self.routes.read, {repeat: true, id: albumId, success: function(item)
			{
				modal.title(item.header);

				$desktop.module('request').get(self.routes.children, {repeat: true, id: albumId, success: function(children)
				{
					$bugaboo.load(self.templates.photo, function(tplPhoto)
					{
						$bugaboo.load(self.templates.photos, function(tplPhotos)
						{
							modal.content(tplPhotos.format({
								item: item,
								children: children,
							})).unblock();

							gallery = modal.find('div.gallery');

							(children.count > 0) && children.items.forEach(function(child)
							{
								gallery.appendChild(tplPhoto.format(child));
							});

							modal.listen('.upload', 'change', function(event)
							{
								var i = 0;
								var queue = event.target.files.length;

								for (; i < event.target.files.length; i++)
								{
									modal.block();

									$desktop.component('uploader').image(event.target.files[i], function(upload)
									{
										$desktop.module('request').post(self.routes.photos.create, {album_id: albumId, file: upload.file}, {repeat: true, success: function(response)
										{
											(queue === 0) && self.photos(albumId);
										}});

									}).complete(function(response)
									{
										(--queue === 0) && self.photos(albumId);
									});
								}
							});

							modal.search('.delete').forEach(function(element)
							{
								jQuery(element).confirmation({onConfirm: function()
								{
									$desktop.module('request').delete(self.routes.photos.delete, {repeat: true, id: element.getAttribute('data-id'), success: function(response)
									{
										modal.remove('div.photo[data-id="' + element.getAttribute('data-id') + '"]');
									}});
								}});
							});

							jQuery(gallery).sortable({handle: 'img', update: function(event, ui)
							{
								$desktop.module('request').patch(self.routes.photos.sort, {id: jQuery(gallery).sortable('toArray', {attribute: 'data-id'})}, {repeat: true});
							}});
						});
					});
				}});
			}});
		});
	};

	/**
	 * Основная форма компонента
	 *
	 * @param   object     modal
	 * @param   mixed      params
	 * @param   callback   onsubmit
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.form = function(modal, params, onsubmit)
	{
		params = params || {};

		this.with(function(self)
		{
			$bugaboo.load(self.templates.form, function(tpl)
			{
				modal.content(tpl.format(params)).unblock();

				modal.search('textarea.ckeditor').forEach(function(element) {
					$desktop.component('ckeditor').init(element);
				});

				modal.submit(function(event) {
					onsubmit.call(this, event, this);
				});
			});
		});
	};

	/**
	 * Модальные окна компонента
	 *
	 * @param   mixed   key
	 *
	 * @access  public
	 * @return  object
	 */
	$component.prototype.modal = function(key)
	{
		if (this.modals[key] === undefined) {
			this.modals[key] = $desktop.module('modal').create({icon: 'fa-camera'});
		}

		return this.modals[key];
	};

	/**
	 * Инициализация компонента
	 *
	 * @param   callback   complete
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.__init__ = function(complete)
	{
		this.with(function(self)
		{
			$desktop.module('icon').add({id: self.id, label: self.title, image: self.icon, click: function(event)
			{
				self.list();
			}});
		});

		complete();
	};

	/**
	 * Регистрация компонента на рабочем столе
	 */
	$desktop.regcom('albums', $component);
})();
