'use strict';

(function()
{
	var $component;

	/**
	 * Конструктор компонента
	 *
	 * @access  public
	 * @return  void
	 */
	$component = function()
	{
		this.id = 'polls.variants';
		this.title = 'Варианты опроса';
		this.addTitle = 'Создание варианта опроса';
		this.modalIcon = 'fa-pie-chart';

		this.modals = {};
		this.params = $desktop.module('params').create();

		this.params.default = {};
		this.params.default.page = 1;
		this.params.default.limit = 25;

		this.params.load(this.params.default);

		this.routes = {};
		this.routes.all = '/admin/api/poll-variant/all/?&{params}';
		this.routes.create = '/admin/api/poll-variant/create/{pollId}/';
		this.routes.update = '/admin/api/poll-variant/update/{id}/';
		this.routes.delete = '/admin/api/poll-variant/delete/{id}/';
		this.routes.unload = '/admin/api/poll-variant/unload/';
		this.routes.read = '/admin/api/poll-variant/read/{id}/';

		this.templates = {};
		this.templates.list = this.root + '/views/list.tpl';
		this.templates.form = this.root + '/views/form.tpl';
	};

	/**
	 * Список объектов
	 *
	 * @param   object   options
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.list = function(options)
	{
		options = options || {};

		if (options.page === undefined) {
			options.page = 1;
		}
		if (options.reset === true) {
			this.params.clear(this.params.default);
		}

		for (var key in options) {
			this.params.set(key, options[key]);
		}

		this.with(function(self)
		{
			self.modal().title(self.title).open().block();

			$desktop.component('polls').read(options.poll_id, function(poll)
			{
				self.modal().title(poll.title);

				$desktop.module('request').get(self.routes.all, {repeat: true, params: self.params.toSerialize(), success: function(items)
				{
					$bugaboo.load(self.templates.list, function(tpl)
					{
						self.modal().content(tpl.format({
							params: self.params,
							items: items,
							poll: poll,
						})).unblock();

						self.modal().search('[data-toggle=tooltip]').forEach(function(element) {
							jQuery(element).tooltip();
						});

						self.modal().search('.delete[data-toggle=confirmation]').forEach(function(element) {
							jQuery(element).confirmation({onConfirm: function() {
								self.delete(element.getAttribute('data-id'), function(response) {
									self.list(options);
								});
							}});
						});

						self.modal().submit(function(event) {
							self.params.load(this);
							self.list(poll.id);
						});

						self.modal().getBodyNode().scrollTop = 0;
					});
				}});
			});
		});
	};

	/**
	 * Добавление объекта
	 *
	 * @param   number   pollId
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.add = function(pollId)
	{
		var modal;

		this.with(function(self)
		{
			modal = self.modal(Math.random());
			modal.title(self.addTitle).open();

			self.form(modal, {}, function(event, form)
			{
				modal.block();

				$desktop.module('request').post(self.routes.create, form, {pollId: pollId, repeat: true, success: function(response)
				{
					if (response.success) {
						self.edit(response.created.id);
						modal.close();
						return;
					}

					$desktop.component('formhandle').handle(form, response);

				}}).complete(function()
				{
					modal.unblock();
				});
			});
		});
	};

	/**
	 * Редактирование объекта
	 *
	 * @param   number   id
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.edit = function(id)
	{
		var modal;

		this.with(function(self)
		{
			modal = self.modal(id);
			modal.title('...').open().block();

			$desktop.module('request').get(self.routes.read, {repeat: true, id: id, success: function(item)
			{
				modal.title(item.title);

				self.form(modal, item, function(event, form)
				{
					modal.block();

					$desktop.module('request').patch(self.routes.update, form, {repeat: true, id: item.id, success: function(response)
					{
						$desktop.component('formhandle').handle(form, response);

					}}).complete(function()
					{
						modal.unblock();
					});
				});
			}});
		});
	};

	/**
	 * Удаление объекта
	 *
	 * @param   number     id
	 * @param   callback   complete
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.delete = function(id, complete)
	{
		this.with(function(self) {
			$desktop.module('request').delete(self.routes.delete, {repeat: true, id: id, success: function(response) {
				complete.call(this, response);
			}});
		});
	};

	/**
	 * Основная форма компонента
	 *
	 * @param   object     modal
	 * @param   mixed      params
	 * @param   callback   onsubmit
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.form = function(modal, params, onsubmit)
	{
		params = params || {};

		this.with(function(self) {
			$bugaboo.load(self.templates.form, function(tpl) {
				modal.content(tpl.format(params)).unblock().submit(function(event) {
					onsubmit.call(this, event, this);
				});
			});
		});
	};

	/**
	 * Модальные окна компонента
	 *
	 * @param   mixed   key
	 *
	 * @access  public
	 * @return  object
	 */
	$component.prototype.modal = function(key)
	{
		if (this.modals[key] === undefined) {
			this.modals[key] = $desktop.module('modal').create({
				icon: this.modalIcon,
			});
		}

		return this.modals[key];
	};

	/**
	 * Регистрация компонента на рабочем столе
	 */
	$desktop.regcom('polls.variants', $component);
})();
