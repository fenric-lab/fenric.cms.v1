<form class="form">
	<nav class="top">
		<div class="clearfix">
			<div class="btn-group pull-left">
				<button type="button" class="btn btn-sm btn-default" onclick="$desktop.component('shortlinks').list()">
					<i class="fa fa-refresh" aria-hidden="true"></i> Обновить | {{items.count|0}}
				</button>
				<button type="button" class="btn btn-sm btn-default" onclick="$desktop.component('shortlinks').add()">
					<i class="fa fa-plus" aria-hidden="true"></i> Создать
				</button>
			</div>
			<div class="btn-group pull-right">
				<button type="button" class="btn btn-sm btn-default" onclick="$desktop.component('shortlinks').filter()">
					<i class="fa fa-filter" aria-hidden="true"></i> Фильтр
				</button>
				<button type="button" class="btn btn-sm btn-info" onclick="$desktop.component('shortlinks').help()">
					<i class="fa fa-life-ring" aria-hidden="true"></i> Помощь
				</button>
			</div>
		</div>
	</nav>

	{{when items.items is not empty}}
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<td width="30%">
						<span class="text-muted">Основное</span>
					</td>
					<td width="30%">
						<span class="text-muted">Статистика</span>
					</td>
					<td width="30%">
						<span class="text-muted">История изменений</span>
					</td>
					<td width="10%" align="center">
						<span class="text-muted">Управление</span>
					</td>
				</tr>
			</thead>
			<tbody>
				{{repeat items.items}}
					<tr data-id="{{id}}">
						<td>
							<p><strong>ID</strong>
							<br><span>{{id}}</span></p>

							<p><strong>Имя</strong>
							<br><span>{{title}}</span></p>

							<p><strong>Исходный URL</strong>
							<br><span>{{location}}</span></p>

							<p><strong>Преобразованный URL</strong>
							<br><a href="{{uri}}">{{uri}}</a></p>
						</td>
						<td>
							<p><strong>Количество переходов</strong>
							<br><span>{{referrals|0}}</span></p>
						</td>
						<td>
							<p><strong>Создано</strong>
							<br><span>{{created_at:datetime(d.m.Y H:i P)}}</span>
							{{when creator is not empty}}
								<br>by <a href="javascript:void(0)">@{{creator.username}}</a>
							{{endwhen creator}}
							</p>

							<p><strong>Обновлено</strong>
							<br><span>{{updated_at:datetime(d.m.Y H:i P)}}</span>
							{{when updater is not empty}}
								<br>by <a href="javascript:void(0)">@{{updater.username}}</a>
							{{endwhen updater}}
							</p>
						</td>
						<td>
							<div class="btn-group-vertical btn-block">
								<button class="btn btn-block btn-sm btn-warning" type="button" onclick="$desktop.component('shortlinks').edit({{id}})">
									<small>Редактировать</small>
								</button>
								<button class="btn btn-block btn-sm btn-danger delete" type="button" data-toggle="confirmation" data-id="{{id}}">
									<small>Удалить</small>
								</button>
							</div>
						</td>
					</tr>
				{{endrepeat items.items}}
			</tbody>
		</table>
	{{endwhen items.items}}

	{{when items.pagination.have is true}}
		<ul class="pagination">
			{{when items.pagination.links.current is greater than | this.items.pagination.links.first}}
				<li>
					<a href="javascript:$desktop.component('shortlinks').list({page: {{items.pagination.links.first}}})">
						<span>Первая</span>
					</a>
				</li>
				<li>
					<a href="javascript:$desktop.component('shortlinks').list({page: {{items.pagination.links.previous}}})">
						<span>Назад</span>
					</a>
				</li>
			{{endwhen items.pagination.links.current}}

			{{list pages start=items.pagination.links.start end=items.pagination.links.end as=page}}
				{{when page is equal | this.__parent__.items.pagination.links.current}}
					<li class="disabled">
						<span>{{page}}</span>
					</li>
				{{endwhen page}}

				{{when page is not equal | this.__parent__.items.pagination.links.current}}
					<li>
						<a href="javascript:$desktop.component('shortlinks').list({page: {{page}}})">
							<span>{{page}}</span>
						</a>
					</li>
				{{endwhen page}}
			{{endlist pages}}

			{{when items.pagination.links.current is less than | this.items.pagination.links.last}}
				<li>
					<a href="javascript:$desktop.component('shortlinks').list({page: {{items.pagination.links.next}}})">
						<span>Следующая</span>
					</a>
				</li>
				<li>
					<a href="javascript:$desktop.component('shortlinks').list({page: {{items.pagination.links.last}}})">
						<span>Последняя</span>
					</a>
				</li>
			{{endwhen items.pagination.links.current}}
		</ul>
	{{endwhen items.pagination.have}}
</form>
