'use strict';

(function()
{
	var $component;

	/**
	 * Конструктор компонента
	 *
	 * @access  public
	 * @return  void
	 */
	$component = function()
	{
		this.id = 'shortlinks';

		this.icons = {};
		this.icons.component = this.root + '/res/icon@32.png';
		this.icons.modals = 'link';

		this.titles = {};
		this.titles.list = 'Короткие ссылки';
		this.titles.add = 'Создание короткой ссылки';
		this.titles.edit = 'Редактирование короткой ссылки';
		this.titles.filter = 'Фильтрация коротких ссылок';
		this.titles.help = 'Помощь по компоненту «Короткие ссылки»';

		this.modals = {};
		this.params = $desktop.module('params').create();

		this.params.default = {};
		this.params.default.sort = 'id_desc';
		this.params.default.page = 1;
		this.params.default.limit = 25;

		this.params.load(this.params.default);

		this.routes = {};
		this.routes.all = '/admin/api/shortlink/all/?&{params}';
		this.routes.create = '/admin/api/shortlink/create/';
		this.routes.update = '/admin/api/shortlink/update/{id}/';
		this.routes.delete = '/admin/api/shortlink/delete/{id}/';
		this.routes.read = '/admin/api/shortlink/read/{id}/';

		this.templates = {};
		this.templates.list = this.root + '/views/list.tpl';
		this.templates.form = this.root + '/views/form.tpl';
		this.templates.filter = this.root + '/views/filter.tpl';
		this.templates.help = this.root + '/views/help.tpl';
	};

	/**
	 * Список объектов
	 *
	 * @param   object   options
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.list = function(options)
	{
		options = options || {};

		if (options.page === undefined) {
			options.page = 1;
		}

		if (options.reset === true) {
			this.params.clear(this.params.default);
		}

		for (var key in options) {
			this.params.set(key, options[key]);
		}

		this.with(function(self)
		{
			self.modal().title(self.titles.list).open().block();

			$desktop.module('request').get(self.routes.all, {repeat: true, params: self.params.toSerialize(), success: function(items)
			{
				$bugaboo.load(self.templates.list, function(tpl)
				{
					self.modal().content(tpl.format({
						params: self.params,
						items: items,
					})).unblock();

					self.modal().search('.delete[data-toggle=confirmation]', function(element)
					{
						jQuery(element).confirmation({onConfirm: function()
						{
							self.delete(element.getAttribute('data-id'));
						}});
					});

					self.modal().submit(function(event)
					{
						self.params.load(this);
						self.list();
					});

					self.modal().getBodyNode().scrollTop = 0;
				});
			}});
		});
	};

	/**
	 * Добавление объекта
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.add = function()
	{
		var modal;

		this.with(function(self)
		{
			modal = self.modal(Math.random());

			modal.title(self.titles.add).open();

			self.form(modal, null, function(event, form)
			{
				modal.block();

				$desktop.module('request').post(self.routes.create, form, {repeat: true, success: function(response)
				{
					if (response.success)
					{
						self.edit(response.created.id);

						modal.close();

						return;
					}

					$desktop.component('formhandle').handle(form, response);

				}}).complete(function()
				{
					modal.unblock();
				});
			});
		});
	};

	/**
	 * Редактирование объекта
	 *
	 * @param   number   id
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.edit = function(id)
	{
		var modal;

		this.with(function(self)
		{
			modal = self.modal(id);

			modal.title(self.titles.edit).open().block();

			$desktop.module('request').get(self.routes.read, {repeat: true, id: id, success: function(item)
			{
				self.form(modal, item, function(event, form)
				{
					modal.block();

					$desktop.module('request').patch(self.routes.update, form, {repeat: true, id: item.id, success: function(response)
					{
						$desktop.component('formhandle').handle(form, response);

					}}).complete(function()
					{
						modal.unblock();
					});
				});
			}});
		});
	};

	/**
	 * Удаление объекта
	 *
	 * @param   number   id
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.delete = function(id)
	{
		this.with(function(self)
		{
			$desktop.module('request').delete(self.routes.delete, {repeat: true, id: id, success: function(response)
			{
				self.list();
			}});
		});
	};

	/**
	 * Основная форма компонента
	 *
	 * @param   object     modal
	 * @param   mixed      params
	 * @param   callback   onsubmit
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.form = function(modal, params, onsubmit)
	{
		params = params || {};

		this.with(function(self)
		{
			$bugaboo.load(self.templates.form, function(tpl)
			{
				modal.content(tpl.format(params));

				modal.unblock().submit(function(event)
				{
					onsubmit.call(this, event, this);
				});
			});
		});
	};

	/**
	 * Фильтр компонента
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.filter = function()
	{
		var modal;

		this.with(function(self)
		{
			modal = self.modal('filter');

			modal.title(self.titles.filter).open(60, 60, true).block();

			$bugaboo.load(self.templates.filter, function(tpl)
			{
				modal.content(tpl.format({
					params: self.params.all(),
				}));

				modal.search('input.datetimepicker', function(element)
				{
					jQuery(element).datetimepicker({format: 'Y-m-d'});
				});

				modal.unblock().submit(function(event, form, params)
				{
					self.list(params.all());

					modal.close();
				});
			});
		});
	};

	/**
	 * Помощь компонента
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.help = function()
	{
		var modal;

		this.with(function(self)
		{
			modal = self.modal('help');

			modal.title(self.titles.help).open(60, 60, true).block();

			$bugaboo.load(self.templates.help, function(tpl)
			{
				modal.content(tpl.format()).unblock();
			});
		});
	};

	/**
	 * Модальные окна компонента
	 *
	 * @param   mixed   key
	 *
	 * @access  public
	 * @return  object
	 */
	$component.prototype.modal = function(key)
	{
		if (this.modals[key] === undefined) {
			this.modals[key] = $desktop.module('modal').create({
				icon: this.icons.modals,
			});
		}

		return this.modals[key];
	};

	/**
	 * Инициализация компонента
	 *
	 * @param   callback   complete
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.__init__ = function(complete)
	{
		this.with(function(self)
		{
			$desktop.module('icon').add({id: self.id, label: self.titles.list, image: self.icons.component, click: function(event)
			{
				self.list();
			}});
		});

		complete();
	};

	/**
	 * Регистрация компонента на рабочем столе
	 */
	$desktop.regcom('shortlinks', $component);
})();
