'use strict';

(function()
{
	var $component;

	/**
	 * Компонент рабочего стола
	 *
	 * @access  public
	 * @return  void
	 */
	$component = function()
	{
		this.modal = $desktop.module('modal').create(
		{
			icon: 'fa-paint-brush', width: 346, height: 420, title: "Персонализация рабочего стола",
		});
	};

	/**
	 * Открытие приложения
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.open = function()
	{
		$desktop.component('settings').modal.open().block();

		$bugaboo.load('/assets/views/com-settings.tpl', function(tpl)
		{
			$desktop.component('settings').modal.content(tpl.format()).unblock();

			$desktop.component('settings').modal.search('.desktop-pallet').forEach(function(element)
			{
				element.addEventListener('click', function(event)
				{
					$desktop.decorate(this.getAttribute('data-value'), function(pallet)
					{
						$desktop.module('request').patch('/admin/api/save-desktop-palette/', {'palette': pallet}, {repeat: true});
					});
				});
			});

			$desktop.component('settings').modal.find('.desktop-wallpaper').addEventListener('change', function(event)
			{
				event.preventDefault();
				event.target.disabled = true;

				$desktop.module('request').put('/user/api/upload-image/', this.files[0], {repeat: true, success: function(response)
				{
					$desktop.app.style.backgroundImage = 'url("/upload/' + response.file + '")';

					$desktop.module('request').patch('/admin/api/save-desktop-wallpaper/', {'wallpaper': response.file}, {repeat: true});

				}}).complete(function()
				{
					event.target.value = null;
					event.target.disabled = false;
				});
			});

			$desktop.component('settings').modal.find('.sort-desktop-icons').addEventListener('click', function(event)
			{
				event.preventDefault();

				$desktop.module('icon').sort();
			});
		});
	};

	/**
	 * Инициализация компонента рабочего стола
	 *
	 * @param   callback  complete
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.__init__ = function(complete)
	{
		// Добавление иконки на рабочий стол
		$desktop.module('icon').add(
		{
			// Идентификатор иконки рабочего стола
			id: 'settings',

			// Наименование иконки рабочего стола
			label: "Персонализация рабочего стола",

			// Изображение иконки рабочего стола
			image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAJ8klEQVRYR7WXeXRU1R3HP7Nnsk0CCWQjZIECmhAVwhYggEIlodiqKLIaiCxWe9BTK1pbwVgUSsIuVRATUSMuLY2lPWgFIgIBQhRiEpIAAtn3ZBImM+/Ne6/nvSwmotj+0TtnzsyZe+fez/19f9vT0T22bEt/MSIiYoWvj4+XLCs9P/8fPnUoiszlb6+cbG2qf0KnnrBr165HJiVMeC82Jg6DwfCjh94K68fm1AN09J+VZYmW1lY++uiDgxpAxrbNmStTVy91Oh3k5+cjiAJ6nb4XRDEY8GlswPvSZRQFFL32t35D/f37Qye5sYeHcyMsFJ0kof5ZfQ0fNpzhw0dw5OiRNm2nrdvTsx5bvnJJc3MTGVsziI6O1gDUxeqQrVbCz+QTlbUft8WC3u1Gm7qZox+DXhBojr2d4hWp2uEGnY7r168zfvwE5iT/giO5n9u1Lbbs2Jy5YtmqpY2NDbzz7jskJCSATtWqC0AyWxiUn0/kG3tpu/MOfC8UohMEbc2thnprFb7o+bU4goMx66C4qJjQ0GCSZidz5Fi3BfoCZL2dxdixY9H1BbBYCDqbT3j2Aepn3k3IJ4dQVCv8BIB6a70kUb48hfqESVjcbkrLyoiMiCBpdtJ3EvQCNDWyL3Mfd91xV5d5u3WVPCwEf3kCn/OFtE4cT+TO17qmfgoAMDgcVD74ABXzHsBDFCkrK9ckTtYs8HmXD/QANDU1snffHuJGx/WTWFItcPwEitOJGBxE9MZ0ZM29f8IJVIDOThpm3s3V1OUYJDeXL11i5IhRPwyg+sCeN/cQExPTT1rZbCbw1GncBgM6i4WIVzchq/5hMHwvwG72CKNLoP7OOL5KWYwkSVRXVpI4ZRpJSX0k2Loj/b3UlBWPqBZ4443XGXnbyH4urgGcPIVgNmMwGgnbsAnZbELvcqHovwvXH3JIvSzTNDScM6tS0ZlM1NfWMmPaDJKSkvuE4Y7N2akpK+erYbj7L7sZNnwY+j7mVQEGqBYwmzAaDIRvSqf6/l8y8FwB1mvXkbvdpVcQFUqNedUJgRuDB3FqzRMoFjP1NbVMT5zO7NnJHO2Jgq19AHbu3kV0VJQWBb1Dp2rpRLKYCSgsImL7Li7+fi1KfQMD/vZ3PNvb8XO5uOHnh+RyYbVaEU0mPOrqUIwmpOgoTix4CKePFw01tUxLnK5FwdFjR7sTUTeAKsHOXTsZGhHeH0Al0etRnTH0ZB5BB3M4//zvaLHbCcz+mBBRoOV6BcaBA/GwemB3uYj29aWqqgpPXxuDbb7kzZyBY+gQGqqrbw2wfed2hgwJuwlA9TnJYmLIiTyM1TWU3DcH2eGg7qODxApualtamGax4LJY6BBcCFZP2h0OEo1GzrmcFCVMwBY/htqKih8HUH1gy7YMQkJCegFUHbX8ryi4TUYi8gugs5OrUyfjqYPDOYe49m0Vq328qZAljuj1jBfdfKbTkyBL2GSZg62tTL1rNGOSZlFxTQWY9n0JMrJTUx7TnDBjSzqDgwbfLIGako1GgkougiRRFxuDl9HAZ/8+xqdHjuPv748gy+hlBZeiYEFBRKflC6mjg7iRw3l44UOaLIlTErsAco/1+EAXgOoDW7dtITgkSAtDWZZ764FaprX0LEm9WdBkMlNefIGTx4+C3qAlz5tzU1f9D4+IIPHuJGpq60icMvWHAez2Nta/tB6jGuMGE263pL3Vg40GNaAUvL098fHxQnAJ6PQGnG3VDAweSkhYBG63iNrMyIqMIsvad1U6Z6eTqitFeNkCqG9s5d6Zs5gzZy5Hc3ujICN7Zeqq+ecKCvjg448ZHz8OXx8fHA6HVpItWgIyIUhuCouKqbx2hdtGjEDRG6G5kMH+VkwWD2S30GWd7ira0yLoTZ5UX7uEEhiPJMOUhATGjh3H8S+/6K4F2zMOrFi+4qE3M7OIix3NlozNfHWxBE8fb3x9bfj5+yEp4Gex8vjcuVxsamTWz2cxKCiMkty9mOpyCR3sj+i6gU7Xt6NSNE3cem8uXG5m3MIdoMiapWw2P06fOd3VD+x4bXtWypKUJZn79zNx4nga6xspeW03SlsrZjW2zWZ0Lhc/cwoEdzrIih/Dk8+uZYD/ACq+OUr54ZcIDg7V6oMgSkiygl6vx2w0oipnb21AHzSRO+e+gFt0gqxg9fTk088Ot2sAubm5Uysrr+65WlEVOS1xqik8MpLBF8twp72M0NCArNcjt7fj0Ok4O3smxzysjB0dx9AhQxDdEtUXc7nRUoXLYcfDfp5h4YMoLb+KwzO8Myg6XtDpLYTHzsRg9kIUXBqcy+WipLjk9d58+9RTT1lFWdq/4rGUB6qrayivrSXZy4eO36xB6OjQQrA6ZQktE8ZRcraAT3I+YeGCBdhsNgxmD0wWL9oqv+Y260XG3R5K+aUrHPwyL73QPvklURS1c9RQjYqK0jK8+n3t2rVdPtAznnvhuXcXL1q4oKmpiXXr0khKTmZWXh6V7x9ASJqN89erEO12WlpaOJuXT1RUJPfcc4+mqcnDh/rCvxJuriDQW+ZGp4uDh47lPJfddN8t27a+k2kb0rIfvP9X880WM5Io84cNrzCsuIT4ggL0f0pDjL0dl92uhVZpSSn+/n4sWrQIp9OJ2erLN//aSKinHbNYh+DWkV9aLx6+Gry7orr+ZGlxaV5NTU11d513q72u1lT1BdiwMe39h+fNf1h0iwQGDMJ+o4Onl6Zgzv2CRfvfwhAQoGXB2tpacnIOsXzZMkaNGokoutEZjJR9uonRwwKRmooQMVNe0YLvuGeQ9BYKL3x9dv26tPubm5tbu6u3ALj7A7yS9sHChYvmdTod+PrY8PTzo+ytTLaueZr6KZOJHBateXBRUREpy1J4dOmjCIKgJSo1KZ1677cMHxqIqzoPyeDLt7XtjFv0Ov6BYZy/UMDK1Y/PO33y9D8Bo3o4IPQDePXPGz5cvHDxg232Vvxs/hi9vLmalUXFH9fRuv5FCi5dwmqxEBIaQviQ0E5FUWRZUbO9HjdGOouzLfcmxhuMrhpErOSeyMMROq/d08eG40Y7WZlvrz5xIu9DwAyI6rsfwMsvr8tc/fiTS93u7lsZDNQXF1OV+wWWyZO0WwYEBHAm/0zHM08/O0+SpFaj0ahlnk5R1AVbO0a+sDgmLX7ijKDKyxeETfs+35hbbjwgCILTw8PD7XQ67erS7ttLqhT9AGJjY8c8ueaJNydPSogLDQ3Tno5EWUJQFFxtbbS2tVJcUtyU/W52ek7OP/Z236In46p7GYI8iRoRSkyDnevFdZzrPlDVu++63ge5m/pqm83mPypm1PSw4JDxkVERoR4WD2+35BYrq6oaK69XlhSVleTWV9WXdm+omrHvHj3t4S2fmPpO/lRjr5ZAVS+V2PVf7/o/LPwPVDbDWPVqw1kAAAAASUVORK5CYII=',

			// Запуск приложения
			click: function(event)
			{
				$desktop.component('settings').open();
			},
		});

		complete();
	};

	/**
	 * Регистрация компонента рабочего стола
	 */
	$desktop.regcom('settings', $component);
})();
