'use strict';

(function()
{
	var $component;

	/**
	 * Конструктор компонента
	 *
	 * @access  public
	 * @return  void
	 */
	$component = function()
	{
		this.title = 'Группы баннеров';
		this.modalIcon = 'fa-anchor';

		this.modals = {};
		this.params = $desktop.module('params').create();

		this.params.default = {};
		this.params.default.page = 1;
		this.params.default.limit = 25;

		this.params.load(this.params.default);

		this.routes = {};
		this.routes.all = '/admin/api/banner-group/all/?&{params}';
		this.routes.unload = '/admin/api/banner-group/unload/';
		this.routes.create = '/admin/api/banner-group/create/';
		this.routes.update = '/admin/api/banner-group/update/{id}/';
		this.routes.delete = '/admin/api/banner-group/delete/{id}/';
		this.routes.read = '/admin/api/banner-group/read/{id}/';

		this.templates = {};
		this.templates.list = '/assets/views/banner-group-list.tpl';
		this.templates.form = '/assets/views/banner-group-form.tpl';
	};

	/**
	 * Список объектов
	 *
	 * @param   object   options
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.list = function(options)
	{
		options = options || {};

		if (options.page === undefined) {
			options.page = 1;
		}

		if (options.reset === true) {
			this.params.clear(this.params.default);
		}

		for (var key in options) {
			this.params.set(key, options[key]);
		}

		this.with(function(self)
		{
			self.modal().title(self.title).open().block();

			$desktop.module('request').get(self.routes.all, {repeat: true, params: self.params.toSerialize(), success: function(items)
			{
				$bugaboo.load(self.templates.list, function(tpl)
				{
					self.modal().content(tpl.format({
						params: self.params,
						items: items,
					})).unblock();

					self.modal().search('[data-toggle=tooltip]').forEach(function(element)
					{
						jQuery(element).tooltip();
					});

					self.modal().search('.delete[data-toggle=confirmation]').forEach(function(element)
					{
						jQuery(element).confirmation({onConfirm: function()
						{
							self.delete(element.getAttribute('data-id'));
						}});
					});

					self.modal().submit(function(event)
					{
						self.params.load(this);

						self.list();
					});

					self.modal().getBodyNode().scrollTop = 0;
				});
			}});
		});
	};

	/**
	 * Добавление объекта
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.add = function()
	{
		var modal;

		this.with(function(self)
		{
			modal = self.modal(Math.random());

			modal.title('Создание группы баннеров').open();

			self.form(modal, null, function(event, form)
			{
				modal.block();

				$desktop.module('request').post(self.routes.create, form, {repeat: true, success: function(response)
				{
					if (response.success)
					{
						self.edit(response.created.id);

						modal.close();

						return;
					}

					$desktop.component('formhandle').handle(form, response);

				}}).complete(function()
				{
					modal.unblock();
				});
			});
		});
	};

	/**
	 * Редактирование объекта
	 *
	 * @param   number   id
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.edit = function(id)
	{
		var modal;

		this.with(function(self)
		{
			modal = self.modal(id);

			modal.title('...').open().block();

			$desktop.module('request').get(self.routes.read, {repeat: true, id: id, success: function(item)
			{
				modal.title(item.title);

				self.form(modal, item, function(event, form)
				{
					modal.block();

					$desktop.module('request').patch(self.routes.update, form, {repeat: true, id: item.id, success: function(response)
					{
						$desktop.component('formhandle').handle(form, response);

					}}).complete(function()
					{
						modal.unblock();
					});
				});
			}});
		});
	};

	/**
	 * Удаление объекта
	 *
	 * @param   number   id
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.delete = function(id)
	{
		this.with(function(self)
		{
			$desktop.module('request').delete(self.routes.delete, {repeat: true, id: id, success: function(response)
			{
				self.list();
			}});
		});
	};

	/**
	 * Простая выгрузка объектов
	 *
	 * @param   callback   complete
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.unload = function(complete)
	{
		this.with(function(self)
		{
			$desktop.module('request').get(self.routes.unload, {repeat: true, success: function(items)
			{
				complete.call(this, items);
			}});
		});
	};

	/**
	 * Основная форма компонента
	 *
	 * @param   object     modal
	 * @param   mixed      params
	 * @param   callback   onsubmit
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.form = function(modal, params, onsubmit)
	{
		params = params || {};

		this.with(function(self)
		{
			$bugaboo.load(self.templates.form, function(tpl)
			{
				modal.content(tpl.format(params)).unblock().submit(function(event)
				{
					onsubmit.call(this, event, this);
				});
			});
		});
	};

	/**
	 * Модальные окна компонента
	 *
	 * @param   mixed   key
	 *
	 * @access  public
	 * @return  object
	 */
	$component.prototype.modal = function(key)
	{
		if (this.modals[key] === undefined)
		{
			this.modals[key] = $desktop.module('modal').create({icon: this.modalIcon});
		}

		return this.modals[key];
	};

	/**
	 * Регистрация компонента на рабочем столе
	 */
	$desktop.regcom('banners.groups', $component);
})();
