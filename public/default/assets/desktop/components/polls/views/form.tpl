<form class="form">
	<nav class="top">
		<div class="btn-group">
			<button type="submit" class="btn btn-sm btn-success">
				<i class="fa fa-floppy-o" aria-hidden="true"></i> Сохранить
			</button>
		</div>
	</nav>
	<div class="form-group" data-name="*">
		<div class="help-block error"></div>
	</div>
	<div class="form-group" data-name="title">
		<label>Наименование</label>
		<input class="form-control" type="text" name="title" value="{{title}}" />
		<div class="help-block error"></div>
	</div>
	<div class="form-group" data-name="code">
		<label>Символьный код</label>
		<input class="form-control" type="text" name="code" value="{{code}}" />
		<p class="help-block">Оставьте это поле пустым, чтобы система сгенерировала символьный код автоматически...</p>
		<div class="help-block error"></div>
	</div>
	<div class="form-group" data-name="multiple">
		<label>Возможность выбора нескольких вариантов</label>
		<select class="form-control" name="multiple">
			<option value="1" {{when multiple is equal | 1}}selected{{endwhen multiple}}>Да</option>
			<option value="0" {{when multiple is equal | 0}}selected{{endwhen multiple}}>Нет</option>
		</select>
		<div class="help-block error"></div>
	</div>

	<hr>
	<div class="form-group" data-name="open_at">
		<label>Дата начала опроса</label>
		<input class="form-control datetimepicker" type="text" name="open_at" value="{{open_at|now:datetime(Y-m-d H:i)}}" />
		<div class="help-block error"></div>
	</div>
	<div class="form-group" data-name="close_at">
		<label>Дата окончания опроса</label>
		<input class="form-control datetimepicker" type="text" name="close_at" value="{{close_at:datetime(Y-m-d H:i)}}" />
		<div class="help-block error"></div>
	</div>
</form>
