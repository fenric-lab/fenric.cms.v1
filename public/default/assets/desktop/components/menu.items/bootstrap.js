'use strict';

(function()
{
	var $component;

	/**
	 * Конструктор компонента
	 *
	 * @access  public
	 * @return  void
	 */
	$component = function()
	{
		this.modals = {};
		this.routes = {};
		this.templates = {};

		this.routes.create = '/admin/api/menu/create-child/{menuId}/';
		this.routes.update = '/admin/api/menu/update-child/{itemId}/';
		this.routes.read = '/admin/api/menu/read-child/{itemId}/';

		this.templates.form = '/assets/views/menu-item-form.tpl';
	};

	/**
	 * Добавление объекта
	 *
	 * @param   int   menuId
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.add = function(menuId)
	{
		var modal;

		this.with(function(self)
		{
			modal = self.modal(Math.random());

			modal.title('Создание элемента меню').open();

			self.form(modal, null, function(event, form)
			{
				modal.block();

				$desktop.module('request').post(self.routes.create, form, {repeat: true, menuId: menuId, success: function(response)
				{
					if (response.success)
					{
						self.edit(response.created.id);

						modal.close();

						return;
					}

					$desktop.component('formhandle').handle(form, response);

				}}).complete(function()
				{
					modal.unblock();
				});
			});
		});
	};

	/**
	 * Редактирование объекта
	 *
	 * @param   int   itemId
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.edit = function(itemId)
	{
		var modal;

		this.with(function(self)
		{
			modal = self.modal(itemId);

			modal.title('...').open().block();

			$desktop.module('request').get(self.routes.read, {repeat: true, itemId: itemId, success: function(item)
			{
				modal.title(item.content);

				self.form(modal, item, function(event, form)
				{
					modal.block();

					$desktop.module('request').patch(self.routes.update, form, {repeat: true, itemId: item.id, success: function(response)
					{
						$desktop.component('formhandle').handle(form, response);

					}}).complete(function()
					{
						modal.unblock();
					});
				});
			}});
		});
	};

	/**
	 * Основная форма компонента
	 *
	 * @param   object     modal
	 * @param   mixed      params
	 * @param   callback   onsubmit
	 *
	 * @access  public
	 * @return  void
	 */
	$component.prototype.form = function(modal, params, onsubmit)
	{
		params = params || {};

		this.with(function(self)
		{
			$bugaboo.load(self.templates.form, function(tpl)
			{
				modal.content(tpl.format(params)).unblock().submit(function(event)
				{
					onsubmit.call(this, event, this);
				});
			});
		});
	};

	/**
	 * Модальные окна компонента
	 *
	 * @param   mixed   key
	 *
	 * @access  public
	 * @return  object
	 */
	$component.prototype.modal = function(key)
	{
		if (this.modals[key] === undefined) {
			this.modals[key] = $desktop.module('modal').create();
		}

		return this.modals[key];
	};

	/**
	 * Регистрация компонента на рабочем столе
	 */
	$desktop.regcom('menu.items', $component);
})();
