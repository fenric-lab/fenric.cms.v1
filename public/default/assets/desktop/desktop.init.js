'use strict';

// Recommended icons pack:
// https://www.iconfinder.com/iconsets/fatcow

(function()
{
	$desktop.init();

	$request.get('/admin/api/desktop-modules/', {success: function(modules)
	{
		$request.get('/admin/api/desktop-components/', {success: function(components)
		{
			$desktop.render({modules: modules, components: components, onload: function()
			{
				$bugaboo.elementary['desktop'] = this;

				$bugaboo.formatters['datetime'] = function(value, format)
				{
					if (value.length === 0) {
						return '';
					}

					if (value === 'now') {
						value = new Date();
					}

					$.datetimepicker.setLocale('ru');

					var formatter = new DateFormatter();

					return formatter.formatDate(new Date(value), format);
				};

				this.module('modal').addEventListener('opening', function(modal)
				{
					var container, reflection;

					container = document.querySelector('.modal-reflections');

					if (container instanceof Node)
					{
						reflection = document.createElement('div');
						reflection.classList.add('modal-reflection');
						reflection.setAttribute('data-id', modal.id);

						reflection.appendChild(modal.getIconNode().cloneNode(true));
						reflection.appendChild(modal.getHeadNode().cloneNode(true));

						reflection.addEventListener('click', function(event) {
							$desktop.module('modal').modals[modal.id].foreground();
						});

						container.appendChild(reflection);
					}
				});

				this.module('modal').addEventListener('closed', function(modal)
				{
					var container;

					container = document.querySelector('.modal-reflections');

					if (container instanceof Node)
					{
						$desktop.search('.modal-reflection[data-id="{id}"]', {id: modal.id}).forEach(function(reflection)
						{
							container.removeChild(reflection);
						});
					}
				});

				this.module('modal').addEventListener('change.icon', function(modal)
				{
					$desktop.search('.modal-reflection[data-id="{id}"]', {id: modal.id}).forEach(function(reflection)
					{
						while (reflection.firstChild) {
							reflection.removeChild(reflection.firstChild);
						}

						reflection.appendChild(modal.getIconNode().cloneNode(true));
						reflection.appendChild(modal.getHeadNode().cloneNode(true));
					});
				});

				this.module('modal').addEventListener('change.title', function(modal)
				{
					$desktop.search('.modal-reflection[data-id="{id}"]', {id: modal.id}).forEach(function(reflection)
					{
						while (reflection.firstChild) {
							reflection.removeChild(reflection.firstChild);
						}

						reflection.appendChild(modal.getIconNode().cloneNode(true));
						reflection.appendChild(modal.getHeadNode().cloneNode(true));
					});
				});

				this.module('modal').addEventListener('backgrounded', function(modal)
				{
					$desktop.search('.modal-reflection[data-id="{id}"]', {id: modal.id}).forEach(function(reflection)
					{
						reflection.classList.remove('active');
					});
				});

				this.module('modal').addEventListener('foregrounded', function(modal)
				{
					$desktop.search('.modal-reflection[data-id="{id}"]', {id: modal.id}).forEach(function(reflection)
					{
						reflection.classList.add('active');
					});
				});

				this.module('request').onload(function(response, event)
				{
					var message = '';

					if (response instanceof Object) {
						if (response.message !== undefined) {
							message = response.message;
						}
					}

					if (this.status === 401) {
						$desktop.component('admin').login(function() {
							$desktop.module('request').runRepeatRequest();
						});
					}
					else if (this.status === 400) {
						$desktop.module('notify').warning('HTTP ошибка 400', message || 'Плохой или неверный запрос.');
					}
					else if (this.status === 403) {
						$desktop.module('notify').warning('HTTP ошибка 403', message || 'Недостаточно прав для выполнения запроса.');
					}
					else if (this.status === 404) {
						$desktop.module('notify').warning('HTTP ошибка 404', message || 'Не удалось найти ресурс, возможно он был удалён ранее.');
					}
					else if (this.status === 405) {
						$desktop.module('notify').warning('HTTP ошибка 405', message || 'Метод не поддерживается контроллером или сервером.');
					}
					else if (this.status === 413) {
						$desktop.module('notify').warning('HTTP ошибка 413', message || 'Сервер отвергнул запрос т.к. запрос оказался слишком большой.');
					}
					else if (this.status === 414) {
						$desktop.module('notify').warning('HTTP ошибка 414', message || 'Сервер отвергнул запрос т.к. URI слишком длинный.');
					}
					else if (this.status === 415) {
						$desktop.module('notify').warning('HTTP ошибка 415', message || 'Тип загружаемого файла не поддерживается.');
					}
					else if (this.status === 423) {
						$desktop.module('notify').warning('HTTP ошибка 423', message || 'Ресурс временно заблокирован.');
					}
					else if (this.status === 500) {
						$desktop.module('notify').error('HTTP ошибка 500', message || 'На сервере произошёл технический сбой.');
					}
					else if (this.status === 503) {
						$desktop.module('notify').error('HTTP ошибка 503', message || 'Не удалось выполнить операцию по техническим причинам.');
					}
					else if (this.status === 504) {
						$desktop.module('notify').error('HTTP ошибка 504', message || 'Сервер не успел обработать запрос за отведённое ему время, попробуйте выполнить действие ещё раз.');
					}
					else if (this.status < 200 || this.status > 202) {
						$desktop.module('notify').error('HTTP ошибка ' + this.status, message || 'Uncaught HTTP code...');
					}
				});

				this.component('admin').with(function()
				{
					if (this.account.desktop.palette) {
						$desktop.app.classList.add(this.account.desktop.palette);
					}

					if (this.account.desktop.wallpaper) {
						$desktop.app.style.backgroundImage = 'url("/upload/' + this.account.desktop.wallpaper + '")';
					}

					$desktop.module('icon').sort(this.account.desktop.icons || null);

					$desktop.show();
				});
			}});
		}});
	}});
})();
