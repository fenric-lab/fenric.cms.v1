<div class="photo pull-left" data-id="{{id}}" style="position: relative; margin: 0 5px 5px 0;">
	<img class="img-thumbnail" src="/upload/160x90/{{file}}" />
	<div style="position: absolute; bottom: 10px; right: 10px;">
		<div class="btn-group" role="group">
			<div class="btn-group" role="group">
				<div class="dropdown">
					<button class="btn btn-sm btn-primary dropdown-toggle" type="button" id="fd218ec5-98b6-40d5-8beb-ee6bdde72bfd-{{id}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
					<ul class="dropdown-menu" aria-labelledby="fd218ec5-98b6-40d5-8beb-ee6bdde72bfd-{{id}}">
						<li>
							<a href="/upload/{{file}}" target="_blank">Открыть</a>
						</li>
					</ul>
				</div>
			</div>
			<button class="btn btn-sm btn-danger delete" type="button" data-toggle="confirmation" data-id="{{id}}">
				<i class="fa fa-times" aria-hidden="true"></i>
			</button>
		</div>
	</div>
</div>
