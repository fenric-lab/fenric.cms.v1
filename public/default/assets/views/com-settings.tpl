<form class="form">
	<div class="form-group">
		<label>Цветовая схема</label>
		<div class="desktop-pallets">
			<div class="desktop-pallet blue" data-value="blue"></div>
			<div class="desktop-pallet red" data-value="red"></div>
			<div class="desktop-pallet pink" data-value="pink"></div>
			<div class="desktop-pallet purple" data-value="purple"></div>
			<div class="desktop-pallet violet" data-value="violet"></div>
			<div class="desktop-pallet cyan" data-value="cyan"></div>
			<div class="desktop-pallet green" data-value="green"></div>
			<div class="desktop-pallet lime" data-value="lime"></div>
			<div class="desktop-pallet orange" data-value="orange"></div>
			<div class="desktop-pallet brown" data-value="brown"></div>
			<div class="desktop-pallet jeans" data-value="jeans"></div>
			<div class="desktop-pallet black" data-value="black"></div>
		</div>
		<small class="help-block">Выбранная вами цветовая схема будет применена к модальным окнам<br/>и ряду других элементов связанных с рабочим столом…</small>
	</div>
	
	<div class="form-group">
		<label>Фоновое изображение</label>
		<div>
			<input class="desktop-wallpaper" type="file" accept="image/*" />
		</div>
		<small class="help-block">Выберите на вашем устройстве изображение, для того, чтобы сделать<br/>его фоновым изображением вашего рабочего стола…</small>
	</div>
	
	<div class="form-group">
		<label>Сортировка иконок</label>
		<div>
			<button class="btn btn-sm btn-default sort-desktop-icons">Применить</button>
		</div>
		<small class="help-block">Иконки по ряду причин могут оказываться за пределами видимой части<br/>рабочего стола, например когда у вас сменилось разрешение экрана…</small>
	</div>
</form>