<form class="form">
	<nav class="top">
		<div class="container-fluid">
			<div class="row">
				<div class="pull-left">
					<div class="btn-group">
						<button type="button" class="btn btn-sm btn-default" onclick="$desktop.component('banners').list()">
							<i class="fa fa-refresh" aria-hidden="true"></i> Обновить | {{items.count|0}}
						</button>
						<button type="button" class="btn btn-sm btn-default" onclick="$desktop.component('banners').add()">
							<i class="fa fa-plus" aria-hidden="true"></i> Создать
						</button>
					</div>
				</div>
				<div class="pull-right">
					<div class="btn-group">
						<button type="button" class="btn btn-sm btn-default" onclick="$desktop.component('banners.groups').list()">
							<i class="fa fa-bars" aria-hidden="true"></i> Управление группами
						</button>
					</div>
				</div>
			</div>
		</div>
	</nav>

	{{when items.items is not empty}}
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<td width="30%">
						<span class="text-muted">Основное</span>
					</td>
					<td width="30%">
						<span class="text-muted">Статистика</span>
					</td>
					<td width="30%">
						<span class="text-muted">Описание</span>
					</td>
					<td width="10%" align="center">
						<span class="text-muted">Управление</span>
					</td>
				</tr>
			</thead>
			<tbody>
				{{repeat items.items}}
					<tr data-id="{{id}}" data-toggle="popover" data-title="Предпросмотр" data-content="&lt;img src=&quot;/upload/{{picture}}&quot;&gt;">
						<td>
							<p><strong>ID</strong>
							<br><span>{{id}}</span></p>

							<p><strong>Наименование</strong>
							<br><span>{{title}}</span></p>

							<p><strong>Группа</strong>
							<br><span>{{group.title}}</span></p>

							<p><strong>Создано</strong>
							<br><span>{{created_at:datetime(j M Y H:i)}}</span></p>
						</td>
						<td>
							<p><strong>Кол-во показов</strong>
							<br><span>{{shows|0}}</span></p>

							{{when shows_limit is not empty}}
							<p><strong>Лимит на показы</strong>
							<br><span>{{shows_limit|0}}</span></p>
							{{endwhen shows_limit}}

							<p><strong>Кол-во переходов</strong>
							<br><span>{{clicks|0}}</span></p>

							{{when clicks_limit is not empty}}
							<p><strong>Лимит на переходы</strong>
							<br><span>{{clicks_limit|0}}</span></p>
							{{endwhen clicks_limit}}

							{{when show_start is not empty}}
								<p><strong>Дата начала показов</strong>
								<br><span>{{show_start:datetime(j M Y H:i)}}</span></p>
							{{endwhen show_start}}

							{{when show_end is not empty}}
								<p><strong>Дата окончания показов</strong>
								<br><span>{{show_end:datetime(j M Y H:i)}}</span></p>
							{{endwhen show_end}}
						</td>
						<td>
							{{when description is not empty}}
								<p><em>{{description}}</em></p>
							{{endwhen description}}
						</td>
						<td>
							<div class="btn-group-vertical btn-block">
								<button class="btn btn-block btn-sm btn-warning" type="button" onclick="$desktop.component('banners').edit({{id}})" data-toggle="tooltip" title="{{updated_at:datetime(j M Y H:i)}}">
									<small>Редактировать</small>
								</button>
								<button class="btn btn-block btn-sm btn-danger delete" type="button" data-toggle="confirmation" data-id="{{id}}">
									<small>Удалить</small>
								</button>
							</div>
						</td>
					</tr>
				{{endrepeat items.items}}
			</tbody>
		</table>
	{{endwhen items.items}}
</form>

{{when items.pagination.have is true}}
	<ul class="pagination">
		{{when items.pagination.links.current is greater than | this.items.pagination.links.first}}
			<li>
				<a href="javascript:$desktop.component('banners').list({page: {{items.pagination.links.first}}})">
					<span>Первая</span>
				</a>
			</li>
			<li>
				<a href="javascript:$desktop.component('banners').list({page: {{items.pagination.links.previous}}})">
					<span>Назад</span>
				</a>
			</li>
		{{endwhen items.pagination.links.current}}

		{{list pages start=items.pagination.links.start end=items.pagination.links.end as=page}}
			{{when page is equal | this.__parent__.items.pagination.links.current}}
				<li class="disabled">
					<span>{{page}}</span>
				</li>
			{{endwhen page}}

			{{when page is not equal | this.__parent__.items.pagination.links.current}}
				<li>
					<a href="javascript:$desktop.component('banners').list({page: {{page}}})">
						<span>{{page}}</span>
					</a>
				</li>
			{{endwhen page}}
		{{endlist pages}}

		{{when items.pagination.links.current is less than | this.items.pagination.links.last}}
			<li>
				<a href="javascript:$desktop.component('banners').list({page: {{items.pagination.links.next}}})">
					<span>Следующая</span>
				</a>
			</li>
			<li>
				<a href="javascript:$desktop.component('banners').list({page: {{items.pagination.links.last}}})">
					<span>Последняя</span>
				</a>
			</li>
		{{endwhen items.pagination.links.current}}
	</ul>
{{endwhen items.pagination.have}}
