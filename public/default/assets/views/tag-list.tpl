<form class="form">
	<nav class="top">
		<div class="btn-group">
			<button type="button" class="btn btn-sm btn-default" onclick="$desktop.component('tags').list()">
				<i class="fa fa-refresh" aria-hidden="true"></i> Обновить | {{items.count|0}}
			</button>
			<button type="button" class="btn btn-sm btn-default" onclick="$desktop.component('tags').add()">
				<i class="fa fa-plus" aria-hidden="true"></i> Создать
			</button>
		</div>
	</nav>

	{{when items.items is not empty}}
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<td width="45%">
						<span class="text-muted">Основное</span>
					</td>
					<td width="45%">
						<span class="text-muted">Метаданные</span>
					</td>
					<td width="10%" align="center">
						<span class="text-muted">Управление</span>
					</td>
				</tr>
			</thead>
			<tbody>
				{{repeat items.items}}
					<tr data-id="{{id}}">
						<td>
							<p><strong>ID</strong>
							<br><span>{{id}}</span></p>

							<p><strong>Заголовок</strong>
							<br><span>{{header}}</span></p>

							<p><strong>Символьный код</strong>
							<br><span>{{code}}</span></p>

							<p><strong>Создано</strong>
							<br><span>{{created_at:datetime(j M Y H:i)}}</span></p>
						</td>
						<td>
							{{when meta_title is empty}}
								<p><strong>Заголовок</strong>
								<br><span class="text-muted">{{header}}</span></p>
							{{endwhen meta_title}}

							{{when meta_title is not empty}}
								<p><strong>Заголовок</strong>
								<br><span>{{meta_title}}</span></p>
							{{endwhen meta_title}}

							{{when meta_keywords is not empty}}
								<p><strong>Ключевые слова</strong>
								<br><span>{{meta_keywords}}</span></p>
							{{endwhen meta_keywords}}

							{{when meta_description is not empty}}
								<p><strong>Описание</strong>
								<br><span>{{meta_description}}</span></p>
							{{endwhen meta_description}}

							<p><strong>Ссылка</strong>
							<br><a href="/tags/{{code}}/" target="_blank">/tags/{{code}}/</a></p>
						</td>
						<td>
							<div class="btn-group-vertical btn-block">
								<button class="btn btn-block btn-sm btn-primary" type="button" onclick="$desktop.component('publications').list({tag: {{id}}})" data-toggle="tooltip" title="{{publications|0}}">
									<small>Публикации</small>
								</button>
								<button class="btn btn-block btn-sm btn-warning" type="button" onclick="$desktop.component('tags').edit({{id}})" data-toggle="tooltip" title="{{updated_at:datetime(j M Y H:i)}}">
									<small>Редактировать</small>
								</button>
								<button class="btn btn-block btn-sm btn-danger delete" type="button" data-toggle="confirmation" data-id="{{id}}">
									<small>Удалить</small>
								</button>
							</div>
						</td>
					</tr>
				{{endrepeat items.items}}
			</tbody>
		</table>
	{{endwhen items.items}}
</form>

{{when items.pagination.have is true}}
	<ul class="pagination">
		{{when items.pagination.links.current is greater than | this.items.pagination.links.first}}
			<li>
				<a href="javascript:$desktop.component('tags').list({page: {{items.pagination.links.first}}})">
					<span>Первая</span>
				</a>
			</li>
			<li>
				<a href="javascript:$desktop.component('tags').list({page: {{items.pagination.links.previous}}})">
					<span>Назад</span>
				</a>
			</li>
		{{endwhen items.pagination.links.current}}

		{{list pages start=items.pagination.links.start end=items.pagination.links.end as=page}}
			{{when page is equal | this.__parent__.items.pagination.links.current}}
				<li class="disabled">
					<span>{{page}}</span>
				</li>
			{{endwhen page}}

			{{when page is not equal | this.__parent__.items.pagination.links.current}}
				<li>
					<a href="javascript:$desktop.component('tags').list({page: {{page}}})">
						<span>{{page}}</span>
					</a>
				</li>
			{{endwhen page}}
		{{endlist pages}}

		{{when items.pagination.links.current is less than | this.items.pagination.links.last}}
			<li>
				<a href="javascript:$desktop.component('tags').list({page: {{items.pagination.links.next}}})">
					<span>Следующая</span>
				</a>
			</li>
			<li>
				<a href="javascript:$desktop.component('tags').list({page: {{items.pagination.links.last}}})">
					<span>Последняя</span>
				</a>
			</li>
		{{endwhen items.pagination.links.current}}
	</ul>
{{endwhen items.pagination.have}}
