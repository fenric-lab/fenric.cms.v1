<form class="form">
	<nav class="top">
		<div class="btn-group">
			<button type="button" class="btn btn-sm btn-default" onclick="$desktop.component('menu').items({{menu.id}})">
				<i class="fa fa-refresh" aria-hidden="true"></i> Обновить | {{children.count|0}}
			</button>
			<button type="button" class="btn btn-sm btn-default" onclick="$desktop.component('menu.items').add({{menu.id}})">
				<i class="fa fa-plus" aria-hidden="true"></i> Создать
			</button>
		</div>
	</nav>
	<div class="tree component-menu-tree"></div>
</form>
