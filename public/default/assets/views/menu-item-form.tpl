<form class="form">
	<nav class="top">
		<div class="btn-group">
			<button type="submit" class="btn btn-sm btn-success">
				<i class="fa fa-floppy-o" aria-hidden="true"></i> Сохранить
			</button>
		</div>
	</nav>

	<div class="form-group" data-name="*">
		<div class="help-block error"></div>
	</div>

	<div class="form-group" data-name="content">
		<label>Содержимое</label>
		<input class="form-control" type="text" name="content" value="{{content}}" />
		<div class="help-block error"></div>
	</div>
	<div class="form-group" data-name="href">
		<label>Ссылка</label>
		<input class="form-control" type="text" name="href" value="{{href}}" />
		<div class="help-block error"></div>
	</div>
	<div class="form-group" data-name="target">
		<label>Поведение ссылки</label>
		<select class="form-control" name="target">
			<option value=""></option>
			<option value="_self" {{when target is equal | _self}}selected{{endwhen target}}>Открывается в этом окне</option>
			<option value="_blank" {{when target is equal | _blank}}selected{{endwhen target}}>Открывается в новом окне</option>
		</select>
		<div class="help-block error"></div>
	</div>
	<div class="form-group" data-name="display">
		<label>Видимость</label>
		<select class="form-control" name="display">
			<option value="true" {{when display is equal | 1}}selected{{endwhen display}}>Да</option>
			<option value="false" {{when display is equal | 0}}selected{{endwhen display}}>Нет</option>
		</select>
		<div class="help-block error"></div>
	</div>
</form>
