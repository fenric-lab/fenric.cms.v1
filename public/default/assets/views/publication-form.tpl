<form class="form">
	<nav class="top">
		<div class="btn-group">
			<button type="submit" class="btn btn-sm btn-success">
				<i class="fa fa-floppy-o" aria-hidden="true"></i> Сохранить
			</button>
		</div>
	</nav>

	<div class="form-group" data-name="*">
		<div class="help-block error"></div>
	</div>

	<div class="form-group" data-name="section_id">
		<label>Рубрика</label>
		<select class="form-control selectpicker" name="section_id">
			<option value=""></option>
			{{repeat sections}}
				<option value="{{id}}" {{when id is equal | this.__parent__.section_id}}selected{{endwhen id}}>{{header}}</option>
			{{endrepeat sections}}
		</select>
		<div class="help-block error"></div>
	</div>

	<hr>
	<div class="form-group" data-name="hot">
		<label>Горячая</label>
		<select class="form-control" name="hot">
			<option value="0" {{when hot is equal | 0}}selected{{endwhen hot}}>Нет</option>
			<option value="1" {{when hot is equal | 1}}selected{{endwhen hot}}>Да</option>
		</select>
		<div class="help-block error"></div>
	</div>
	<div class="form-group" data-name="header">
		<label>Заголовок</label>
		<input class="form-control" type="text" name="header" value="{{header}}" />
		<div class="help-block error"></div>
	</div>
	<div class="form-group" data-name="code">
		<label>Символьный код</label>
		<input class="form-control" type="text" name="code" value="{{code}}" />
		<p class="help-block">Оставьте это поле пустым, чтобы система сгенерировала символьный код автоматически...</p>
		<div class="help-block error"></div>
	</div>

	<hr>
	<div class="form-group" data-name="picture">
		<label>Изображение</label>
		<input class="form-control picture-upload" type="file" accept="image/*" />
		<div class="picture-container" style="margin: 10px 0;">
			{{when picture is not empty}}
				<img class="img-thumbnail" src="/upload/320x180/{{picture}}" />
			{{endwhen picture}}
		</div>
		<button type="button" class="btn btn-sm btn-warning picture-reset">
			<i class="fa fa-trash-o" aria-hidden="true"></i> Удалить изображение
		</button>
		<input type="hidden" name="picture" value="{{picture}}" />
		<div class="help-block error"></div>
	</div>
	<div class="form-group" data-name="picture_signature">
		<label>Подпись к изображению</label>
		<input class="form-control" type="text" name="picture_signature" value="{{picture_signature}}" />
		<div class="help-block error"></div>
	</div>

	<hr>
	<div class="form-group" data-name="anons">
		<label>Анонс</label>
		<textarea class="form-control ckeditor" name="anons" rows="5">{{anons}}</textarea>
		<div class="help-block error"></div>
	</div>
	<div class="form-group" data-name="content">
		<label>Содержимое</label>
		<textarea class="form-control ckeditor" name="content" rows="5">{{content}}</textarea>
		<div class="help-block error"></div>
	</div>

	<hr>
	<div class="form-group" data-name="meta_title">
		<label>Заголовок документа</label>
		<input class="form-control" type="text" name="meta_title" value="{{meta_title}}" />
		<div class="help-block error"></div>
	</div>
	<div class="form-group" data-name="meta_keywords">
		<label>Ключевые слова документа</label>
		<input class="form-control" type="text" name="meta_keywords" value="{{meta_keywords}}" />
		<div class="help-block error"></div>
	</div>
	<div class="form-group" data-name="meta_description">
		<label>Описание документа</label>
		<input class="form-control" type="text" name="meta_description" value="{{meta_description}}" />
		<div class="help-block error"></div>
	</div>

	<hr>
	<div class="form-group" data-name="show_at">
		<label>Дата активации</label>
		<input class="form-control datetimepicker" type="text" name="show_at" value="{{show_at|now:datetime(Y-m-d H:i)}}" />
		<div class="help-block error"></div>
	</div>
	<div class="form-group" data-name="hide_at">
		<label>Дата деактивации</label>
		<input class="form-control datetimepicker" type="text" name="hide_at" value="{{hide_at:datetime(Y-m-d H:i)}}" />
		<div class="help-block error"></div>
	</div>

	<hr>
	<div class="form-group" data-name="tags">
		<label>Теги</label>
		<div>
			{{when tags is not empty}}
				{{repeat tags}}
					{{when id in array | this.__parent__.attached_tags}}
						<label class="btn btn-sm btn-default">
							<input type="checkbox" name="tags[]" value="{{id}}" checked /> {{header}}
						</label>
					{{endwhen id}}

					{{when id not in array | this.__parent__.attached_tags}}
						<label class="btn btn-sm btn-default">
							<input type="checkbox" name="tags[]" value="{{id}}" /> {{header}}
						</label>
					{{endwhen id}}
				{{endrepeat tags}}
			{{endwhen tags}}
		</div>
		<div class="help-block error"></div>
	</div>
</form>
