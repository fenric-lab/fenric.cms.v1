<form class="form">
	<nav class="top">
		<div class="btn-group">
			<button type="submit" class="btn btn-sm btn-success">
				<i class="fa fa-floppy-o" aria-hidden="true"></i> Сохранить
			</button>
		</div>
	</nav>

	<div class="form-group" data-name="*">
		<div class="help-block error"></div>
	</div>

	<div class="form-group" data-name="banner_group_id">
		<label>Группа</label>
		<select class="form-control selectpicker" name="banner_group_id">
			<option value=""></option>
			{{repeat groups}}
				<option value="{{id}}" {{when id is equal | this.__parent__.banner_group_id}}selected{{endwhen id}}>{{title}}</option>
			{{endrepeat groups}}
		</select>
		<div class="help-block error"></div>
	</div>

	<hr>
	<div class="form-group" data-name="title">
		<label>Наименование</label>
		<input class="form-control" type="text" name="title" value="{{title}}" />
		<div class="help-block error"></div>
	</div>
	<div class="form-group" data-name="description">
		<label>Описание для себя</label>
		<textarea class="form-control" name="description" rows="3">{{description}}</textarea>
		<p class="help-block"></p>
		<div class="help-block error"></div>
	</div>

	<hr>
	<div class="form-group" data-name="picture">
		<label>Изображение</label>
		<input class="form-control picture-upload" type="file" accept="image/*" />
		<div class="picture-container" style="margin: 10px 0;">
			{{when picture is not empty}}
				<img class="img-thumbnail" src="/upload/{{picture}}" />
			{{endwhen picture}}
		</div>
		<input type="hidden" name="picture" value="{{picture}}" />
		<div class="help-block error"></div>
	</div>
	<div class="form-group" data-name="picture_alt">
		<label>Альтернативный текст изображения</label>
		<input class="form-control" type="text" name="picture_alt" value="{{picture_alt}}" />
		<p class="help-block">Содержимое будет отражено в атрибуте <b>alt</b> изображения.</p>
		<div class="help-block error"></div>
	</div>
	<div class="form-group" data-name="picture_title">
		<label>Описание изображения</label>
		<input class="form-control" type="text" name="picture_title" value="{{picture_title}}" />
		<p class="help-block">Содержимое будет отражено в атрибуте <b>title</b> изображения.</p>
		<div class="help-block error"></div>
	</div>

	<hr>
	<div class="form-group" data-name="hyperlink">
		<label>Гиперссылка</label>
		<input class="form-control" type="text" name="hyperlink" value="{{hyperlink}}" placeholder="https://www.example.com/" />
		<div class="help-block error"></div>
	</div>
	<div class="form-group" data-name="hyperlink_target">
		<label>Поведение гиперссылки</label>
		<select class="form-control" name="hyperlink_target">
			<option value="_self" {{when hyperlink_target is equal | _self}}selected{{endwhen hyperlink_target}}>Открывается в этом окне</option>
			<option value="_blank" {{when hyperlink_target is equal | _blank}}selected{{endwhen hyperlink_target}}>Открывается в новом окне</option>
		</select>
		<div class="help-block error"></div>
	</div>

	<hr>
	<div class="form-group" data-name="show_start">
		<label>Дата начала показа</label>
		<input class="form-control datetimepicker" type="text" name="show_start" value="{{show_start|now:datetime(Y-m-d H:i)}}" />
		<div class="help-block error"></div>
	</div>
	<div class="form-group" data-name="show_end">
		<label>Дата окончания показа</label>
		<input class="form-control datetimepicker" type="text" name="show_end" value="{{show_end:datetime(Y-m-d H:i)}}" />
		<div class="help-block error"></div>
	</div>

	<hr>
	<div class="form-group" data-name="shows_limit">
		<label>Максимальное количество показов</label>
		<input class="form-control" type="number" name="shows_limit" value="{{shows_limit}}" />
		<p class="help-block">При достижении лимита баннер перестанет показываться.</p>
		<div class="help-block error"></div>
	</div>
	<div class="form-group" data-name="clicks_limit">
		<label>Максимальное количество переходов</label>
		<input class="form-control" type="number" name="clicks_limit" value="{{clicks_limit}}" />
		<p class="help-block">При достижении лимита баннер перестанет показываться.</p>
		<div class="help-block error"></div>
	</div>
</form>
