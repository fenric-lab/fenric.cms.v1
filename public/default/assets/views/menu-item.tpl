<div class="component-menu-tree-item {{when display is equal | 0}}component-menu-tree-item-disabled{{endwhen display}}">
	<div class="container-fluid">
		<div class="row">
			<div class="pull-left">
				<p>{{content}}</p>
				<p class="text-muted">{{href}}</p>
			</div>
			<div class="pull-right">
				<a class="btn btn-sm btn-primary" href="{{href}}" target="_blank">
					<i class="fa fa-link" aria-hidden="true"></i>
				</a>
				<button class="btn btn-sm btn-warning" type="button" onclick="$desktop.component('menu.items').edit({{id}})" data-toggle="tooltip" title="{{updated_at:datetime(j M Y H:i)}}">
					<i class="fa fa-pencil" aria-hidden="true"></i>
				</button>
				<button class="btn btn-sm btn-danger delete" type="button" data-toggle="confirmation" data-id="{{id}}">
					<i class="fa fa-trash" aria-hidden="true"></i>
				</button>
			</div>
		</div>
	</div>
</div>
