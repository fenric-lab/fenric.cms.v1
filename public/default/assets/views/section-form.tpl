<form class="form">
	<nav class="top">
		<div class="btn-group">
			<button type="submit" class="btn btn-sm btn-success">
				<i class="fa fa-floppy-o" aria-hidden="true"></i> Сохранить
			</button>
		</div>
	</nav>

	<div class="form-group" data-name="*">
		<div class="help-block error"></div>
	</div>

	<div class="form-group" data-name="header">
		<label>Заголовок</label>
		<input class="form-control" type="text" name="header" value="{{header}}" />
		<div class="help-block error"></div>
	</div>
	<div class="form-group" data-name="code">
		<label>Символьный код</label>
		<input class="form-control" type="text" name="code" value="{{code}}" />
		<p class="help-block">Оставьте это поле пустым, чтобы система сгенерировала символьный код автоматически...</p>
		<div class="help-block error"></div>
	</div>

	<hr>
	<div class="form-group" data-name="picture">
		<label>Изображение</label>
		<input class="form-control picture-upload" type="file" accept="image/*" />
		<div class="picture-container" style="margin: 10px 0;">
			{{when picture is not empty}}
				<img class="img-thumbnail" src="/upload/320x180/{{picture}}" />
			{{endwhen picture}}
		</div>
		<button type="button" class="btn btn-sm btn-warning picture-reset">
			<i class="fa fa-trash-o" aria-hidden="true"></i> Удалить изображение
		</button>
		<input type="hidden" name="picture" value="{{picture}}" />
		<div class="help-block error"></div>
	</div>

	<hr>
	<div class="form-group" data-name="content">
		<label>Содержимое</label>
		<textarea class="form-control ckeditor" name="content" rows="10">{{content}}</textarea>
		<div class="help-block error"></div>
	</div>

	<hr>
	<div class="form-group" data-name="meta_title">
		<label>Заголовок документа</label>
		<input class="form-control" type="text" name="meta_title" value="{{meta_title}}" />
		<div class="help-block error"></div>
	</div>
	<div class="form-group" data-name="meta_keywords">
		<label>Ключевые слова документа</label>
		<input class="form-control" type="text" name="meta_keywords" value="{{meta_keywords}}" />
		<div class="help-block error"></div>
	</div>
	<div class="form-group" data-name="meta_description">
		<label>Описание документа</label>
		<input class="form-control" type="text" name="meta_description" value="{{meta_description}}" />
		<div class="help-block error"></div>
	</div>
</form>
