<form class="form">
	<nav class="top">
		<div class="btn-group">
			<button type="submit" class="btn btn-sm btn-success">
				<i class="fa fa-floppy-o" aria-hidden="true"></i> Сохранить
			</button>
		</div>
	</nav>

	<div class="form-group" data-name="*">
		<div class="help-block error"></div>
	</div>

	<div class="form-group" data-name="header">
		<label>Заголовок</label>
		<input class="form-control" type="text" name="header" value="{{header}}" />
		<div class="help-block error"></div>
	</div>
	<div class="form-group" data-name="code">
		<label>Символьный код</label>
		<input class="form-control" type="text" name="code" value="{{code}}" />
		<p class="help-block">Оставьте это поле пустым, чтобы система сгенерировала символьный код автоматически...</p>
		<div class="help-block error"></div>
	</div>
	<div class="form-group" data-name="content">
		<label>Содержимое</label>
		<textarea class="form-control ckeditor" name="content" rows="10">{{content}}</textarea>
		<div class="help-block error"></div>
	</div>

	<hr>
	<div class="form-group" data-name="meta_title">
		<label>Заголовок документа</label>
		<input class="form-control" type="text" name="meta_title" value="{{meta_title}}" />
		<div class="help-block error"></div>
	</div>
	<div class="form-group" data-name="meta_keywords">
		<label>Ключевые слова документа</label>
		<input class="form-control" type="text" name="meta_keywords" value="{{meta_keywords}}" />
		<div class="help-block error"></div>
	</div>
	<div class="form-group" data-name="meta_description">
		<label>Описание документа</label>
		<input class="form-control" type="text" name="meta_description" value="{{meta_description}}" />
		<div class="help-block error"></div>
	</div>

	<hr>
	<div class="form-group" data-name="status">
		<label>Статус</label>
		<select class="form-control" name="status">
			<option value="published" {{when status is equal | published}}selected{{endwhen status}}>Опубликовано</option>
			<option value="unpublished" {{when status is equal | unpublished}}selected{{endwhen status}}>Не опубликовано</option>
		</select>
		<div class="help-block error"></div>
	</div>
</form>
