function look(z){
	q=document.getElementById(z);
	w=document.getElementById("q-"+z);
	if(q.style.display=="none"){
		w.innerHTML='<img src="/assets/images/close-white.png">';
		q.style.display="block";
		document.getElementById("text2").style.display="none";
	}
	else{
		w.innerHTML='<img src="/assets/images/options-white.png">';
		q.style.display="none";
		document.getElementById("text2").style.display="block";
	}
}

function see(x){
	r=document.getElementById(x);
	u=document.getElementById("g-"+x);
	if(r.style.display=="none"){
		u.innerHTML='<img src="/assets/images/close-white.png">';
		r.style.display="block";
		document.getElementById("text2").style.display="none";
		document.getElementById('search').focus();
	}
	else{
		u.innerHTML='<img src="/assets/images/search-white.png">';
		r.style.display="none";
		document.getElementById("text2").style.display="block";
	}
}