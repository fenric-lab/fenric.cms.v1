'use strict';

var $poll;

(function(window)
{
	$poll = function()
	{};

	$poll.submit = function(form)
	{
		var i, poll, vote, error;

		if (form instanceof HTMLFormElement)
		{
			poll = form.getAttribute('data-id');

			vote = new Array();

			for (i = 0; i < form.elements.length; i++)
			{
				if (form.elements[i] instanceof HTMLInputElement)
				{
					if (form.elements[i].checked)
					{
						vote.push(form.elements[i].value);
					}
				}
			}

			if (vote.length > 0)
			{
				error = form.querySelector('.error');

				error.innerHTML = '';
				error.style.display = 'none';

				$request.post('/api/vote/{poll}/', {variants: vote}, {poll: poll, success: function(response)
				{
					if (response.success)
					{
						var i, containers, replacement;

						containers = window.document.querySelectorAll('.poll-' + poll);

						replacement = $bugaboo.fragmentation(response.htmlChart);

						for (i = 0; i < containers.length; i++)
						{
							containers[i].parentNode.replaceChild(replacement, containers[i]);
						}

						return;
					}

					error.innerHTML = response.message;
					error.style.display = '';
				}});
			}
		}
	};

})(window);
