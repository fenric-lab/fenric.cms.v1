"use strict";

var clientRequest;

/**
 * Конструктор модуля
 *
 * @access  public
 * @return  void
 */
clientRequest = function()
{};

/**
 * Инициализация модуля
 *
 * @access  public
 * @return  void
 */
clientRequest.init = function()
{
	var forms = document.querySelectorAll('form.form-request');
	
	for (var i = 0; i < forms.length; i++)
	{
		clientRequest.listenForm(forms[i]);
	}
};

/**
 * Слушание отправки формы
 *
 * @param   Node   form
 *
 * @access  public
 * @return  void
 */
clientRequest.listenForm = function(form)
{
	if (form instanceof Node)
	{
		form.addEventListener('submit', function(event)
		{
			event.preventDefault();
			
			if (this.hasAttribute('data-sent')) {
				console.warn('Повторная отправка формы…');
				return;
			}
			
			var data, iName, iPhone, iEmail, iNote, iFormId, iObjectId, iArrivedAt, iArrivedFrom, iCustomFields;
			
			// form = this;
			data = new Object();
			
			iName = form.querySelector('input[name="name"]');
			iPhone = form.querySelector('input[name="phone"]');
			iEmail = form.querySelector('input[name="email"]');
			iNote = form.querySelector('textarea[name="note"]');
			iFormId = form.querySelector('input[name="form_id"]');
			iObjectId = form.querySelector('input[name="object_id"]');
			iArrivedAt = form.querySelector('input[name="arrived_at"]');
			iArrivedFrom = form.querySelector('input[name="arrived_from"]');
			iCustomFields = form.querySelectorAll('[name*="custom_field_"]');
			
			if (iName instanceof Node) {
				data.name = iName.value;
			}
			if (iPhone instanceof Node) {
				data.phone = iPhone.value;
			}
			if (iEmail instanceof Node) {
				data.email = iEmail.value;
			}
			if (iNote instanceof Node) {
				data.note = iNote.value;
			}
			if (iFormId instanceof Node) {
				data.form_id = iFormId.value;
			}
			if (iObjectId instanceof Node) {
				data.object_id = iObjectId.value;
			}
			if (iArrivedAt instanceof Node) {
				data.arrived_at = iArrivedAt.value;
			}
			if (iArrivedFrom instanceof Node) {
				data.arrived_from = iArrivedFrom.value;
			}
			
			data.custom_fields = {};
			
			if (iCustomFields.length > 0)
			{
				for (var i = 0; i < iCustomFields.length; i++)
				{
					if (iCustomFields[i].hasAttribute('data-label'))
					{
						if (iCustomFields[i].type === 'checkbox')
						{
							if (iCustomFields[i].checked == true)
							{
								data.custom_fields[iCustomFields[i].getAttribute('data-label')] = iCustomFields[i].value;
							}
						}
						else {
							data.custom_fields[iCustomFields[i].getAttribute('data-label')] = iCustomFields[i].value;
						}
						
					}
				}
			}
			
			data.send_from = window.location.href;
			
			form.classList.add('blocked');
			
			request.post('/api/client-request/', {data: JSON.stringify(data), success: function(response)
			{
				form.classList.remove('blocked');
				
				if (response.success)
				{
					form.classList.add('success');
					form.classList.add('successful');
					form.classList.remove('error');
					form.setAttribute('data-sent', 'true');
					
					if (data.form_id !== undefined)
					{
						if (yaCounter4480828 !== undefined)
						{
							yaCounter4480828.reachGoal(data.form_id, function()
							{
								console.info('Запрос об отправленной заявки из формы ' + data.form_id + ' успешно отправлен в Яндекс.Метрику.');
							});
							
							yaCounter4480828.reachGoal('CallBack', function()
							{
								console.info('Запрос об отправленной заявки успешно отправлен в Яндекс.Метрику как цель CallBack.');
							});
						}
					}

					setTimeout(function()
					{
						if (iFormId.value == 'realty-primary-index-top-right-form')
						{
							form.classList.add('form-display-none');
						}
					}, 2000);
					
					return;
				}
				
				form.classList.add('error');
			}});
		});
	}
};

/**
 * Автоматическая инициализация модуля
 */
clientRequest.init();
