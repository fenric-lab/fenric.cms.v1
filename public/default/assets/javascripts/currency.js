'use strict';

(function()
{
	$request.get('/api/currency/', {success: function(response)
	{console.dir(response);
		if (response instanceof Array)
		{
			var container, text, i;
			
			text = '';
			
			container = document.querySelector('.currency--container');
			
			for (i = 0;i<response.length;i++)
			{
				if (response[i].code == 'USD')
				{
					text += 'USD '+ parseFloat(response[i].price).toFixed(2) + ' ';
				}
				
				if (response[i].code == 'EUR')
				{
					text += 'EUR '+ parseFloat(response[i].price).toFixed(2) + ' ';
				}
			}
			
			container.innerText = text;
		}
	}});
})();