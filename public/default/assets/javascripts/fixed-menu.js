$(window).bind('scroll', function () {
	if ($(window).scrollTop() > 0) {
		$('.menu').addClass('fixed');
		$('.header-menu').addClass('display-none-important');
	} else {
		$('.menu').removeClass('fixed');
		$('.header-menu').removeClass('display-none-important');
	}
});