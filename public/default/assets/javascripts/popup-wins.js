function showModalWin()
{
	var darkLayer = document.createElement('div'); 
	darkLayer.id = 'shadow';
	document.body.appendChild(darkLayer); 

	var modalWin = document.getElementById('popupWin');
	modalWin.style.display = 'block';

	var closeModal = document.getElementById('modal_close');
	closeModal.onclick = function ()
	{
		darkLayer.onclick();
		modalWin.style.display = 'none';
	};

	darkLayer.onclick = function ()
	{
		darkLayer.parentNode.removeChild(darkLayer); 
		modalWin.style.display = 'none';
		
		return false;
	};
};

function showModalWin1()
{
	var darkLayer = document.createElement('div'); 
	darkLayer.id = 'shadow';
	document.body.appendChild(darkLayer); 

	var modalWin1 = document.getElementById('popupWin1');
	modalWin1.style.display = 'block';

	var closeModal1 = document.getElementById('modal_close1');
	closeModal1.onclick = function ()
	{
		darkLayer.onclick();
		modalWin1.style.display = 'none';
	};

	darkLayer.onclick = function ()
	{
		darkLayer.parentNode.removeChild(darkLayer); 
		modalWin1.style.display = 'none';
		
		return false;
	};
};