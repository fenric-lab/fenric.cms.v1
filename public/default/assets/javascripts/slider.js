"use strict";

var slider;

slider = function(options)
{
	var id, size, underSliderClass, underSliderContainerClass, slidesClass, mainSlider, preUnder, underSlider, slides, carousel, i, videoImage;
	
	i=0;
	
	this.options = options || {};
	
	id = this.options.id || 'awesome_slider';
	size = this.options.size || '160x110';
	videoImage = this.options.videoImage || '/novostrojki_ot_zastrojshhikov/img/video.png';
	underSliderClass = this.options.underSliderClass || 'under_slider';
	underSliderContainerClass = this.options.underSliderContainerClass || 'pre-under-slider';
	slidesClass = this.options.slidesClass || 'slide';
	carousel = this.options.carousel || true;
	
	mainSlider = this.mainSlider = document.getElementById(id);
	
	underSlider = this.underSlider = mainSlider.querySelector('.'+underSliderClass);
	preUnder = this.preUnder = mainSlider.querySelector('.'+underSliderContainerClass);
	slides = this.slides = mainSlider.querySelectorAll('.'+slidesClass);
	
	if (carousel)
	{
		this.buildCarousel(underSlider, slides, size, videoImage);
	}	
};

/**
 * Формирование карусели
 *
 * @param   Node	underSlider
 * @param   Node	slides
 *
 * @access  private
 *
 * @return  void
 */
slider.prototype.buildCarousel = function(underSlider, slides, size, videoImage)
{
	var i;
	
	i=0;
	
	slides.forEach(function(element)
	{
		var divElement, aElement, imgElement, img;
		
		divElement = document.createElement('div');
		
		divElement.classList.add('slide_min');
		
		i == 0 ? divElement.classList.add('active') : '';
		
		divElement.setAttribute('data-number', i);
		
		aElement = document.createElement('a');
		aElement.classList.add('item_photo_mini');
		aElement.setAttribute('href', 'javascript:void(0)');
		aElement.setAttribute('onclick', 'slider.pick(this)');
		
		if (element.querySelector('img') instanceof Node)
		{
			img = element.querySelector('img').getAttribute('src');
			img = img.replace(/[0-9]+x[0-9]+/, size);
			
			imgElement = document.createElement('img');
			imgElement.setAttribute('src', img);
			imgElement.style.width = '160px';
			imgElement.style.height = '110px';
			aElement.appendChild(imgElement);
		}
		else if (element.childNodes[1].nodeName === 'IFRAME') 
		{
			imgElement = document.createElement('img');
			imgElement.setAttribute('src', videoImage);
			aElement.appendChild(imgElement);
		}

		divElement.appendChild(aElement);
		
		i++;
		underSlider.appendChild(divElement);
	});
};

slider.prototype.first = function(target)
{
	slider.slide(target, 'first');
};

slider.prototype.prev = function(target)
{
	slider.slide(target, 'prev');
};

slider.prototype.next = function(target)
{
	slider.slide(target, 'next');
};

slider.prototype.last = function(target)
{
	slider.slide(target, 'last');
};

slider.prototype.pick = function(target)
{
	slider.slide(target, 'pick')
};

/**
 * Инициализация слайдера
 *
 * @param   options
 *
 * @access  public
 *
 * @return  Object
*/
slider.init = function(options)
{
	slider = new slider(options);
};

/**
 * Функция слайд
 *
 * @param   Node	target
 * @param   String  direction  
 *
 * @access  public
 *
 * @return  void
*/
slider.prototype.slide = function(target, direction)
{
	var slide, slides, mainSlider, preUnder, underSlider, position, carousel, countSlides, offset, count;
	
	if (target instanceof Node)
	{
		if (target.parentNode instanceof Node)
		{			
			slide = 0;
			slides = this.slides;
			mainSlider = this.mainSlider;
			preUnder = this.preUnder;
			underSlider = this.underSlider;
			
			position = mainSlider.getAttribute('data-active-slide') || 0;
			carousel = mainSlider.querySelectorAll('.slide_min');
			
			countSlides = Math.floor(preUnder.clientWidth/carousel[0].offsetWidth);
			offset = (carousel[0].offsetWidth)*2 - carousel[0].clientWidth;
			
			count = 0;
			
			if (slides.length > 1)
			{
				for (; slide < slides.length; slide++) {
					slides[slide].classList.add('slide-background');
					slides[slide].classList.remove('slide-foreground');
					carousel[slide].classList.remove('active');
				}

				if (direction == 'first') {
					position = 0;
				}
				if (direction == 'prev') {
					position--;
				}
				if (direction == 'next') {
					position++;
				}
				if (direction == 'last') {
					position = slides.length - 1;
				}
				if (direction == 'pick') {
					position = target.parentNode.getAttribute('data-number');
				}

				if (position <= -1) {
					position = slides.length - 1;
				}
				if (position >= slides.length) {
					position = 0;
				}

				slides[position].classList.add('slide-foreground');
				slides[position].classList.remove('slide-background');
				carousel[position].classList.add('active');
				mainSlider.setAttribute('data-active-slide', position);
				
				if (slides.length > countSlides+1)
				{
					if (position <= Math.ceil(countSlides/2))
					{
						underSlider.style.right = 0;
					}
					if (position >= slides.length-Math.ceil(countSlides/2))
					{
						underSlider.style.right = offset*(slides.length - countSlides-1) + 'px';
					}
					if (position >=Math.ceil(countSlides/2) && position <=(slides.length-Math.ceil(countSlides/2)-1))
					{
						count = Math.abs(Math.floor(countSlides/2) - position);
						underSlider.style.right = count*offset + 'px';
					}
				}
			}
		}
	}
};

/**
 * Автоматическая инициализация слайдера
 */
slider.init();
