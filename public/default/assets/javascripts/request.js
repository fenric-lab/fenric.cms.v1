"use strict";

var request = function(verb, uri, params)
{
	var XHR;
	
	params = params || {};
	
	XHR = new XMLHttpRequest();
	XHR.open(verb, request.prepareURI(uri, params));
	
	XHR.setRequestHeader('Accept', 'application/json');
	XHR.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	XHR.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
	
	XHR.onload = function(event)
	{
		var response;
		
		try {
			response = JSON.parse(this.responseText);
		}
		catch (error) {
			response = this.responseText;
		}
		
		if (params.complete instanceof Function) {
			params.complete.call(this, response, event);
		}
		
		if (this.status >= 200 && this.status <= 202) {
			if (params.success instanceof Function) {
				params.success.call(this, response, event);
			}
		}
	};
	
	XHR.onerror = function(event) {
		if (params.onerror instanceof Function) {
			params.onerror.call(this, event);
		}
	};
	
	XHR.onabort = function(event) {
		if (params.onabort instanceof Function) {
			params.onabort.call(this, event);
		}
	};
	
	XHR.onprogress = function(event) {
		if (params.onprogress instanceof Function) {
			params.onprogress.call(this, event);
		}
	};
	
	XHR.complete = function(callback) {
		params.complete = callback;
		return XHR;
	};
	
	XHR.success = function(callback) {
		params.success = callback;
		return XHR;
	};
	
	XHR.send(params.data || null);
	
	return XHR;
};

request.get = function(uri, params)
{
	return request('GET', uri, params);
};

request.put = function(uri, params)
{
	return request('PUT', uri, params);
};

request.post = function(uri, params)
{
	return request('POST', uri, params);
};

request.patch = function(uri, params)
{
	return request('PATCH', uri, params);
};

request.delete = function(uri, params)
{
	return request('DELETE', uri, params);
};

request.root = function(uri)
{
	return (window.root || '') + uri;
};

request.prepareURI = function(uri, params)
{
	var key, tag;
	
	for (key in params)
	{
		tag = new RegExp('{' + key + '}', 'g');
		
		uri = uri.replace(tag, params[key]);
	}
	
	uri += (uri.indexOf('?') < 0 ? '?' : '&') + Math.random();
	
	return uri;
};