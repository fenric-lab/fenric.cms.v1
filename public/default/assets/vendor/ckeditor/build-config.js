﻿/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

/**
 * This file was added automatically by CKEditor builder.
 * You may re-use it at any time to build CKEditor again.
 *
 * If you would like to build CKEditor online again
 * (for example to upgrade), visit one the following links:
 *
 * (1) http://ckeditor.com/builder
 *     Visit online builder to build CKEditor from scratch.
 *
 * (2) http://ckeditor.com/builder/7f8289aabfa10d4f68e7f0d98dab3c18
 *     Visit online builder to build CKEditor, starting with the same setup as before.
 *
 * (3) http://ckeditor.com/builder/download/7f8289aabfa10d4f68e7f0d98dab3c18
 *     Straight download link to the latest version of CKEditor (Optimized) with the same setup as before.
 *
 * NOTE:
 *    This file is not used by CKEditor, you may remove it.
 *    Changing this file will not change your CKEditor configuration.
 */

var CKBUILDER_CONFIG = {
	skin: 'office2013',
	preset: 'basic',
	ignore: [
		'.DS_Store',
		'.bender',
		'.editorconfig',
		'.gitattributes',
		'.gitignore',
		'.idea',
		'.jscsrc',
		'.jshintignore',
		'.jshintrc',
		'.mailmap',
		'README.md',
		'bender-err.log',
		'bender-out.log',
		'bender.js',
		'dev',
		'gruntfile.js',
		'less',
		'node_modules',
		'package.json',
		'tests'
	],
	plugins : {
		'basicstyles' : 1,
		'blockquote' : 1,
		'clipboard' : 1,
		'enterkey' : 1,
		'entities' : 1,
		'floatingspace' : 1,
		'font' : 1,
		'format' : 1,
		'image' : 1,
		'indentlist' : 1,
		'link' : 1,
		'list' : 1,
		'maximize' : 1,
		'table' : 1,
		'tableresize' : 1,
		'tabletoolstoolbar' : 1,
		'toolbar' : 1,
		'undo' : 1,
		'wordcount' : 1,
		'wysiwygarea' : 1,
		'youtube' : 1
	},
	languages : {
		'en' : 1,
		'ru' : 1
	}
};