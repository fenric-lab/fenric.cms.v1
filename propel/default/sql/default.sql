
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- banner
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `banner`;

CREATE TABLE `banner`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `banner_group_id` INTEGER,
    `title` VARCHAR(255) NOT NULL,
    `description` VARCHAR(255),
    `picture` VARCHAR(255) NOT NULL,
    `picture_alt` VARCHAR(255),
    `picture_title` VARCHAR(255),
    `hyperlink` VARCHAR(255) NOT NULL,
    `hyperlink_target` VARCHAR(255),
    `show_start` DATETIME,
    `show_end` DATETIME,
    `shows` DECIMAL DEFAULT 0,
    `shows_limit` DECIMAL,
    `clicks` DECIMAL DEFAULT 0,
    `clicks_limit` DECIMAL,
    `created_at` DATETIME,
    `created_by` INTEGER,
    `updated_at` DATETIME,
    `updated_by` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `banner_i_ad8f66` (`title`, `show_start`, `show_end`, `shows`, `shows_limit`, `clicks`, `clicks_limit`, `created_at`, `updated_at`),
    INDEX `fi_c056a-4bb5-4ffe-ad4d-1abb56fcaf2b` (`banner_group_id`),
    INDEX `fi_591dd-cbb8-47c3-9e98-7a8fc2112a13` (`created_by`),
    INDEX `fi_bcb76-07c4-48d2-8b34-c0272c824c32` (`updated_by`),
    CONSTRAINT `6e2c056a-4bb5-4ffe-ad4d-1abb56fcaf2b`
        FOREIGN KEY (`banner_group_id`)
        REFERENCES `banner_group` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `47f591dd-cbb8-47c3-9e98-7a8fc2112a13`
        FOREIGN KEY (`created_by`)
        REFERENCES `user` (`id`)
        ON UPDATE CASCADE
        ON DELETE SET NULL,
    CONSTRAINT `fdbbcb76-07c4-48d2-8b34-c0272c824c32`
        FOREIGN KEY (`updated_by`)
        REFERENCES `user` (`id`)
        ON UPDATE CASCADE
        ON DELETE SET NULL
) ENGINE=InnoDB CHARACTER SET='utf8' COLLATE='utf8_unicode_ci';

-- ---------------------------------------------------------------------
-- banner_group
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `banner_group`;

CREATE TABLE `banner_group`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `code` VARCHAR(255) NOT NULL,
    `title` VARCHAR(255) NOT NULL,
    `created_at` DATETIME,
    `created_by` INTEGER,
    `updated_at` DATETIME,
    `updated_by` INTEGER,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `banner_group_u_4db226` (`code`),
    INDEX `banner_group_i_57a377` (`title`, `created_at`, `updated_at`),
    INDEX `fi_71fcc-e3b9-4c61-9d96-d34e2ffaaf87` (`created_by`),
    INDEX `fi_c1152-8248-4508-8740-9b36b6e07e05` (`updated_by`),
    CONSTRAINT `51c71fcc-e3b9-4c61-9d96-d34e2ffaaf87`
        FOREIGN KEY (`created_by`)
        REFERENCES `user` (`id`)
        ON UPDATE CASCADE
        ON DELETE SET NULL,
    CONSTRAINT `7a8c1152-8248-4508-8740-9b36b6e07e05`
        FOREIGN KEY (`updated_by`)
        REFERENCES `user` (`id`)
        ON UPDATE CASCADE
        ON DELETE SET NULL
) ENGINE=InnoDB CHARACTER SET='utf8' COLLATE='utf8_unicode_ci';

-- ---------------------------------------------------------------------
-- menu
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `code` VARCHAR(255) NOT NULL,
    `title` VARCHAR(255) NOT NULL,
    `created_at` DATETIME,
    `created_by` INTEGER,
    `updated_at` DATETIME,
    `updated_by` INTEGER,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `menu_u_4db226` (`code`),
    INDEX `menu_i_57a377` (`title`, `created_at`, `updated_at`),
    INDEX `fi_ddce8-2900-435d-a9fc-0878ac6076a5` (`created_by`),
    INDEX `fi_fc21d-48ad-4aa0-a3ff-318dbf6e2957` (`updated_by`),
    CONSTRAINT `cd7ddce8-2900-435d-a9fc-0878ac6076a5`
        FOREIGN KEY (`created_by`)
        REFERENCES `user` (`id`)
        ON UPDATE CASCADE
        ON DELETE SET NULL,
    CONSTRAINT `a19fc21d-48ad-4aa0-a3ff-318dbf6e2957`
        FOREIGN KEY (`updated_by`)
        REFERENCES `user` (`id`)
        ON UPDATE CASCADE
        ON DELETE SET NULL
) ENGINE=InnoDB CHARACTER SET='utf8' COLLATE='utf8_unicode_ci';

-- ---------------------------------------------------------------------
-- menu_item
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `menu_item`;

CREATE TABLE `menu_item`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `menu_id` INTEGER,
    `parent_id` INTEGER,
    `content` VARCHAR(255) NOT NULL,
    `href` VARCHAR(255),
    `target` VARCHAR(255),
    `display` TINYINT(1) DEFAULT 1 NOT NULL,
    `sequence` DECIMAL DEFAULT 0 NOT NULL,
    `created_at` DATETIME,
    `created_by` INTEGER,
    `updated_at` DATETIME,
    `updated_by` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `menu_item_i_200d21` (`display`, `sequence`),
    INDEX `fi_3607b-5656-4e52-9d92-4b2c2b48f917` (`menu_id`),
    INDEX `fi_98e65-93c8-4bea-8cef-ee0cbe4fd1f7` (`parent_id`),
    INDEX `fi_80ced-bf22-4a8c-9bf8-4d899541483e` (`created_by`),
    INDEX `fi_3b663-f503-4917-b30f-4230c74328f7` (`updated_by`),
    CONSTRAINT `62d3607b-5656-4e52-9d92-4b2c2b48f917`
        FOREIGN KEY (`menu_id`)
        REFERENCES `menu` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `41898e65-93c8-4bea-8cef-ee0cbe4fd1f7`
        FOREIGN KEY (`parent_id`)
        REFERENCES `menu_item` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `92680ced-bf22-4a8c-9bf8-4d899541483e`
        FOREIGN KEY (`created_by`)
        REFERENCES `user` (`id`)
        ON UPDATE CASCADE
        ON DELETE SET NULL,
    CONSTRAINT `b2f3b663-f503-4917-b30f-4230c74328f7`
        FOREIGN KEY (`updated_by`)
        REFERENCES `user` (`id`)
        ON UPDATE CASCADE
        ON DELETE SET NULL
) ENGINE=InnoDB CHARACTER SET='utf8' COLLATE='utf8_unicode_ci';

-- ---------------------------------------------------------------------
-- parameter
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `parameter`;

CREATE TABLE `parameter`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `code` VARCHAR(255) NOT NULL,
    `label` VARCHAR(255) NOT NULL,
    `value` TEXT,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `parameter_u_4db226` (`code`)
) ENGINE=InnoDB CHARACTER SET='utf8' COLLATE='utf8_unicode_ci';

-- ---------------------------------------------------------------------
-- album
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `album`;

CREATE TABLE `album`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `code` VARCHAR(255) NOT NULL,
    `header` VARCHAR(255) NOT NULL,
    `content` TEXT,
    `status` VARCHAR(255) DEFAULT 'published' NOT NULL,
    `meta_title` VARCHAR(255),
    `meta_author` VARCHAR(255),
    `meta_keywords` VARCHAR(255),
    `meta_description` VARCHAR(255),
    `meta_robots` VARCHAR(255),
    `created_at` DATETIME,
    `created_by` INTEGER,
    `updated_at` DATETIME,
    `updated_by` INTEGER,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `album_u_4db226` (`code`),
    INDEX `album_i_0d339f` (`header`, `status`, `created_at`, `updated_at`),
    INDEX `fi_8a94d-70d8-4ab9-a407-f40a316accbd` (`created_by`),
    INDEX `fi_af4b2-bcd0-45c4-9c0d-14f9ddec1367` (`updated_by`),
    CONSTRAINT `cd38a94d-70d8-4ab9-a407-f40a316accbd`
        FOREIGN KEY (`created_by`)
        REFERENCES `user` (`id`)
        ON UPDATE CASCADE
        ON DELETE SET NULL,
    CONSTRAINT `d63af4b2-bcd0-45c4-9c0d-14f9ddec1367`
        FOREIGN KEY (`updated_by`)
        REFERENCES `user` (`id`)
        ON UPDATE CASCADE
        ON DELETE SET NULL
) ENGINE=InnoDB CHARACTER SET='utf8' COLLATE='utf8_unicode_ci';

-- ---------------------------------------------------------------------
-- photo
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `photo`;

CREATE TABLE `photo`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `album_id` INTEGER,
    `file` VARCHAR(255) NOT NULL,
    `display` TINYINT(1) DEFAULT 1 NOT NULL,
    `sequence` DECIMAL DEFAULT 0 NOT NULL,
    `created_at` DATETIME,
    `created_by` INTEGER,
    `updated_at` DATETIME,
    `updated_by` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `photo_i_663992` (`display`, `sequence`, `created_at`, `updated_at`),
    INDEX `fi_ce3a4-6b23-4ece-bca1-1f6438147010` (`album_id`),
    INDEX `fi_a7429-8e15-4ccd-b341-9db32d995be8` (`created_by`),
    INDEX `fi_0ae65-5ec4-40fc-a84c-bdf8df580b49` (`updated_by`),
    CONSTRAINT `19cce3a4-6b23-4ece-bca1-1f6438147010`
        FOREIGN KEY (`album_id`)
        REFERENCES `album` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `76fa7429-8e15-4ccd-b341-9db32d995be8`
        FOREIGN KEY (`created_by`)
        REFERENCES `user` (`id`)
        ON UPDATE CASCADE
        ON DELETE SET NULL,
    CONSTRAINT `0050ae65-5ec4-40fc-a84c-bdf8df580b49`
        FOREIGN KEY (`updated_by`)
        REFERENCES `user` (`id`)
        ON UPDATE CASCADE
        ON DELETE SET NULL
) ENGINE=InnoDB CHARACTER SET='utf8' COLLATE='utf8_unicode_ci';

-- ---------------------------------------------------------------------
-- poll
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `poll`;

CREATE TABLE `poll`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `code` VARCHAR(255) NOT NULL,
    `title` VARCHAR(255) NOT NULL,
    `multiple` TINYINT(1) DEFAULT 0 NOT NULL,
    `open_at` DATETIME,
    `close_at` DATETIME,
    `created_at` DATETIME,
    `created_by` INTEGER,
    `updated_at` DATETIME,
    `updated_by` INTEGER,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `poll_u_4db226` (`code`),
    INDEX `poll_i_bfeafb` (`title`, `open_at`, `close_at`, `created_at`, `updated_at`),
    INDEX `fi_93a69-faa5-4cf7-8421-affab4bdd705` (`created_by`),
    INDEX `fi_21383-d037-4fbb-b0c1-ac6b485119e2` (`updated_by`),
    CONSTRAINT `05f93a69-faa5-4cf7-8421-affab4bdd705`
        FOREIGN KEY (`created_by`)
        REFERENCES `user` (`id`)
        ON UPDATE CASCADE
        ON DELETE SET NULL,
    CONSTRAINT `6aa21383-d037-4fbb-b0c1-ac6b485119e2`
        FOREIGN KEY (`updated_by`)
        REFERENCES `user` (`id`)
        ON UPDATE CASCADE
        ON DELETE SET NULL
) ENGINE=InnoDB CHARACTER SET='utf8' COLLATE='utf8_unicode_ci';

-- ---------------------------------------------------------------------
-- poll_variant
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `poll_variant`;

CREATE TABLE `poll_variant`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `poll_id` INTEGER,
    `title` VARCHAR(255) NOT NULL,
    `created_at` DATETIME,
    `created_by` INTEGER,
    `updated_at` DATETIME,
    `updated_by` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `fi_331c0-62d6-4647-b74e-02a9ae9724d8` (`poll_id`),
    INDEX `fi_5366f-abcd-4d59-879c-6b5a41ab0f98` (`created_by`),
    INDEX `fi_dac17-4cda-4369-a71b-c24ae7c547c1` (`updated_by`),
    CONSTRAINT `9b3331c0-62d6-4647-b74e-02a9ae9724d8`
        FOREIGN KEY (`poll_id`)
        REFERENCES `poll` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `8a85366f-abcd-4d59-879c-6b5a41ab0f98`
        FOREIGN KEY (`created_by`)
        REFERENCES `user` (`id`)
        ON UPDATE CASCADE
        ON DELETE SET NULL,
    CONSTRAINT `3a9dac17-4cda-4369-a71b-c24ae7c547c1`
        FOREIGN KEY (`updated_by`)
        REFERENCES `user` (`id`)
        ON UPDATE CASCADE
        ON DELETE SET NULL
) ENGINE=InnoDB CHARACTER SET='utf8' COLLATE='utf8_unicode_ci';

-- ---------------------------------------------------------------------
-- poll_vote
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `poll_vote`;

CREATE TABLE `poll_vote`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `poll_variant_id` INTEGER,
    `respondent_user_agent` VARCHAR(255) NOT NULL,
    `respondent_remote_address` VARCHAR(255) NOT NULL,
    `respondent_session_id` VARCHAR(255) NOT NULL,
    `respondent_vote_id` VARCHAR(255) NOT NULL,
    `respondent_id` VARCHAR(255) NOT NULL,
    `vote_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `poll_vote_i_6d5391` (`vote_at`),
    INDEX `fi_5abf0-4a0f-488d-985e-a26d11ba529b` (`poll_variant_id`),
    CONSTRAINT `6155abf0-4a0f-488d-985e-a26d11ba529b`
        FOREIGN KEY (`poll_variant_id`)
        REFERENCES `poll_variant` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=InnoDB CHARACTER SET='utf8' COLLATE='utf8_unicode_ci';

-- ---------------------------------------------------------------------
-- section
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `section`;

CREATE TABLE `section`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `code` VARCHAR(255) NOT NULL,
    `header` VARCHAR(255) NOT NULL,
    `content` TEXT,
    `meta_title` VARCHAR(255),
    `meta_author` VARCHAR(255),
    `meta_keywords` VARCHAR(255),
    `meta_description` VARCHAR(255),
    `meta_robots` VARCHAR(255),
    `created_at` DATETIME,
    `created_by` INTEGER,
    `updated_at` DATETIME,
    `updated_by` INTEGER,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `section_u_4db226` (`code`),
    INDEX `section_i_f5dff9` (`header`, `created_at`, `updated_at`),
    INDEX `fi_af8a9-dacd-4b52-a60a-e13fb80e7342` (`created_by`),
    INDEX `fi_a7a88-b516-4f37-a575-84aea282b1ed` (`updated_by`),
    CONSTRAINT `ae3af8a9-dacd-4b52-a60a-e13fb80e7342`
        FOREIGN KEY (`created_by`)
        REFERENCES `user` (`id`)
        ON UPDATE CASCADE
        ON DELETE SET NULL,
    CONSTRAINT `3aaa7a88-b516-4f37-a575-84aea282b1ed`
        FOREIGN KEY (`updated_by`)
        REFERENCES `user` (`id`)
        ON UPDATE CASCADE
        ON DELETE SET NULL
) ENGINE=InnoDB CHARACTER SET='utf8' COLLATE='utf8_unicode_ci';

-- ---------------------------------------------------------------------
-- publication
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `publication`;

CREATE TABLE `publication`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `section_id` INTEGER,
    `hot` TINYINT(1) DEFAULT 0 NOT NULL,
    `code` VARCHAR(255) NOT NULL,
    `header` VARCHAR(255) NOT NULL,
    `picture` VARCHAR(255),
    `picture_signature` VARCHAR(255),
    `anons` TEXT NOT NULL,
    `content` TEXT NOT NULL,
    `meta_title` VARCHAR(255),
    `meta_author` VARCHAR(255),
    `meta_keywords` VARCHAR(255),
    `meta_description` VARCHAR(255),
    `meta_robots` VARCHAR(255),
    `created_at` DATETIME,
    `created_by` INTEGER,
    `updated_at` DATETIME,
    `updated_by` INTEGER,
    `show_at` DATETIME,
    `hide_at` DATETIME,
    `hits` DECIMAL DEFAULT 0,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `publication_u_4db226` (`code`),
    INDEX `publication_i_ad3c64` (`hot`, `header`, `created_at`, `updated_at`, `show_at`, `hide_at`, `hits`),
    INDEX `fi_6d124-043c-4286-abd8-b2c1d71c4667` (`section_id`),
    INDEX `fi_643e9-5fd8-4ea8-a247-6c2537382ecd` (`created_by`),
    INDEX `fi_60282-9ecc-4beb-b2b8-8f7340cb359a` (`updated_by`),
    CONSTRAINT `6ee6d124-043c-4286-abd8-b2c1d71c4667`
        FOREIGN KEY (`section_id`)
        REFERENCES `section` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `e15643e9-5fd8-4ea8-a247-6c2537382ecd`
        FOREIGN KEY (`created_by`)
        REFERENCES `user` (`id`)
        ON UPDATE CASCADE
        ON DELETE SET NULL,
    CONSTRAINT `5de60282-9ecc-4beb-b2b8-8f7340cb359a`
        FOREIGN KEY (`updated_by`)
        REFERENCES `user` (`id`)
        ON UPDATE CASCADE
        ON DELETE SET NULL
) ENGINE=InnoDB CHARACTER SET='utf8' COLLATE='utf8_unicode_ci';

-- ---------------------------------------------------------------------
-- publication_tag
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `publication_tag`;

CREATE TABLE `publication_tag`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `publication_id` INTEGER,
    `tag_id` INTEGER,
    `created_at` DATETIME,
    `created_by` INTEGER,
    `updated_at` DATETIME,
    `updated_by` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `fi_06349-8b35-4620-b8db-75d284af1940` (`publication_id`),
    INDEX `fi_b04f8-5f70-4c73-9c6a-179cb17df43f` (`tag_id`),
    INDEX `fi_32bdc-2fcb-40f3-b4cb-4e5abe63b6f5` (`created_by`),
    INDEX `fi_77e91-04d6-48b0-9412-4305f2140629` (`updated_by`),
    CONSTRAINT `9be06349-8b35-4620-b8db-75d284af1940`
        FOREIGN KEY (`publication_id`)
        REFERENCES `publication` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `d28b04f8-5f70-4c73-9c6a-179cb17df43f`
        FOREIGN KEY (`tag_id`)
        REFERENCES `tag` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `7aa32bdc-2fcb-40f3-b4cb-4e5abe63b6f5`
        FOREIGN KEY (`created_by`)
        REFERENCES `user` (`id`)
        ON UPDATE CASCADE
        ON DELETE SET NULL,
    CONSTRAINT `c3377e91-04d6-48b0-9412-4305f2140629`
        FOREIGN KEY (`updated_by`)
        REFERENCES `user` (`id`)
        ON UPDATE CASCADE
        ON DELETE SET NULL
) ENGINE=InnoDB CHARACTER SET='utf8' COLLATE='utf8_unicode_ci';

-- ---------------------------------------------------------------------
-- shortlink
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `shortlink`;

CREATE TABLE `shortlink`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `code` VARCHAR(255) NOT NULL,
    `title` VARCHAR(255) NOT NULL,
    `location` VARCHAR(255) NOT NULL,
    `referrals` BIGINT DEFAULT 0,
    `created_at` DATETIME,
    `created_by` INTEGER,
    `updated_at` DATETIME,
    `updated_by` INTEGER,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `shortlink_u_4db226` (`code`),
    INDEX `shortlink_i_77925e` (`title`, `location`, `referrals`, `created_at`, `updated_at`),
    INDEX `fi_6b908-8e04-43fc-b802-813bea51a1c7` (`created_by`),
    INDEX `fi_ade69-a6d6-4ce1-b3df-08388766d7f3` (`updated_by`),
    CONSTRAINT `77e6b908-8e04-43fc-b802-813bea51a1c7`
        FOREIGN KEY (`created_by`)
        REFERENCES `user` (`id`)
        ON UPDATE CASCADE
        ON DELETE SET NULL,
    CONSTRAINT `c27ade69-a6d6-4ce1-b3df-08388766d7f3`
        FOREIGN KEY (`updated_by`)
        REFERENCES `user` (`id`)
        ON UPDATE CASCADE
        ON DELETE SET NULL
) ENGINE=InnoDB CHARACTER SET='utf8' COLLATE='utf8_unicode_ci';

-- ---------------------------------------------------------------------
-- snippet
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `snippet`;

CREATE TABLE `snippet`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `code` VARCHAR(255) NOT NULL,
    `title` VARCHAR(255) NOT NULL,
    `value` TEXT NOT NULL,
    `created_at` DATETIME,
    `created_by` INTEGER,
    `updated_at` DATETIME,
    `updated_by` INTEGER,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `snippet_u_4db226` (`code`),
    INDEX `snippet_i_57a377` (`title`, `created_at`, `updated_at`),
    INDEX `fi_bee5b-6ed6-4823-a281-c8cfb4b6549b` (`created_by`),
    INDEX `fi_aa3d1-eef7-43d7-8b2b-39a6e310c30b` (`updated_by`),
    CONSTRAINT `251bee5b-6ed6-4823-a281-c8cfb4b6549b`
        FOREIGN KEY (`created_by`)
        REFERENCES `user` (`id`)
        ON UPDATE CASCADE
        ON DELETE SET NULL,
    CONSTRAINT `fbdaa3d1-eef7-43d7-8b2b-39a6e310c30b`
        FOREIGN KEY (`updated_by`)
        REFERENCES `user` (`id`)
        ON UPDATE CASCADE
        ON DELETE SET NULL
) ENGINE=InnoDB CHARACTER SET='utf8' COLLATE='utf8_unicode_ci';

-- ---------------------------------------------------------------------
-- tag
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `tag`;

CREATE TABLE `tag`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `code` VARCHAR(255) NOT NULL,
    `header` VARCHAR(255) NOT NULL,
    `content` TEXT,
    `meta_title` VARCHAR(255),
    `meta_author` VARCHAR(255),
    `meta_keywords` VARCHAR(255),
    `meta_description` VARCHAR(255),
    `meta_robots` VARCHAR(255),
    `created_at` DATETIME,
    `created_by` INTEGER,
    `updated_at` DATETIME,
    `updated_by` INTEGER,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `tag_u_4db226` (`code`),
    INDEX `tag_i_f5dff9` (`header`, `created_at`, `updated_at`),
    INDEX `fi_c5de6-e1b1-4a8b-944f-b91bf9d8d3bc` (`created_by`),
    INDEX `fi_8be1f-c948-4b0a-bf7e-ad3820536276` (`updated_by`),
    CONSTRAINT `92dc5de6-e1b1-4a8b-944f-b91bf9d8d3bc`
        FOREIGN KEY (`created_by`)
        REFERENCES `user` (`id`)
        ON UPDATE CASCADE
        ON DELETE SET NULL,
    CONSTRAINT `8008be1f-c948-4b0a-bf7e-ad3820536276`
        FOREIGN KEY (`updated_by`)
        REFERENCES `user` (`id`)
        ON UPDATE CASCADE
        ON DELETE SET NULL
) ENGINE=InnoDB CHARACTER SET='utf8' COLLATE='utf8_unicode_ci';

-- ---------------------------------------------------------------------
-- user
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user`
(
    `id` INTEGER(11) NOT NULL AUTO_INCREMENT,
    `role` VARCHAR(64) DEFAULT 'user' NOT NULL,
    `email` VARCHAR(128) NOT NULL,
    `username` VARCHAR(48) NOT NULL,
    `password` VARCHAR(60) NOT NULL,
    `firstname` VARCHAR(64),
    `lastname` VARCHAR(64),
    `photo` VARCHAR(255),
    `gender` VARCHAR(16),
    `birthday` DATETIME,
    `signature` VARCHAR(255),
    `about` TEXT,
    `params` TEXT,
    `registration_at` DATETIME,
    `registration_ip` VARCHAR(45),
    `registration_confirmed` TINYINT(1) DEFAULT 0,
    `registration_confirmed_at` DATETIME,
    `registration_confirmed_ip` VARCHAR(45),
    `registration_confirmation_code` VARCHAR(40),
    `authentication_at` DATETIME,
    `authentication_ip` VARCHAR(45),
    `authentication_key` VARCHAR(255),
    `authentication_token` VARCHAR(40),
    `authentication_token_at` DATETIME,
    `authentication_token_ip` VARCHAR(45),
    `track_at` DATETIME,
    `track_ip` VARCHAR(45),
    `track_url` VARCHAR(255),
    `ban_from` DATETIME,
    `ban_until` DATETIME,
    `ban_reason` VARCHAR(255),
    PRIMARY KEY (`id`),
    UNIQUE INDEX `user_u_afde13` (`email`, `username`),
    INDEX `user_i_8bb332` (`role`, `firstname`, `lastname`, `gender`, `birthday`, `registration_at`, `registration_ip`, `registration_confirmation_code`, `authentication_at`, `authentication_ip`, `authentication_token`, `track_at`, `track_ip`, `ban_from`, `ban_until`)
) ENGINE=InnoDB CHARACTER SET='utf8' COLLATE='utf8_unicode_ci';

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
